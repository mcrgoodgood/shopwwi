﻿$(function (){
    if (getQueryString('key') != '') {
        var key = getQueryString('key');
        var username = getQueryString('username');
        addCookie('key', key);
        addCookie('username', username);
    } else {
        var key = getCookie('key');
    }
    var html = '<div class="nctouch-footer-wrap posr">'
        +'<div class="nav-text">';
    if(key){
        html += '<a href="'+WapSiteUrl+'/tmpl/member/member.html">我的商城</a>'
            + '<a id="logoutbtn" href="javascript:void(0);">注销</a>'
            + '<a href="'+WapSiteUrl+'/tmpl/member/member_feedback.html">反馈</a>';
            
    } else {
        html += '<a href="'+WapSiteUrl+'/tmpl/member/login.html">登录</a>'
            + '<a href="'+WapSiteUrl+'/tmpl/member/register.html">注册</a>'
            + '<a href="'+WapSiteUrl+'/tmpl/member/login.html">反馈</a>';
    }
    html += '<a href="javascript:void(0);" class="gotop">返回顶部</a>'
        +'</div>'
        +'<div class="nav-pic">'
			+'<a href="'+SiteUrl+'/mall/index.php?app=mb_app" class="app"><span><i></i></span><p>客户端</p></a>'
			+'<a href="javascript:void(0);" class="touch"><span><i></i></span><p>触屏版</p></a>'
            +'<a href="'+SiteUrl+'" class="pc"><span><i></i></span><p>电脑版</p></a>'
         +'</div>'
		 +'<div class="copyright">'
    	 +'</div>';
	$("#footer").append(html);
    var key = getCookie('key');
	$('#logoutbtn').click(function(){
		var username = getCookie('username');
		var key = getCookie('key');
		var client = 'wap';
		$.ajax({
			type:'get',
			url:ApiUrl+'/index.php?app=logout',
			data:{username:username,key:key,client:client},
			success:function(result){
				if(result){
					delCookie('username');
					delCookie('key');
					location.href = WapSiteUrl;
				}
			}
		});
	});


    var ua = navigator.userAgent.toLowerCase();
    if ((ua.indexOf('micromessenger') > -1)) { // 微信相关操作
        $.getScript('http://res.wx.qq.com/open/js/jweixin-1.0.0.js', function() {
            var sharedata = {
                title: '优居完整家居平台上线大促，全房装修只需399元/㎡(主材价)',
                desc: '选设计，找施工，挑主材就上优居完整家居',
                link: window.location.href,
                imgUrl: CdnUrl + '/wap/images/wx_share.png'
            };
            $.getJSON(ApiUrl + '/index.php?app=wx&wwi=get_js_config', function(data) {
                wx.config(JSON.parse(data.datas));
                wx.ready(function() {
                    wx.onMenuShareAppMessage(sharedata);
                    wx.onMenuShareTimeline(sharedata);
                    wx.onMenuShareQQ(sharedata);
                    wx.onMenuShareWeibo(sharedata);
                });
            });
            wx.error(function(res){
                alert('微信操作失败: ' + JSON.stringify(res));
            });
        });
    }


    // 统计
    var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
    $('body').append(unescape("%3Cdiv class='none' %3E%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F2abfc0d2c6f703a09ffb16d42a0073fe' type='text/javascript'%3E%3C/script%3E%3C/div%3E"));
});