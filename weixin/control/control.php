<?php
/**
 * 前台control父类,会员control父类
 *
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');

class Control{
	/**
	 * 系统通知发送函数
	 *
	 * @param int $receiver_id 接受人编号
	 * @param string $tpl_code 模板标识码
	 * @param array $param 内容数组
	 * @param bool $flag 是否遵从系统设置
	 * @return boolean
	 */
	protected function send_notice($receiver_id,$tpl_code,$param,$flag = true){
		$model	= Model();
		$mail_tpl = $model->table('msg_temlates')->where(array('code'=>$tpl_code))->find();
		if(empty($mail_tpl) || $mail_tpl['is_open'] == 0)return false;
		$receiver = $model->table('member')->where(array('member_id'=>$receiver_id))->find();
		if(empty($receiver)) return false;
		$subject	= ncReplaceText($mail_tpl['title'],$param);
		$message	= ncReplaceText($mail_tpl['content'],$param);
		//根据模板里面确定的通知类型采用对应模式发送通知
		$result	= false;
		switch($mail_tpl['type']){
			case '0':
				$email	= new Email();
				$result	= true;
				if($flag && C('email_enabled') == '1' || $flag == false){
					$result	= $email->send_sys_email($receiver['member_email'],$subject,$message);
				}
				break;
			case '1':
				$insert_arr = array();
				$insert_arr['to_member_id'] = $receiver_id;
				$insert_arr['to_member_name'] = $receiver['member_name'];
				$insert_arr['message_title'] = $subject;
				$insert_arr['message_body'] = $message;
				$insert_arr['message_time'] = time();
				$insert_arr['message_ismore'] = 0;
				$result = $model->table('message')->insert($insert_arr);
				break;
		}
		return $result;
	}
	/**
	 * 检查短消息数量
	 *
	 */
	protected function checkMessage() {
		if($_SESSION['member_id'] == '') return ;
		//判断cookie是否存在
		$cookie_name = 'msgnewnum'.$_SESSION['member_id'];
		if (cookie($cookie_name) != null && intval(cookie($cookie_name)) >=0){
			$countnum = intval(cookie($cookie_name));
		} else {
			$message_model = Model('message');
			$countnum = $message_model->countNewMessage($_SESSION['member_id']);
			setNcCookie($cookie_name,"$countnum",2*3600);//保存1天
		}
		Tpl::output('message_num',$countnum);
	}
	/**
	 * 验证会员是否登录
	 */
	protected function checkLogin(){
		if ($_SESSION['is_login'] !== '1'){
			if (trim($_GET['op']) == 'favoritesgoods'){
				$lang = Language::getLangContent('UTF-8');
				echo json_encode(array('done'=>false,'msg'=>$lang['no_login']));
				die;
			}
			$ref_url = request_uri();
			if ($_GET['inajax']){
				showDialog('','','js',"login_dialog();",200);
			}else {
				@header("location: index.php?act=login&op=login&ref_url=".urlencode($ref_url));
			}
			exit;
		}
	}
	/**
	 * 保存远程头像图片到本地
	 */
	public function save_img($img_url,$openid,$wxid){
		if($img_url == "") return false;
		$filename = md5($openid.'=wxid='.$wxid).'.png';
		ob_start();
		readfile($img_url);
		$img = ob_get_contents();
		ob_end_clean();
		$fp2=@fopen(BASE_UPLOAD_PATH.'/avatar/'.$filename, "a");
		fwrite($fp2,$img);
		fclose($fp2);
		return $filename;
	}

	/**
	 * 用户行为日志
	 * */
	protected  function fans_log($data){
	    if(is_array($data) && !empty($data)){
	        $model = Model();
	        $model->table('fans_activity')->insert($data);
	    }
	}

	/**
	 * 更新粉丝列表
	 */
	public function fanslist_update($wx_id){
		$model = Model();
		//调取公众账号的接口信息
		$wx_info = $model->field('wx_appid,wx_appsecret,wx_accesstoken,wx_nextopenid')->table('wxaccount')->where(array('wx_id'=>$wx_id))->find();
		$access_token = $wx_info['wx_accesstoken'];
		if($access_token == ''){
			$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
		}
		$url = 'https://api.weixin.qq.com/cgi-bin/user/get?access_token='.$access_token;
		if($wx_info['wx_nextopenid'] != ''){
			$url .= '&next_openid='.$wx_info['wx_nextopenid'];
		}
		$res = @file_get_contents($url);
		$res = json_decode($res,true);
		if($res['errcode'] != ''){
			if($res['errcode'] == '40001' || $res['errcode'] == '42001' || $res['errcode'] == '42002' || $res['errcode'] == '42003'){
				$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $wx_id);
				$res = @file_get_contents($url);
				$res = json_decode($res,true);
				if($res['errcode'] != ''){
					return false;
				}
			}else{
				return false;
			}
		}
		if($res['count'] > 0){
			$openid_list = $res['data']['openid'];
			$pagenum = ceil($res['total']/$res['count']);
			$next_openid = $res['next_openid'];
			if($pagenum > 1){
				for ($i=2;$i<=$pagenum;$i++){
					$rs = @file_get_contents('https://api.weixin.qq.com/cgi-bin/user/get?access_token='.$access_token.'&next_openid='.$next_openid);
					if($res['errcode'] != ''){
						return false;
					}
					$openid_list = array_merge($openid_list,$rs['data']['openid']);
					$next_openid = $rs['next_openid'];
				}
			}
			//抓取所有粉丝信息
			$fans_insert_array = array();
			if(!empty($openid_list)){
				foreach ($openid_list as $val){
					$fans_info = @file_get_contents('https://api.weixin.qq.com/cgi-bin/user/info?access_token='.$access_token.'&openid='.$val);
					$fans_info = json_decode($fans_info,true);
					if($fans_info['errcode'] != ''){
						return false;
					}
					$fans_insert_array[] = array(
						'fans_openid'=>$val,
						'wx_id'=>$wx_id,
						'fans_nickname'=>$fans_info['nickname'],
						'fans_sex'=>$fans_info['sex'],
						'fans_language'=>$fans_info['language'],
						'fans_city'=>$fans_info['city'],
						'fans_province'=>$fans_info['province'],
						'fans_country'=>$fans_info['country'],
						'fans_headimgurl'=>$fans_info['headimgurl'],
						'fans_pic'=>'',
						'fans_subtime'=>$fans_info['subscribe_time']
					);
				}
				$frs = $model->table('fans')->insertAll($fans_insert_array);
				if($frs){
					//更新next_openid
					$model->table('wxaccount')->where(array('wx_id'=>$wx_id))->update(array('wx_nextopenid'=>$next_openid));
					return true;
				}else{
					return false;
				}
			}
		}
		return true;
	}
	/**
	 * 重新获取accesstoken
	 */
	public function get_accesstoken($appid,$apps,$wx_id){
		$url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='.$appid.'&secret='.$apps;
		$res = @file_get_contents($url);
		$res = json_decode($res,true);
		if($res['errcode'] != ''){
			return false;
		}else{
			$model = Model();
			$model->table('wxaccount')->where(array('wx_id'=>$wx_id))->update(array('wx_accesstoken'=>$res['access_token']));
			return $res['access_token'];
		}
	}
}

/********************************** 前台control父类 **********************************************/

class BaseHomeControl extends Control {
	public function __construct(){
		$this->checkLogin();
		Tpl::setDir('home');
		Tpl::setLayout('home_layout');
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		if($wx_id <= 0){
			showMessage('参数错误','','error');
		}
		$wx_info = $model->table('wxaccount')->where(array('wx_id'=>$wx_id))->find();
		if($_SESSION['admin_is_super'] == 0 && $wx_info['admin_id'] != $_SESSION['admin_id']){
			showMessage('您无权管理该公共账号','','error');
		}
		Tpl::output('wx_info',$wx_info);
		Language::read('common,site');
		if(!C('site_status')) halt(C('closed_reason'));
	}
}
/********************************** 前台微信墙 **********************************************/
class WallBaseControl extends Control{
	public function __construct(){
		Language::read('weixin_wall');
		Tpl::setDir('wall');
	}
}

/************************************* 前台wap **********************************************/
class wapBaseControl extends Control{
	var $wxinfo;
	public function __construct(){
		Tpl::setDir('waps');
		Tpl::setLayout('null_layout');
		$model = Model();
		$wx_id = intval($_REQUEST['wx_id']);
		if($wx_id <= 0){
			showMessage('参数错误','','error');
		}
		$this->wxinfo = $model->table('wxaccount')->where(array('wx_id'=>$wx_id))->find();
	}
}

/********************************** 前台商城展示 **********************************************/
class mallBaseControl extends Control{
	public function __construct(){

		if(isset($_GET['amp;op'])){
			$_GET['op'] = $_GET['amp;op'];
		}

		if(isset($_GET['amp;article_id'])){
			$_GET['article_id'] = $_GET['amp;article_id'];
		}

		if(isset($_GET['amp;wx_id'])){
			$_GET['wx_id'] = $_GET['amp;wx_id'];
		}

		$model = Model();
		$account = $model->table('wxaccount')->where(array('wx_id'=>intval($_GET['wx_id'])))->find();
		if(empty($account)){
			showMessage('参数错误','?act=index','error');
		}

		$site = $model->table('site')->where(array('wx_id'=>intval($_GET['wx_id'])))->find();
		Tpl::output('site',$site);

		Tpl::setDir('mall');
		Tpl::setLayout('mall_layout');
		Language::read('mall');
	}
}