<div class="main_hd">
  <h2><?php echo $lang['nc_basic_article'];?></h2>
</div>
<div class="main_bd">
  <div class="table_msg">
    <form method="post" id="list_form">
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell time asc"><?php echo $lang['nc_basic_title'];?></th>
          <th class="table_cell goods"><?php echo $lang['nc_basic_publish_time'];?></th>
          <th class="table_cell desc"><?php echo $lang['nc_handle'];?></th>
        </tr>
      </thead>
      <tbody class="nc-tbody">
        <?php if(!empty($output['list'])){?>
        <?php foreach($output['list'] as $key=>$val){?>
        <tr>
          <td><?php echo $val['title'];?></td>
          <td><?php echo date("Y-m-d",$val['publish_time']);?></td>
          <td class="last">
		  	<a href="?act=basic&op=editBasic&wx_id=<?php echo intval($_GET['wx_id']);?>&basic_id=<?php echo $val['basic_id'];?>"><?php echo $lang['nc_edit'];?></a>
		  </td>
        </tr>
        <?php }?>
        <?php }else{?>
        <tr>
          <td colspan="20" valign="middle" ><div class="norecord"><span><?php echo $lang['nc_record'];?></span></div></td>
        </tr>
        <?php }?>
      </tbody>
      <tfoot class="nc-footer">
       <tr>
      	<td class="table_cell">
      	</td>
      	<td colspan='6' class="table_cell last">       
      	</td>
        </tr>
      </tfoot>
    </table>
    </form>
  </div>
</div>
