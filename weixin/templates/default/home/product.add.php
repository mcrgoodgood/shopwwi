<div class="main_hd">
  <h2>商品管理</h2>
</div>
<div class="main_bd">
  <form method="post" id="apply_form" action="?act=product&op=addProduct&wx_id=<?php echo intval($_GET['wx_id']);?>" enctype="multipart/form-data">
    <ul>
      <li class="list_item">
        <label class="label_box">商品名称:</label>
        <div class="label_form"> 
          <span>
          	<input type="text" class="label_input" name="product_name" id="product_name">
            <label for='product_name' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>
      
      <li class="list_item">
        <label class="label_box">商品价格:</label>
        <div class="label_form"> 
          <span>
          	<input type="text" class="label_input" name="product_price" id="product_price">
            <label for='product_price' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>     
 
      <li class="list_item">
        <label class="label_box">商品状态:</label>
        <div class="label_form"> 
          <span>
          	<input type="radio" name="product_state" value="0" checked>上架&nbsp;&nbsp;
          	<input type="radio" name="product_state" value="1">下架
            <label for='product_state' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>     

      <li class="list_item">
        <label class="label_box">首页推荐:</label>
        <div class="label_form"> 
          <span>
          	<input type="radio" name="product_recommend" value="0" checked>不推荐&nbsp;&nbsp;
          	<input type="radio" name="product_recommend" value="1">推荐
            <label for='product_state' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li> 
      
      <li class="list_item">
        <label class="label_box">商品图片:</label>
        <div class="label_form"> 
          <span>
          	<input type="file" name="product_pic">
            <label for='product_state' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>   
      
      <li class="list_item">
        <label class="label_box">商品描述:</label>
        <div class="label_form">
          <?php showEditor('product_content','','550px','300px','','true',false);?>
        </div>
      </li>
      <li>
        <div class="btn_bar">
        <input type="submit" class="btn_input" value="<?php echo $lang['nc_save'];?>"></div>
      </li>
    </ul>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script> 
<script type="text/javascript">
$(function(){
	jQuery.validator.addMethod("letters", function(value, element) {
		return this.optional(element) || ($.trim(value.replace(/[^\u0000-\u00ff]/g,"aa")).length==1);
	}, "Letters min please"); 
	
	$("#apply_form").validate({//验证表单
        rules: {
        	product_name: {
				required:true
            },
            product_price:{
            	required:true,
				number : true,
                min : 0.01,
                max : 1000000
            },
            product_pic:{
            	required:true,
            	accept:'jpg|png|gif'
            }
        },
        messages:{
        	product_name:{
        		required:'商品名称不能为空'
            },
            product_price:{
            	required:'商品价格不能为空',
            	number:'商品价格必须是数字',
            	min:'商品价格在0.01~1000000',
            	max:'商品价格在0.01~1000000'
            },
            product_pic:{
				required:'商品图片不能为空',
				accept:'图片格式不正确'
            }          
        }
	});
});
</script> 
