import extend from 'lodash/extend';
import axios from 'axios';

/**
 * Api请求http服务
 */
export let http = axios.create({
  baseURL: G.basePath + '/mo_bile',
  data: {key: getKey()}
});
http.interceptors.response.use(function (response) {
  if (response.data && (response.data.code != 200)) { // 判断用户的code代码是否执行成功
    return Promise.reject(response.data);
  }
  return response;
});

/**
 * 获取本地存储的用户身份key
 * @returns {null}
 */
export function getKey() {
  let keyExpires = localStorage.getItem('keyExpires');
  return keyExpires && (keyExpires > new Date().getTime()) ? localStorage.getItem('key') : null;
}
/**
 * 存储用户身份key到本地存储中
 * @param key
 * @param keyExpires
 */
export function setKey(key, keyExpires = new Date().getTime() + 60 * 60 * 24 * 7 * 1000) {
  localStorage.setItem('key', key);
  localStorage.setItem('keyExpires', keyExpires);
  // 更新http的用户访问key
  http.defaults.data = extend({key: key}, http.defaults.data || {});
}