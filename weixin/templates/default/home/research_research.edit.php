<style>
.list_item,.list_item2{ padding:8px 4px;}
.list_item3{ padding:5px;}
#dy_question .anwser dd,#dy_question .anwser dt{ padding-left:40px;}
.label_box{ width:9em;}
.list_item2 .question,.list_item2 .anwser{ padding:4px;}
</style>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/date/WdatePicker.js"></script>
<div class="main_hd">
  <h2><?php echo '微调研活动-奖项设置';?></h2>
  <p class="extra_info"> <a href="?act=research&op=list&wx_id=<?php echo intval($_GET['wx_id']);?>"><?php echo '返回列表';?></a> </p>
</div>
<div class="main_bd">
  <form method="post" id="apply_form" action="?act=research&op=res_edit&wx_id=<?php echo intval($_GET['wx_id']);?>">
  	<input type="hidden" name="form_submit" value="ok" />
    <input type="hidden" name="lot_id" value="<?php echo intval($_GET['lot_id']);?>" />
    <ul>
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo '兑奖信息';?></label>
        <div class="label_form"> 
          <span>
            <?php showEditor('start_title',$output['info']['start_title'],'550px','200px','','true',false);?>
            <label for='start_title' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo '中奖提示';?></label>
        <div class="label_form"> 
          <span>
          	<?php showEditor('start_content',$output['info']['start_content'],'550px','200px','','true',false);?>
            <label for='start_content' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo '活动结束公告主题';?></label>
        <div class="label_form"> 
          <span>
          	<input type="text" class="label_input" name="end_title" id="end_title" value="<?php echo $output['info']['end_title'];?>">
            <label for='end_title' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo '活动结束说明';?></label>
        <div class="label_form"> 
          <span>
          	<?php showEditor('end_content',$output['info']['end_content'],'550px','200px','','true',false);?>
            <label for='end_content' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo '预计参与人数';?></label>
        <div class="label_form"> 
          <span>
          	<input type="text" class="label_input" name="join_count" id="join_count" value="<?php echo $output['info']['join_count'];?>">
            &nbsp;&nbsp;<br />
            <label style="font-size:12px; color:#999">预计参与人数直接影响抽奖概率：中奖概率 = 奖品总数/预计参与人数 &nbsp;&nbsp;如果要确保任何时候都100%中奖建议设置为1人参加，并且奖项只设置一等奖</label>
            <br />
            <label for='join_count' class='error msg_invalid' style='display:none;'></label>
          </span>      
        </div>
      </li>
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo '兑奖密码';?></label>
        <div class="label_form"> 
          <span>
          	<input type="password" class="label_input" name="lot_password" id="lot_password">
            <label for='lot_password' class='error msg_invalid' style='display:none;'></label>
          </span>      
        </div>
      </li>
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo '显示奖品数量';?></label>
        <div class="label_form"> 
          <span>
          	<input type="radio" name="isshow_lotcount" value="1" <?php echo ($output['info']['isshow_lotcount'] == 1)?'checked="checked"':'';?> /> 是 &nbsp;&nbsp;&nbsp;&nbsp;
            <input type="radio" name="isshow_lotcount" value="0" <?php echo ($output['info']['isshow_lotcount'] == 0)?'checked="checked"':'';?>/> 否
          </span>      
        </div>
      </li>
      <li class="list_item2">
      	<h2>奖项设置</h2>
      	<ul id="dy_question">
        <?php 
			$res_lot = unserialize($output['info']['lot_info']);
			$a = array("一","二","三","四","五","六","七","八","九","十");
			foreach($res_lot as $key => $val){
				if($key ==0){
		?>
        	<li class="list_item2" list_id="<?php echo $key+1;?>">
				<div class="question"><label><font style="color:red">*&nbsp;</font><?php echo $a[$key]?>等奖奖品设置：</label>
                <input type="text" placeholder="请输入奖品名称" value="<?php echo $val['jp'];?>" class="label_input" name="dyjx[<?php echo $key+1;?>][jp]" id="dyjp_<?php echo $key+1;?>" />
                <label for='dyjx[<?php echo $key+1;?>][jp]' class='error msg_invalid' style='display:none;'></label>
				</div>
				<div class="anwser"><span><font style="color:red">*&nbsp;</font><?php echo $a[$key]?>等奖奖品数量：</span>
                <input type="text" placeholder="请输入奖品数量" class="label_input" name="dyjx[<?php echo $key+1;?>][num]" id="dynum_<?php echo $key+1;?>" value="<?php echo $val['num'];?>" />
                <label for='dyjx[<?php echo $key+1;?>][num]' class='error msg_invalid' style='display:none;'></label>
                </div>
            </li>
            <?php }else{ ?>
            <li class="list_item2" list_id="<?php echo $key+1;?>">
				<div class="question"><label><?php echo $a[$key]?>等奖奖品设置：</label>
                <input type="text" placeholder="请输入奖品名称" value="<?php echo $val['jp'];?>" class="label_input" name="dyjx[<?php echo $key+1;?>][jp]" id="dyjp_<?php echo $key+1;?>" />
                <label for='dyjx[<?php echo $key+1;?>][jp]' class='error msg_invalid' style='display:none;'></label>
				</div>
				<div class="anwser"><?php echo $a[$key]?>等奖奖品数量：</span>
                <input type="text" placeholder="请输入奖品数量" class="label_input" name="dyjx[<?php echo $key+1;?>][num]" id="dynum_<?php echo $key+1;?>" value="<?php echo $val['num'];?>" />
                <label for='dyjx[<?php echo $key+1;?>][num]' class='error msg_invalid' style='display:none;'></label>
                </div>
            </li>
            <?php } ?>
        <?php }?>
        </ul>
        <div><a href="javascript:void(0);" id="addq">添加奖项</a></div>
      </li>      
      <li>
        <div class="btn_bar">
        <input type="submit" class="btn_input" value="<?php echo $lang['nc_save'];?>"></div>
      </li>
    </ul>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script> 
<script type="text/javascript">
var i = <?php echo count($res_lot);?>;
var flag = 0;
var a = new Array("一","二","三","四","五","六","七","八","九","十");
$(function(){	
	$('#addq').click(function(){
		flag = $('#dy_question').find('li').size();
		if(flag < 10){
			addquestion();
		}else{
			alert('最多可以添加10个奖项等级');
		}
	});
	$("#apply_form").validate({//验证表单
        rules: {
        	start_title: {
				required:true
            },
			start_content: {
				required:true
            },
			end_title: {
				required:true
            },
			end_content: {
				required:true
            },
			join_count: {
				required:true,
				number:true,
				min:1,
				max:999999999
            }
        },
        messages:{
        	start_title:{
        		required:'<?php echo '兑奖信息不能为空';?>'
            },
			start_content: {
				required:'<?php echo '中奖提示不能为空';?>'
            },
			end_title: {
				required:'<?php echo '活动结束公告主题不能为空';?>'
            },
			end_content: {
				required:'<?php echo '活动结束说明不能为空';?>'
            },
			join_count: {
				required:'<?php echo '预计参与人数不能为空';?>',
				number:'<?php echo '预计参与人数1-999999999之间的数字';?>',
				min:'<?php echo '预计参与人数1-999999999之间的数字';?>',
				max:'<?php echo '预计参与人数1-999999999之间的数字';?>'
            }
        }
	});
});

//添加奖项
function addquestion(){
	i += 1;
	var html = '<li class="list_item2" list_id="'+i+'">'
			  +'<div class="question"><label>&nbsp;&nbsp;'+a[flag]+'等奖奖品设置：</label>'
              +'<input type="text" placeholder="请输入奖品名称" value="" class="label_input" name="dyjx['+i+'][jp]" id="dyjp_'+i+'" />'
			  +'&nbsp;&nbsp;&nbsp;&nbsp; <a href="javascript:void(0)" onClick="del('+i+')">删除</a></div>'
			  +'<div class="anwser"><span>&nbsp;&nbsp;'+a[flag]+'等奖奖品数量：</span>'
              +'<input type="text" placeholder="请输入奖品数量" class="label_input" name="dyjx['+i+'][num]" id="dynum_'+i+'" value="" />'
              +'</div></li>';
	$('#dy_question').append(html);
				
}

//删除奖项
function del(n){
	if($('#dy_question').find('li').size()>1){
		$('#dy_question').find('li[list_id='+n+']').remove();
		var k = 0;
		$('#dy_question li').each(function(){
			if(k != 0){
				$(this).find('.question label').html('&nbsp;&nbsp;'+a[k]+'等奖奖品数量：');
				$(this).find('.anwser span').html('&nbsp;&nbsp;'+a[k]+'等奖奖品数量：');	
			}			
			k++;
		});
	}else{
		alert('至少保留一个奖项等级');
	}
}
</script> 
