<?php defined('ByShopWWI') or exit('Access Invalid!');?>
<link href="<?php echo MALL_TEMPLATES_URL;?>/css/wwi_new_index.css" rel="stylesheet" type="text/css">
<link href="<?php echo MALL_TEMPLATES_URL;?>/css/wwi-main.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?php echo MALL_RESOURCE_SITE_URL;?>/js/home_index.js" charset="utf-8"></script>
</script><script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/waypoints.js"></script>
<style type="text/css">
.category { display: block !important; }
</style>
<div class="clear"></div>

<!-- HomeFocusLayout Begin-->
<div class="banner">
<?php echo $output['web_html']['index_pic'];?>
    <!--进入页面默认的三个专题推荐-->
<div class="fastZT fastZT1">
<div class="hoverTab topbox">
	<div class="tabCont">
      <a target="_blank" href="javascript:void(0)" class="now">公告</a>
      <a target="_blank" href="javascript:void(0)" class="">入驻</a>
    </div>
    <div style="display: block;" class="hoverCont "><ul class=" noticeList"> <?php if(!empty($output['show_article']['notice']['list']) && is_array($output['show_article']['notice']['list'])) { ?> <?php foreach($output['show_article']['notice']['list'] as $val) { ?><li><a rel="nofollow" href="<?php echo empty($val['article_url']) ? urlMember('article', 'show',array('article_id'=> $val['article_id'])):$val['article_url'] ;?>" target="_blank">【公告】<?php echo $val['article_title']; ?></a></li><?php }} ?></ul></div>
    <div style="display: none;" class="hoverCont "><a href="<?php echo urlMall('show_joinin', 'index');?>" title="申请商家入驻；已提交申请，可查看当前审核状态。" class="store-join-btn" target="_blank">&nbsp;</a><a href="<?php echo urlMall('seller_login','show_login');?>" target="_blank" class="store-join-help"><i class="icon-cog"></i>登录商家管理中心</a></div>
</div>
<div class="topbox2"><ul class="featuresList clearfix"><li><a rel="nofollow" href="<?php echo urlMall('show_joinin', 'index');?>" target="_self"><i class="i_ico01"></i>招商入驻</a></li><li><a rel="nofollow" href="<?php echo urlMall('seller_login','show_login');?>" target="_self"><i class="i_ico02"></i>商家管理</a></li><li><a rel="nofollow" href="<?php echo urlmall('special','special_detail', array('special_id'=>'1'));?>" target="_self"><i class="i_ico03"></i>消费保障</a></li><li><a rel="nofollow" href="<?php echo urlMall('invite', 'index');?>" target="_self"><i class="i_ico04"></i>推广返利</a></li><li><a rel="nofollow" href="<?php echo DELIVERY_SITE_URL;?>" target="_self"><i class="i_ico05"></i>物流自提</a></li><li><a rel="nofollow" href="<?php echo WAP_SITE_URL;?>" target="_self"><i class="i_ico06"></i>手机专享</a></li></ul></div>
    </div>
    <!--//进入页面默认的三个专题推荐-->
</div>
<!--HomeFocusLayout End-->

<!--限时特价随机--> 
<?php if(!empty($output['xianshi_item']) && is_array($output['xianshi_item'])) { ?>
<div class="wrapper partTit"><p><span><em class="ft31"><a href="<?php echo urlMall('promotion','list');?>" target="_blank"> <i class="pink">限时</i>特价 </a> </em> <i class="eng">ON SALE</i> </span> </p></div>
<div class="boxItem1_index wrapper zoom hoverTab">
    <ul style="display:block" class="hoverCont">
    	<?php foreach($output['xianshi_item'] as $val) { ?>
               <li><div class="act-img-wrap"><a href="<?php echo urlMall('promotion','index',array('id'=>$val['xianshi_id']));?>" target="_blank"><img  width="688" height="190" src="<?php echo xsthumb($val['xianshi_image1']);?>" class="">
    								</a>
    								<div class="countdown-tag">
                                    <span class="tick-logo"></span>
                                    <p class="infoTit1 time-remain" count_down="<?php echo $val['end_time']-TIMESTAMP;?>"><i></i><em time_id="d">0</em>天<em time_id="h">0</em>时<em time_id="m">0</em>分<em time_id="s">0</em>秒</p>
                                        <span class="arrow-circle"></span>
									</div>
                				</div>
                				<div class="act-detail-wrap">
                					<div class="inner">
    									<img width="150" height="35" src="<?php echo xsthumb($val['xianshi_image2']);?>" class="">
                						<h3><a  href="<?php echo urlMall('promotion','index',array('id'=>$val['xianshi_id']));?>" target="_blank"><?php echo $val['xianshi_explain']; ?></a>
    									</h3>
                						<span class="discount">低至<?php echo $val['xianshi_discount']; ?>折</span>
                					</div>
                				</div>
                			</li>
        <?php } ?>
            </ul>
</div><?php } ?>
<!--限时特价随机end-->
<!--今日主题优惠-->
<?php echo $output['web_html']['index_sale'];?>
<!--今日主题优惠end-->

<!--品牌及店铺推荐-->
<div class="wrap partTit">
    <p>
        	<span>
            	<em class="ft31"><a href="" target="_blank">热门品牌店铺</a></em>
                <i class="eng">MALL&amp;BRAND</i>
            </span>
    </p>
</div>
<div class="boxItem4 wrapper">
    <div class="hotTab">
        <span class="now">热门品牌</span>
        <span class="">热门店铺</span>
    </div>
    <div class="hotCont" style="display: block;">
        <a class="checkMore">换一换<i></i></a>        <div class="focusImg focusImg1">
            <div class="inner wwi-new-slider"><?php echo rec(2,'html');?></div>
        </div>
        <div class="items hosItem">

        	<?php if(!empty($output['brand_r'])){$i = 0;?>
        		<div class="showItem now" style="display:block;">
        	<?php foreach($output['brand_r'] as $key=>$brand_r){ if ($i == 12){ echo '</div><div class="showItem">'; $i = 1;} else{ echo ' '; $i++; }?>
        			<a href="<?php echo urlMall('brand', 'list',array('brand'=>$brand_r['brand_id']));?>" title="<?php echo $brand_r['brand_name'];?>" target="_blank"><img shopwwi-url="<?php echo brandImage($brand_r['brand_pic']);?>"  rel='lazy' src="<?php echo MALL_SITE_URL;?>/img/loading.gif" alt="<?php echo $brand_r['brand_name'];?>"></a>
        			<?php }?></div> 
        		<?php } ?>
                    </div>

    </div>
    <div class="hotCont" style="display: none;">
        <a class="checkMore">换一换<i></i></a>        <div class="focusImg focusImg2">
            <div class="inner wwi-new-slider"><?php echo rec(1,'html');?></div>
        </div>
        <div class="items docItem">
                        <div class="showItem" style="display: block;">
        <?php if(!empty($output['store_list']) && is_array($output['store_list'])) {$i=0 ?>
        	<?php foreach($output['store_list'] as $val) { if ($i == 6){ echo '</div><div class="showItem">'; $i = 1;} else{ echo ' '; $i++; } ?>
                                <a href="<?php echo urlMall('show_store','', array('store_id'=>$val['store_id']),$val['store_domain']);?>" target="_blank">
                    <p class="doc-img"><span><img alt="<?php echo $val['store_name'];?>" src="<?php echo getStoreLogo($val['store_label']);?>"></span><i></i></p>
                    <p class="doc-name"><?php echo $val['store_name'];?></p>
                    <p class="hos-name"><?php echo $val['store_address'];?></p>
                    <span class="ordNum"><?php echo $val['goods_count'];?>件商品</span>
                </a><?php }}?>
                    </div>
    </div></div><div class="wwi-right rank"><p class="rank-tit">热销排行榜</p><?php if(!empty($output['goods_list']) && is_array($output['goods_list'])) {$i=0; ?><?php foreach($output['goods_list'] as $val) {$i++; ?> <a href="<?php echo urlMall('goods','index',array('goods_id'=> $val['goods_id']));?>" class="rank-item" target="_blank"><span class="babyImg"><img shopwwi-url="<?php echo thumb($val, 240);?>" rel='lazy' src="<?php echo MALL_SITE_URL;?>/img/loading.gif" alt="<?php echo $val['goods_name']; ?>"><?php if ($i < 4) { ?><i class="icon"><?php echo $i;?></i><?php }?></span><div class="babyInfo"><p><?php echo $val['goods_name']; ?></p> <span><i class="pink wwi-left">￥<?php echo $val['goods_promotion_price']; ?></i><i class="c999 wwi-right"><?php echo $val['goods_salenum']; ?>人购买</i></span></div> </a><?php }}?> </div></div>

<!--品牌及店铺推荐end-->

<div class="wrapper">
  <div class="mt10">
<?php echo loadadv(9,html);?>
</div>
<!--StandardLayout Begin--> 
<?php echo $output['web_html']['index'];?>
<!--StandardLayout End-->
</div>
<!--快捷导航-->
<div id="nav_box"><ul><div class="m-logo"></div> 
	    <?php if (is_array($output['lc_list']) && !empty($output['lc_list'])) {$i=0 ?>
    <?php foreach($output['lc_list'] as $v) { $i++?>
    <li class="nav_Sd_<?php echo $i;?> <?php if($i==1) echo 'hover'?>"> <a class="word" href="javascript:;"><em class="em"><?php echo $v['value']?></em><?php echo $v['name']?></a></li>
    <?php }} ?>
	</ul></div>
<!--快捷导航end-->
<!--晒单评价-->
<div class="wrapper partTit">
    <p>
        	<span>
            	<em class="ft31">
                    <a href="<?php echo urlMall('show_groupbuy','index');?>" target="_blank">
                    晒单评价
                    </a>
                </em>
                <i class="eng">NEW ORDERSHARE</i>
            </span>
    </p>
</div>
<div class="wrapper zoom">
	<?php if(!empty($output['group_list']) && is_array($output['group_list'])) { ?><div class="groupbuy boxItem5 wwi-left"><ul><?php foreach($output['group_list'] as $val) { ?><li class="wwi-left">
                <a target="_blank" href="<?php echo urlMall('show_groupbuy','groupbuy_detail',array('group_id'=> $val['groupbuy_id']));?>" class="infoImg"><img alt="<?php echo $val['groupbuy_name']; ?>" shopwwi-url="<?php echo gthumb($val['groupbuy_image1'], 'small');?>" rel='lazy' src="<?php echo MALL_SITE_URL;?>/img/loading.gif"  style="display: block;"></a>
        <p class="infoTit1">
                        <a target="_blank" href="<?php echo urlMall('show_groupbuy','groupbuy_detail',array('group_id'=> $val['groupbuy_id']));?>" class="wwi-left"><?php echo $val['groupbuy_name']; ?></a>
        </p>
        <p class="infoTit2">
            <span class="wwi-left"><span class="time-remain" count_down="<?php echo $val['end_time']-TIMESTAMP; ?>"> <em time_id="d">0</em><?php echo $lang['text_tian'];?><em time_id="h">0</em><?php echo $lang['text_hour'];?> <em time_id="m">0</em><?php echo $lang['text_minute'];?><em time_id="s">0</em><?php echo $lang['text_second'];?> </span> </span>
        </p>
        <p class="infoTit2 infoIit3">
            <span class="wwi-left"><i>￥</i><?php echo $val['groupbuy_price']; ?>                <del class="ft12 c999">￥<?php echo $val['goods_price']; ?></del>                    </span></p>
            </li><?php } ?></ul></div><?php } ?>
            
    <ul class="boxItem2 boxItem2-1 zoom" style="display:block">
    	
    <?php if(!empty($output['goods_evaluate_info']) && is_array($output['goods_evaluate_info'])){?>
    	<?php foreach($output['goods_evaluate_info'] as $k=>$v){?>
                <li>
                        <a href="<?php echo urlMall('goods','comments_list',array('goods_id'=> $v['geval_goodsid']));?>" target="_blank">
                <p class="infoImg wwi-left"><img alt="" shopwwi-url="<?php echo strpos($v['goods_pic'],'http')===0 ? $v['goods_pic']:UPLOAD_SITE_URL."/".ATTACH_GOODS."/".$v['geval_storeid']."/".$v['geval_goodsimage'];?>"   rel='lazy' src="<?php echo MALL_SITE_URL;?>/img/loading.gif" style="display: block;"></p>
                <div class="wwi-right">
                    <p class="infoItem2"><?php echo str_cut($v['geval_frommembername'],2).'***';?></p>
                    <p class="infoItem3 c999"><?php echo $v['geval_content'];?></p>
                    <p class="price pink">
                        <i class="ft14">￥</i>
                        <b class="ft20"><?php echo $v['geval_goodsprice'];?></b>
                        <b class="butie"><?php echo $v['geval_scores'];?></b></p>
                </div>
            </a>
        </li>
<?php }}?>	
 </ul>
</div>
<!--晒单评价end-->
<!--网店运维专题布局--> 
<div class="wrapper partTit ">
    <p>
        	<span>
            	<em class="ft31"><a href="<?php echo urlMall('special','special_list');?>" target="_blank">抢购会场专题</a></em>
                <i class="eng">TOPICS</i>
            </span>
    </p>
</div>
<div class="wrapper zoom">
    <div class="topics zoom">
    	<?php if(!empty($output['special_list']) && is_array($output['special_list'])) {?>
    		<?php foreach($output['special_list'] as $value) {?>
                <a href="<?php echo $value['special_link'];?>" class="boxItem8" target="_blank">
            <div class="info_img">
                <img  shopwwi-url="<?php echo getTZXSpecialImageUrl($value['special_image']);?>" rel='lazy' src="<?php echo MALL_SITE_URL;?>/img/loading.gif" alt="<?php echo $value['special_title'];?>">
            </div>
            <div class="infoTxt">
                <p class="info_tit1"><?php echo $value['special_title'];?></p>
                <p class="info_tit2"><?php echo $value['special_stitle'];?></p>
                <span class="info_tit3"><?php echo date('Y-m-d',$value['special_modify_time']);?></span>
            </div>
        </a><?php }} ?>
               
            </div>
</div>
<!--网店运维专题布局end--> 
