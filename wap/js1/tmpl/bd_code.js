
require(['zepto','wap.simple-plugin'],function($){
    var t_member_id = getQueryString('member_id');
    var store_id = getQueryString('store_id');
    var activity_id = getQueryString('activity_id');
    var goods_id = getQueryString('goods_id');
	$.sValid.init({
		rules:{
            card_number:"required",
			mob_name:"required",
			mob_phone:"required",
			area_info:"required",
			address:"required",
			xiaoshou_quyu:"required",
            xiaoshou_brand:"required",
            xiaoshou_name:"required",
		},
		messages:{
            card_number:"卡券号码必填!",
            mob_name:"姓名必填！",
			mob_phone:"手机号必填！",
			area_info:"地区必填！",
			address:"街道必填！",
            xiaoshou_quyu:"销售区域必选！",
            xiaoshou_brand:"销售品牌必填！",
            xiaoshou_name:"销售员姓名必填！",
		},
		callback:function (eId,eMsg,eRules){
			if(eId.length >0){
				var errorHtml = "";
				$.map(eMsg,function (idx,item){
					errorHtml += "<p>"+idx+"</p>";
				});
				errorTipsShow(errorHtml);
			}else{
				errorTipsHide();
			}
		}  
	});
	if(activity_id == ''){
        $.sDialog({
            skin:"red",
            content:"请重新扫码获取活动编号",
            okBtn:true,
            cancelBtn:true
        });
	}
	$('#header-nav').click(function(){
		$('.btn').click();
	});
	$('.btn').click(function(){
		if($.sValid()){
            var card_number = $('#card_number').val();
			var buyer_name = $('#mob_name').val();
			var buyer_phone = $('#mob_phone').val();
			var address = $('#address').val();
			var city_id = $('#area_info').attr('data-areaid2');
			var area_id = $('#area_info').attr('data-areaid');
			var area_info = $('#area_info').val();
			var xiaoshou_quyu = $('xiaoshou_quyu').val();
			var xiaoshou_brand = $('xiaoshou_brand').val();
			var xiaoshou_name = $('xiaoshou_name').val();

			$.ajax({
				type:'post',
				url:ApiUrl+"/index.php?app=bd&wwi=bd_code",
				data:{
					card_number:card_number,
				    buyer_name:buyer_name,
				    buyer_phone:buyer_phone,
				    buyer_address:area_info + ' ' + address ,
					t_member_id:t_member_id,
					xiaoshou_quyu:xiaoshou_quyu,
					xiaoshou_brand:xiaoshou_brand,
					xiaoshou_name:xiaoshou_name,
					store_id:store_id,
					goods_id:goods_id,
                    activity_id:activity_id,
				},
				dataType:'json',
				success:function(result){
					if(result.code == 200){
                        $.sDialog({
                            skin:"green",
                            content:result.datas,
                            okBtn:false,
                            cancelBtn:false
                        });
					}else{
                        $.sDialog({
                            skin:"red",
                            content:result.datas.error,
                            okBtn:false,
                            cancelBtn:false
                        });
					}
				}
			});
		}
	});

    // 选择地区
    $('#area_info').on('click', function(){
        $.areaSelected({
            success : function(data){
                $('#area_info').val(data.area_info).attr({'data-areaid':data.area_id, 'data-areaid2':(data.area_id_2 == 0 ? data.area_id_1 : data.area_id_2)});
            }
        });
    });
});