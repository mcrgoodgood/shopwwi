<?php defined('InByShopWWI') or exit('Access Invalid!');?>

<div class="main_hd">
  <h2>添加图文推荐商品</h2>
  <p class="extra_info"><a href="index.php?act=store&op=goods_manage&wx_id=<?php echo intval($_GET['wx_id']); ?>&reply_type=click_reply">图文推荐商品管理</a></p>
</div>
<form id="add_form" method="post" enctype="multipart/form-data">
<input type="hidden" name="form_submit" value="ok" />
<input type="hidden" name="store_id" value="<?php echo $output['store_id']; ?>" />
<input type="hidden" name="choose_goods" value="" />
<input type="hidden" name="choose_gc" value="" />
<input type="hidden" name="type" value="<?php echo $output['type']; ?>" />
<input type="hidden" name="curpage" value="1" />
<input type="hidden" name="allpage" value="1" />
<div class="main_bd">
<label class="label_box"><font style="color:red"></font>类型：</label>
  <div class="label_form"> 
	<span>
	<input type="radio" name="reply_type" id="reply_type_click" value="click_reply" checked ><label for="reply_type_click" style="cursor:pointer"> 点击回复素材</label>
	<input type="radio" name="reply_type" id="reply_type_auto" value="auto_reply" ><label for="reply_type_auto" style="cursor:pointer"> 自动回复素材</label>
	</span>     
  </div><br>
  <label class="label_box"><font style="color:red">*</font>主题：</label>
  <div class="label_form"> 
	<span>
	<input type="text" name="reply_title" value="" class="label_input">
	<label for='reply_title' class='error msg_invalid' style='display:none;'></label>
	</span>     
  </div><br>
  <div id="code">
  <label class="label_box"><font style="color:red">*</font>标识码：</label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="cr_code" value="" class="label_input">
	          	<label for='cr_code' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div><br></div>
  <label class="label_box">说明：</label>
  <div class="label_form"> 
	<textarea name="reply_note" style="width:350px;height:100px"></textarea>     
  </div><br>
  <label class="label_box"><font style="color:red"></font>已选择：</label>
  <div class="label_form"> 
	<span id="choose_goods_name">
	</span>     
  </div><br><br>
  <div class="tip">注：请先检索商品并勾选需要推荐的商品，点击“保存”按钮后将生成一个推荐商品的图文回复素材，最多选择10个商品。</div>
  <div class="zt">
    <span>商品分类：</span>
    <select class="goods_class" id="" >
  		<option value="0" >请选择分类</option>
  		<?php if (!empty($output['gc_list'])) { ?>
		<?php foreach ($output['gc_list'] as $val) { ?>
		<option value="<?php echo $val['gc_id']; ?>" ><?php echo $val['gc_name']; ?></option>
		<?php }} ?>
  	</select>
  	<select class="goods_class" id="cgc_one" >
  	</select>
  	<select class="goods_class" id="cgc_two" >
  	</select>
  </div>
  <div class="zt">
  <span>商品名称：</span>
    <input name="s_title" type="text" class="label_input_zt" value="<?php echo trim($_GET['s_title']); ?>"/>
    <input type="button" class="search-button" id="search" />
  </div>
  <div class="table_msg" >
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell" style="width:20%">商品图片</th>
          <th class="table_cell" style="width:40%">商品名称</th>
          <th class="table_cell" style="width:15%">商品价格</th>
          <th class="table_cell" style="width:15%">商品销量</th>
          <th class="table_cell" style="width:10%"></th>
        </tr>
      </thead>
      <tbody class="nc-tbody" id="goods_body"></tbody>
    </table>
  </div>
  <div class="pagination">
	<ul>
	<li>
	<a class="more_goods" type="first_page" style="cursor:pointer"><span>首页</span></a>
	</li>
	<li>
	<a class="more_goods" type="prev_page" style="cursor:pointer"><span>上一页</span></a>
	</li>
	<li><span id="pageshow">1/35</span></li>
	<li>
	<a class="more_goods" type="next_page" style="cursor:pointer"><span>下一页</span></a>
	</li>
	<li>
	<a class="more_goods" type="last_page" style="cursor:pointer"><span>末页</span></a>
	</li>
	</ul>
  </div>
  <div class="btn_bar">
	<input class="btn_input" type="submit" value="保存">
  </div>
  <div class="loading">商品信息加载中，请稍候...</div>
</div>
</form>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script>
<script type="text/javascript">
$(function(){
	$('.table_msg').hide();
	$('.btn_bar').hide();
	$('.loading').hide();
	$('#cgc_one').hide();
	$('#cgc_two').hide();
	$('.pagination').hide();
	//ajax加载商品信息
	$('#search').click(function(){
		$('.loading').show();
		$('#goods_body').empty();
		var sgcid = $('input[name="choose_gc"]').val();
		var kw = $('input[name="s_title"]').val();
		var type = $('input[name="type"]').val();
		var sid = $('input[name="store_id"]').val();
		$.getJSON('index.php?act=store&op=ajax_getgoods&wx_id=<?php echo intval($_GET['wx_id']); ?>&gc_id='+sgcid+'&keyword='+kw+'&type='+type+'&sid='+sid, function(result){
			$('.loading').hide();
			if (result.done) {
				$.each(result.goods_list, function(key,val){
					$('#goods_body').append("<tr><td><img src='"+val.goods_image_url+"' width='80' height='80' border='0' style='margin-top:10px' /></td><td>"+val.goods_name+"</td><td>"+val.goods_price+"元</td><td>"+val.goods_salenum+"</td><td><a class='choose_goods' gi='"+val.goods_id+","+val.goods_name+","+val.goods_image_url+"' goods_id='"+val.goods_id+"' name='"+val.goods_name+"' style='cursor:pointer'>选择</a></td></tr>");
				});
				$('.table_msg').show();
	        	$('.btn_bar').show();
	        	$('input[name="allpage"]').val(result.allpage);
	        	$('#pageshow').text(result.pageshow);
	        	$('.pagination').show();
			} else {
				alert(result.msg);
			}
		});
	});
	$('.more_goods').click(function(){
		var type = $(this).attr("type");
		var curpage = parseInt($('input[name="curpage"]').val());
		var allpage = parseInt($('input[name="allpage"]').val());
		var loadpage = 0;
		if (type == "first_page") {
			loadpage = 1;
		}
		if (type == "prev_page") {
			loadpage = curpage - 1;
			if (loadpage == 0) {
				alert("已是第一页");
				return;
			}
		}
		if (type == "next_page") {
			loadpage = curpage + 1;
			if (loadpage > allpage) {
				alert("已是最后一页");
				return;
			}
		}
		if (type == "last_page") {
			loadpage = allpage;
		}
		$('.loading').show();
		$('#goods_body').empty();
		$('input[name="curpage"]').val(loadpage);
		$('.pagination').hide();
		$('.btn_bar').hide();
		var sgcid = $('input[name="choose_gc"]').val();
		var kw = $('input[name="s_title"]').val();
		var type = $('input[name="type"]').val();
		var sid = $('input[name="store_id"]').val();
		$.getJSON('index.php?act=store&op=ajax_getgoods&wx_id=<?php echo intval($_GET['wx_id']); ?>&gc_id='+sgcid+'&keyword='+kw+'&type='+type+'&sid='+sid+'&curpage='+loadpage, function(result){
			$('.loading').hide();
			$('.pagination').show();
			if (result.done) {
				$.each(result.goods_list, function(key,val){
					$('#goods_body').append("<tr><td><img src='"+val.goods_image_url+"' width='80' height='80' border='0' style='margin-top:10px' /></td><td>"+val.goods_name+"</td><td>"+val.goods_price+"元</td><td>"+val.goods_salenum+"</td><td><a class='choose_goods' gi='"+val.goods_id+","+val.goods_name+","+val.goods_image_url+"' goods_id='"+val.goods_id+"' name='"+val.goods_name+"' style='cursor:pointer'>选择</a></td></tr>");
				});
				$('.table_msg').show();
	        	$('.btn_bar').show();
	        	$('.pagination').show();
	    		$('.btn_bar').show();
	        	$('input[name="allpage"]').val(result.allpage);
	        	$('#pageshow').text(result.pageshow);
			} else {
				alert(result.msg);
			}
		});
	});	
	$("#add_form").validate({
        rules: {
        	reply_title: {
				required:true
            }
        },
        messages:{
        	reply_title:{
        		required:'请填写标题'
            }
        }
	});
	$('#checkAll').click(function(){
		if($(this).attr('checked') == 'checked'){
			$('input[name="choose_goods_id[]"]').attr('checked','checked');
		}else{
			$('input[name="choose_goods_id[]"]').removeAttr('checked');
		}
	});
	$('input[name="reply_type"]').change(function(){
		if ($(this).val() == "click_reply"){
			$('#code').show();
		} else {
			$('#code').hide();
		}
	});
	<?php if ($output['type'] == 1) { ?>
	$('.goods_class').live('change',function(){
		var gcid = $(this).val();
		var id = $(this).attr("id");
		$('input[name="choose_gc"]').val(gcid);
		//ajax加载平台子分类
		if (gcid != "0" && $(this).attr("id") != "cgc_two") {
			$.getJSON('index.php?act=store&op=ajax_getmallgc&wx_id=<?php echo intval($_GET['wx_id']); ?>&gc_id='+gcid+'&goods=1&nowid='+id, function(result){
		    	if(result.done){
		    		if (result.nowid == "") {
		    			$('#cgc_one').empty();
	        			$('#cgc_one').append("<option value='0' >请选择分类</option>");
	        		} else {
	        			$('#cgc_two').empty();
	        			$('#cgc_two').append("<option value='0' >请选择分类</option>");
	        		}
		        	$.each(result.gc_list, function(key,val){
		        		if (result.nowid == "") {
		        			$('#cgc_one').append("<option value='"+ val.gc_id +"' >"+ val.gc_name +"</option>");
		        			$('#cgc_one').show();
		        			$('#cgc_two').hide();
		        		} else {
		        			$('#cgc_two').append("<option value='"+ val.gc_id +"' >"+ val.gc_name +"</option>");
		        			$('#cgc_two').show();
		        		}
		            });
		    	}
		    });
		}
	});
	<?php } else { ?>
	$('.goods_class').change(function(){
		$('input[name="choose_gc"]').val($(this).val());
	});
	<?php } ?>
	//选择商品
	var goods_arr = new Array();
	$('.choose_goods').live('click',function(){
		if (goods_arr.length == 10) {
			alert("请最多选择10件商品");
			return;
		}
		var goods_info = $(this).attr("gi");
		var goods_name = $(this).attr("name");
		var goods_id = $(this).attr("goods_id");
		for(var i=0;i<goods_arr.length;i++) {
			if (goods_arr[i] == goods_info) {
				alert("已选择了该商品");
				return;
			}
		}
		goods_arr.push(goods_info);
		$('#choose_goods_name').append("<li id='cg_"+goods_id+"'><font>"+goods_name+"</font>&nbsp;&nbsp;<a gid='"+goods_id+"' class='del_goods' gi='"+goods_info+"' style='cursor:pointer'>删除</a><br></li>");
		var cgstr = goods_arr.join("|||||");
		$('input[name="choose_goods"]').val(cgstr);
	});
	$('.del_goods').live('click',function(){
		var goods_info = $(this).attr("gi");
		var goods_id = $(this).attr("gid");
		goods_arr.remove(goods_info);
		$('#cg_'+goods_id).remove();
		var cgstr = goods_arr.join("|||||");
		$('input[name="choose_goods"]').val(cgstr);
	});
})
Array.prototype.indexOf = function(val) {
            for (var i = 0; i < this.length; i++) {
                if (this[i] == val) return i;
            }
            return -1;
        };
        Array.prototype.remove = function(val) {
            var index = this.indexOf(val);
            if (index > -1) {
                this.splice(index, 1);
            }
        };
</script>