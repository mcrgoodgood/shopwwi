define(function(){
    'use strict';

    $('[hover-slideout]').each(function(){
        $(this).hover(function(e){
            $(this).find('[hover-box]').css(induction_position($(this), e)).stop(true, true).animate({"left":0, "top":0}, 200);
        },function(e){
            $(this).find('[hover-box]').stop(true, true).animate(induction_position($(this), e), 200);
        });
    });

    //鼠标进入方向
    function induction_position(elem, e){
        var w = elem.width(), h = elem.height(), direction = 0, obj = {};
        /** 计算x和y得到一个角到elem的中心，得到相对于x和y值到div的中心 **/
        var x = (e.pageX - elem.offset().left - (w / 2)) * (w > h ? (h / w) : 1);
        var y = (e.pageY - elem.offset().top - (h / 2)) * (h > w ? (w / h) : 1);

        direction = Math.round((((Math.atan2(y, x) * (180 / Math.PI)) + 180) / 90) + 3) % 4;

        switch(direction) {
            case 0://from top
                obj.left = 0;
                obj.top = "-100%";
                break;
            case 1://from right
                obj.left = "100%";
                obj.top = 0;
                break;
            case 2://from bottom
                obj.left = 0;
                obj.top = "100%";
                break;
            case 3://from left
                obj.left = "-100%";
                obj.top = 0;
                break;
        }
        return obj;
    }
});




