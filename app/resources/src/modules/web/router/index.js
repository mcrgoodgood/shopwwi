import WebLayout from '../pages/layouts/Default.vue';

import DefaultIndex from '../pages/default/Index.vue';

import UserLogin from '../pages/user/Login.vue';
import UserRegister from '../pages/user/Register.vue';
import UserPasswordReset from '../pages/user/PasswordReset.vue';

export default {
  path: '/',
  component: WebLayout,
  children: [
    { path: '/', component: DefaultIndex },
    { path: 'login', component: UserLogin },
    { path: 'register', component: UserRegister },
    { path: 'password-reset', component: UserPasswordReset },
  ]
};