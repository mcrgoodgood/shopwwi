<div class="main_hd">
  <h2>商品管理</h2>
  <p class="extra_info"><a href="?act=product&op=addProduct&wx_id=<?php echo intval($_GET['wx_id']);?>"><?php echo $lang['nc_add'];?></a></p>
</div>
<div class="main_bd">
  <div class="table_msg">
  	<form method="post" id="list_form">
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell"></th>
          <th class="table_cell time asc">商品名称</th>
          <th class="table_cell tradeId">商品价格</th>
          <th class="table_cell goods">商品状态</th>
		  <th class="table_cell goods">发布时间</th>
          <th class="table_cell desc"><?php echo $lang['nc_handle'];?></th>
        </tr>
      </thead>
      <tbody class="nc-tbody">
        <?php if(!empty($output['list'])){?>
        <?php foreach($output['list'] as $key=>$val){?>
        <tr>
          <td><input type='checkbox' value="<?php echo $val['product_id']?>" class="checkitem"></td>
          <td><?php echo $val['product_name'];?></td>
          <td><?php echo $val['product_price']?></td>
          <td><?php if($val['product_state'] == '0'){ echo '上架';}else{ echo '下架';} ?></td>
          <td><?php echo date("Y-m-d H:i",$val['product_time']);?></td>
		  <td class="last">
		  	<a href="?act=product&op=editProduct&wx_id=<?php echo intval($_GET['wx_id']);?>&product_id=<?php echo $val['product_id'];?>"><?php echo $lang['nc_edit'];?></a>
		  	<a href="javascript:;" onclick="javascript:submit_delete(<?php echo $val['product_id'];?>);"><?php echo $lang['nc_delete'];?></a>
		  </td>
        </tr>
        <?php }?>
        <?php }else{?>
		<tr>
			<td colspan="20" valign="middle" ><div class="norecord"><span><?php echo $lang['nc_record'];?></span></div></td>
		</tr>
        <?php }?>
      </tbody>
  	  <tfoot class="nc-footer">
       <tr>
      	<td class="table_cell">
      		<input type="hidden" name="product_id" value="">
      		<input type='checkbox' class="checkall">
      	</td>
      	<td colspan='6' class="table_cell last">       
	        <a onclick="javascript:submit_delete_batch();"  type='button' class="btn" href="JavaScript:void(0);"><span><?php echo $lang['nc_delete'];?></span></a>
      	</td>
        </tr>
      </tfoot>
    </table>
    <input type="hidden" name="message_id" value="">
    </form>
  </div><div class="pagination"> <?php echo $output['show_page'];?> </div>
</div>

<script type="text/javascript">
	$(function(){
		$('.checkall').click(function(){
			if($(this).attr('checked') == 'checked'){
				$('.checkitem').attr('checked','checked');
			}else{
				$('.checkitem').removeAttr('checked');
			}
		});
	});
	
	function submit_delete_batch(){
	    /* 获取选中的项 */
	    var items = '';
	    $('.checkitem:checked').each(function(){
	        items += this.value + ',';
	    });
	    if(items != '') {
	        items = items.substr(0, (items.length - 1));
	        submit_delete(items);
	    } else {
	        alert('请选择选项');
	    }
	}

	function submit_delete(id){
	    if(confirm('确认审核操作?')) {
	       	$('#list_form').attr('action','index.php?act=product&op=delProduct&wx_id='+'<?php echo intval($_GET['wx_id']);?>');
	        $("input[name=product_id]").val(id);
	        $('form').submit();
	    }
	}
</script>




