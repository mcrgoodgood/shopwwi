<?php
defined('InByShopWWI') or exit('Access Invalid!');
//常用文字
$lang['error']					= '在处理您的请求时出现了问题:<br />';
$lang['cur_location']			= '当前位置';
$lang['wrong_argument']			= '参数错误';
$lang['nc_common_op_repeat'] 	= '您的操作过于频繁，请稍后再试';
$lang['no_record']				= '暂无符合条件的记录';
$lang['no_login']				= '您还没有登录';
$lang['welcome_to_site']		= '欢迎来到';
$lang['nc_common_loading']		= '加载中...';
$lang['nc_common_shipping_free']= '（免运费）';
$lang['nc_buying_goods']			= "已买到的商品";
$lang['nc_favorites_goods']			= "收藏的商品";
$lang['nc_favorites']				= "我的收藏";
$lang['nc_notallowed_login']		= "您的账号已经被管理员禁用，请联系平台客服进行核实";
$lang['nc_customer']				= "客户";
$lang['nc_system_auto']				= "系统自动";
$lang['nc_cartgoods']				= '购物车商品';
$lang['nc_my_collection']			= '我的收藏';
$lang['nc_member_center']			= '会员中心';
$lang['nc_nothing']					= "无";
$lang['nc_index']					= "首页";
$lang['nc_logout']                  = '退出';
$lang['nc_guest']                   = '游客';
$lang['nc_login']                   = '登录';
$lang['nc_register']                = '注册';
$lang['nc_otherlogintip']			= "您可以用合作伙伴账号登录：";
$lang['nc_otherlogintip_sina']		= "新浪微博";
$lang['nc_record']					= '暂无符合条件的数据记录';
$lang['nc_show']					= '显示';

//验证码
$lang['nc_checkcode']			= '验证码';
$lang['wrong_null']				= '请填写验证码';
$lang['wrong_checkcode']		= '验证码错误';
$lang['wrong_checkcode_change']	= '点击更换验证码';

//标点 
$lang['nc_colon']			= '：';
$lang['nc_comma']           = '，';
$lang['nc_sign_multiply']	= "×";
$lang['nc_openparen']       = '（';
$lang['nc_closeparen']      = '）';

//操作提示
$lang['nc_back_to_pre_page']    = '返回上一页';
$lang['nc_status']				= '状态';
$lang['nc_ensure_del']			= '您确定要删除吗？';
$lang['nc_common_op_succ'] 		= '操作成功';
$lang['nc_common_op_fail'] 		= '操作失败';
$lang['nc_common_del_succ'] 	= '删除成功';
$lang['nc_common_del_fail'] 	= '删除失败';
$lang['nc_common_save_succ'] 	= '保存成功';
$lang['nc_common_save_fail'] 	= '保存失败';
$lang['nc_please_choose']		= '请选择';

//操作文字
$lang['nc_search']	 			= '搜索';
$lang['nc_handle']				= '操作';
$lang['nc_submit']				= '提交';
$lang['nc_save']	 			= '保存';
$lang['nc_ok']					= '确定';
$lang['nc_select']				= '选择';
$lang['nc_upload']				= '上传';
$lang['nc_delete']				= '删除';
$lang['nc_filter'] 				= '筛选';
$lang['nc_add']					= '添加';
$lang['nc_edit']				= '编辑';
$lang['nc_close']				= '关闭';
$lang['nc_select_all']			= '全选';
$lang['nc_view']				= '查看';

//状态文字
$lang['nc_yes']				= '是';
$lang['nc_no']				= '否';

//量词
$lang['currency']			= '&yen;';
$lang['currency_zh']		= '元';
$lang['piece']				= '件';
$lang['nc_month']			= "月";
$lang['nc_day']				= "日";
$lang['nc_times']			= "次";
$lang['nc_points']			= '分';

//配送方式
$lang['transport_type_py'] 				= '平邮';
$lang['transport_type_kd'] 				= '快递';
$lang['transport_type_em'] 				= 'EMS';

//footer中的文字
$lang['nc_page_execute']	= "页面执行";
$lang['nc_second']			= "秒";
$lang['nc_query']			= "查询";
$lang['nc_online']			= "在线";
$lang['nc_person']			= "人";
$lang['nc_enabled']			= "已启用";
$lang['nc_disabled']		= "已禁用";
$lang['nc_memory_cost']		= "占用内存";
$lang['nc_credit_explain']  = "卖家解释";

//分页
$lang['first_page'] = '首页';
$lang['last_page'] = '末页';
$lang['pre_page'] = '上一页';
$lang['next_page'] = '下一页';

$lang['nc_use'] = '启用';
$lang['nc_not_use'] = '不启用';
$lang['nc_open'] = '开启';
$lang['nc_not_open'] = '未开启';
?>