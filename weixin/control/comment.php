<?php
/**
 * 默认展示页面
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
define('MYSQL_RESULT_TYPE',1);
class commentControl extends BaseHomeControl{
	
	public function __construct(){
		parent::__construct();
		Tpl::output('sign','comment');
	}

	public function indexOp(){
		$this->listOp();
	}
	
	/*
	 * 评论列表
	 */
	public function listOp(){
		$model = Model();
		$list = $model->table('comment,product')->field("comment.item_id,comment.fans_nickname,comment.comment_time,product.product_name")->join('left')->on("comment.item_id = product.product_id")->where(array('product.wx_id'=>intval($_GET['wx_id'])))->page(15)->order('comment.comment_time desc')->select();
		Tpl::output('list',$list);
		Tpl::showpage('comment.list');
	}
	
	/*
	 * 评论详细
	 */
	public function detailOp(){
		$model = Model();
		$comment = $model->table('comment')->where(array('comment_id'=>intval($_GET['comment_id'])))->find();
		Tpl::output('comment',$comment);
		Tpl::showpage('comment.detail');
	}
	
	/*
	 * 删除评论
	 */
	public function delCommentOp(){		
		$model = Model();
		$res = $model->table('comment')->where(array('comment_id'=>array('in',trim($_POST['comment_id']))))->delete();
		if($res){
			showMessage('删除评论成功','?act=comment&op=list&wx_id='.intval($_GET['wx_id']),'succ');
		}else{
			showMessage('删除评论失败','?act=product&op=list&wx_id='.intval($_GET['wx_id']),'error');
		}
		
	}
}