<?php
/**
 * 默认展示页面
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
define('MYSQL_RESULT_TYPE',1);
class messageControl extends BaseHomeControl{
	
	public function __construct(){
		Language::read('weixin_message');
	}

	public function indexOp(){
		$this->listOp();
	}
	
	/*
	 * 消息列表
	 */
	public function listOp(){
		$model = Model();
		$list = $model->table('message')->select();
		
		Tpl::output('list',$list);
		Tpl::showpage('message.list');
	}

	/*
	 * 消息审核
	 */
	public function auditOp(){
		$model = Model();
		$message = $model->table('message')->where(array('message_id'=>intval($_GET['message_id'])))->find();
		
		if(empty($message)){
			showMessage(L('nc_weixin_wall_message_is_not_exists'),'','succ');
		}
		
		$res = $model->table('message')->where(array('message_id'=>intval($_GET['message_id'])))->update(array('state'=>intval($_GET['state'])));

		if($res){
			showMessage(L('nc_weixin_wall_message_audit_succ'),'','succ');
		}else{
			showMessage(L('nc_weixin_wall_message_audit_fail'),'','error');
		}
	}

	/*
	 * 删除消息
	 */
	public function delOp(){
		$model = Model();
		$res = $model->table('message')->where(array('message_id'=>intval($_GET['message_id'])))->delete();
		
		if($res){
			showMessage(L('nc_weixin_wall_message_delete_succ'),'','succ');
		}else{
			showMessage(L('nc_weixin_wall_message_delete_fail'),'','fail');
		}
	}

	/*
	 * 接受消息
	 */
	public function responseOp(){		
		$postStr = $GLOBALS["HTTP_RAW_POST_DATA"];
		
		if(!empty($postStr)){
			$postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
            $fromUsername = $postObj->FromUserName;
            $toUsername = $postObj->ToUserName;
            $keyword = trim($postObj->Content);
            $time = time();	
			
			$content_pattern = "/\#([^\#|.]+)\#/"; 
			preg_match_all($content_pattern, $keyword, $keywordarr);
			$activity_name = implode(',',$keywordarr[1]);
			
			$model	  = Model();
			$activity = $model->table('activity')->where(array('activity_name'=>$activity_name))->find();			
			if(empty($activity)){//活动不存在
				exit;
			}
			$admin = $model->table('wxaccount')->where(array('wx_account'=>$toUsername))->find();
			if(empty($admin)){//微信号不存在
				exit;
			}
			$fans  = $model->table('fans')->where(array('fans_openid'=>$fromUsername))->find();
			if(empty($fans)){//粉丝不存在
				exit;
			}
						
			$params					= array();
			$params['user_openid']	= $fans['fans_openid'];
			$params['user_name']	= $fans['fans_nickname'];
			$params['activity_id']	= $activity['activity_id'];
			$params['activity_name']= $activity['activity_name'];
			$params['message_time']	= $postObj->CreateTime;
			$params['message_content'] = $postObj->Content;
			
			$model->table('message')->insert($params);
			exit;
		}
	}


}