<?php
/**
 * 微调研wap端
 * */
defined('InByShopWWI') or exit('Access Invalid!');
define('MYSQL_RESULT_TYPE',1);
class wap_researchControl extends wapBaseControl{
	function __construct(){
	    parent::__construct();
	}
	/**
	 * 微调研
	 * */
    public function indexOp(){
    	$item_id = intval($_GET['item_id']);
    	$fromuser = trim($_GET['wxchat_id']);
    	$wx_id = intval($_REQUEST['wx_id']);
    	$model = Model();
    	$dyinfo = $model->table('research')->where(array('item_id'=>$item_id))->find();
    	$dyquestion = $model->table('research_question')->where(array('research_id'=>$item_id))->select();
    	$dyresult = $model->table('research_result')->where(array('member_wx'=>$fromuser))->select();
    	$tp = 'research_info';
    	print_r($dyresult);
    	if(!empty($dyresult)){
    		$url = '?act=wap_research&op=getprive&wx_id='.$wx_id.'&res_id='.$item_id.'&fromuser='.$fromuser;
    		redirect($url);
    	}
    	Tpl::output('dyinfo',$dyinfo);
    	Tpl::output('dycon',$dyquestion);
    	Tpl::output('fromuser',$fromuser);
        Tpl::showpage($tp);
    }

    public function saveOp(){
        $res_id = intval($_POST['res_id']);
    	$fromuser = trim($_POST['fromuser']);
    	$wx_id = intval($_REQUEST['wx_id']);
    	$model = Model();
    	$dyquestion = $model->table('research_question')->where(array('research_id'=>$res_id))->select();
    	$anwser = $_POST['ans'];
    	$insert = array();
    	foreach($dyquestion as $key => $val){
    	    $acount = unserialize($val['acount']);
    	    $id = $val['qid'];
    	    $in_tmp = array();
    	    $in_tmp['res_id'] = $res_id;
    	    $in_tmp['member_wx'] = $fromuser;
    	    $in_tmp['qid'] = $id;
    	    $in_tmp['aids'] = is_array($anwser[$id])?implode(',',$anwser[$id]):$anwser[$id];
    	    $insert[] = $in_tmp;
			if(is_array($anwser[$id])){
			    foreach($anwser[$id] as $k => $v){
			    	$acount[$v] += 1;
			    }
			}else{
			    $acount[$anwser[$id]] += 1;
			}
			$model->table('research_question')->where(array('qid'=>$id))->update(array('acount'=>serialize($acount)));
    	}
    	$model->table('research_result')->insertAll($insert);
    	$model->table('research')->where(array('item_id'=>$res_id))->setInc('item_num');
    	showMessage('提交成功','?act=wap_research&op=getprive&wx_id='.$wx_id.'&res_id='.$res_id.'&fromuser='.$fromuser,'succ');
    }

    /**
     * 抽奖展示页
     * */
    public function getpriveOp(){
        $res_id = intval($_REQUEST['res_id']);
        $fromuser = $_REQUEST['fromuser'];
        $model = Model();
        $lottery = $model->table('research_lottery')->where(array('res_id'=>$res_id))->find();
        if(chksubmit()){
			$prize_info = $this->_prize($lottery);
			$prize = $this->_handle($prize_info,$res_id);
			$prize['lot_id'] = $lottery['lot_id'];
			$prize['res_id'] = $res_id;
			$prize['wxchat_id'] = $fromuser;
			$prize['wx_id'] = $_REQUEST['wx_id'];
			$prize['insert_time'] = time();
			$model->table('research_lottery_record')->insert($prize);
			$url = '?act=wap_research&op=getprive&wx_id='.$wx_id.'&res_id='.$res_id.'&fromuser='.$fromuser;
			redirect($url);
        }
        $lot_result = $model->table('research_lottery_record')->where(array('res_id'=>$res_id,'wxchat_id'=>$fromuser))->find();
        Tpl::output('fromuser',$fromuser);
        $tp = 'show_lot';
        if(empty($lottery)){
            $tp = 'show_end';
            $res = $model->table('research')->where(array('item_id'=>$res_id))->find();
            Tpl::output('res',$res);
        }
        Tpl::output('lot',$lottery);
        if(!empty($lot_result)){
        	Tpl::output('result',$lot_result);
        	$tp = 'lot_info';
        }
        Tpl::showpage($tp);
    }

    /**
     * 抽奖处理
     * */
    private function _handle($lot,$res_id){
    	if(empty($lot)){
    		showMessage('参数错误','','error');
    	}
    	$lot_id = 0;
    	for($k=0;$k<10;$k++){
			$lot_id += mt_rand(0,360);
    	}
    	$lot_id = ceil($lot_id*1.0/10);
		$default = array();
		$row = 360;
		$count = count($lot['lot']);
		$i = 0;
		foreach($lot['lot'] as $key => $val){
			$num = ceil(($val['num']*1.0)/$lot['set_count']*360);
			$default[$key] = range($i,$i+$num);
		}
		$return = array();
		$model = Model();
		foreach($default as $l => $v){
		    if(in_array($lot_id,$v)){
		    	$now_lot = $model->table('research_lottery_record')->where(array('res_id'=>$res_id,'prize'=>$l+1))->select();
				if(count($now_lot) < $lot['lot'][$l]['num']){
			    	$return['prize'] = $l+1;
			    	$return['lot_sn'] = uniqid($res_id.'_');
			    	$return['is_lot'] = 1;
			        break;
				}else{
				    $this->_handle($lot,$res_id);
				}
		    }
		}
		return $return;
    }

    /**
     * 奖项数据整理
     * */
    private function _prize($lottery){
        $lot_count = 0;
        $set_count = $lottery['join_count'];
        $join = $lottery['joinnum'];
        $lot = unserialize($lottery['lot_info']);
        foreach((array)$lot as $k => $v){
            $lot_count += $v['num'];
        }
        if($lot_count <= 0){
        	showMessage('参数错误','','error');
        }
        $return  = array();
        $return['lot_count'] = $lot_count;
        $return['set_count'] = $set_count;
        $return['lot'] = $lot;
        $return['lot_persent'] = ($lot_count*1.0)/$set_count;
        $return['join_count'] = $join;
        return $return;
    }
}