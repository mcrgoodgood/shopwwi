<?php
/**
 * 商城公共方法
 *
 * @package    function
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com/
 * @link       http://www.shopwwi3.com/
 * @author	   ShopWWI Team
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');

/**
 * 验证当前管理员权限是否可以进行操作
 *
 * @param string $link_nav
 * @return
 */
function checkPermission($act='',$op=''){
	$admin_info = unserialize(decrypt(cookie('sys_key'),MD5_KEY));
	if ($admin_info['sp'] == 1 || $admin_info['glimits'] == 'all') return true;
	if ($act == '' || $op == ''){
		$act = $_GET['act']?$_GET['act']:$_POST['act'];
		$op = $_GET['op']?$_GET['op']:$_POST['op'];
	}
	$permission = decrypt($admin_info['glimits'],MD5_KEY.md5($admin_info['gname']));
	$permission = explode('|',$permission);
	//以下几项不需要验证
	$tmp = array('index','dashboard','login','common');
	if (in_array($act,$tmp)) return true;
	if (in_array($act.'.all',$permission) || in_array("$act.$op",$permission)){
		return true;
	}
	return false;
}

//curl post提交数据
function curl_post($url, $data){
	$ch = curl_init();
	$header = array("Accept-Charset:utf-8");
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$tmpInfo = curl_exec($ch);
	$result = array();
	if (curl_errno($ch)) {
		$result = array('nc_status'=>false);
	}else{
		$tmpInfo = json_decode($tmpInfo,true);
		$result['nc_status'] = true;
		$result['data'] = $tmpInfo;
	}
	curl_close($ch);
	return $result;
}

//curl get提交数据
function curl_get($url, $data = array()){
	if(!empty($data)){
		$url .='?'.http_build_query ($data);
	}
	$ch = curl_init();
/* 	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_HEADER, FALSE); */
	$header = array("Accept-Charset:utf-8");
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$result = curl_exec ($ch);
	curl_close($ch);
	return $result;
}

?>
