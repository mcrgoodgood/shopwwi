<?php
defined('InByShopWWI') or exit('Access Invalid!');
$config = array();
$config['shop_site_url'] 	 = '===url===';
$config['version'] 		= '2013720';
$config['setup_date'] 	= '===setup_date===';
$config['gip'] 			= 0;
$config['dbdriver'] 	= '===db_driver===';
$config['tablepre']		= '===db_prefix===';
$config['db'][1]['dbhost']  	= '===db_host===';
$config['db'][1]['dbport']		= '===db_port===';
$config['db'][1]['dbuser']  	= '===db_user===';
$config['db'][1]['dbpwd'] 	 	= '===db_pwd===';
$config['db'][1]['dbname']  	= '===db_name===';
$config['db'][1]['dbcharset']   = '===db_charset===';
$config['db']['slave'] 		= array();
$config['session_expire'] 	= 3600;
$config['lang_type'] 		= 'zh_cn';
$config['cookie_pre'] 		= '===cookie_pre===';
$config['tpl_name'] 		= 'default';
$config['thumb']['save_type'] 		= 1;
$config['thumb']['cut_type'] = 'gd';
$config['thumb']['impath'] = '';
$config['cache']['type'] 			= 'file';
$config['memcache']['prefix']      	= '';
$config['memcache'][1]['port']     	= 11211;
$config['memcache'][1]['host']     	= '127.0.0.1';
$config['memcache'][1]['pconnect'] 	= 0;
$config['debug'] 			        = false;
$config['syn_site'][] = 'shop';
$config['app_key']['shop'] = '===app_key1===';
$config['wap_site_url'] = '===wap_url===';
$config['mall_api_url'] = '===api_url===';