<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $lang['nc_weixin_wall_show_title'];?></title>
<link href="<?php echo TEMPLATES_URL; ?>/css/wall_<?php if(isset($_GET['style'])){ echo trim($_GET['style']);}else{ echo 'brown';}?>.css" rel="stylesheet" class="theme">
<link href="<?php echo TEMPLATES_URL; ?>/css/default_wall.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.js"></script>
</head>
<body>
<div class="nc_main">
  <div class="nc_header">
    <div class="logo"><img src="<?php echo TEMPLATES_URL; ?>/images/logo_wall.png"></div>
    <div class="notice"> <i></i><?php echo $lang['nc_weixin_wall_show_account'];?>：<strong><?php echo $output['account']['wx_name'];?></strong> <br>
      <?php echo $lang['nc_weixin_wall_show_send'];?><span>#<?php echo $output['activity']['activity_name'];?>#+</span><?php echo $lang['nc_weixin_wall_your_talk'];?></div>
  </div>
  <div class="nc_wall">
    <div class="lottery_wall">
      <div class="lottery_prize"> <strong><?php echo $lang['nc_weixin_wall_show_prize'];?>：<?php echo $output['prize']['user_name'];?></strong></div>
    </div>
  </div>
  <div class="footer">
    <ul>
      <li><a href="javascript:;" onclick='javascript:wall_href("index.php?act=wall&op=message&activity_id=<?php echo intval($output['activity']['activity_id']);?>&style=");'><?php echo $lang['nc_weixin_wall_show_home'];?></a></li>
      <li><a href="javascript:;" onclick='javascript:wall_href("index.php?act=wall&op=rank&activity_id=<?php echo intval($output['activity']['activity_id']);?>&style=");'><?php echo $lang['nc_weixin_wall_show_rank'];?></a></li>
      <li><a href="javascript:;" onclick='javascript:wall_href("index.php?act=wall&op=lottery&activity_id=<?php echo intval($output['activity']['activity_id']);?>&style=");'><?php echo $lang['nc_weixin_wall_show_lottery'];?></a></li>
      <li style="position:relative;"><a id="theme" href="javascript:;"><?php echo $lang['nc_weixin_wall_show_style'];?></a>
        <div id="theme_menu" class="color" style="display:none;"> 
        	<em onclick="wall_theme('orange')" class="c1"></em> 
        	<em onclick="wall_theme('black')" class="c2"></em>
        	<em onclick="wall_theme('red')" class="c3"></em> 
        	<em onclick="wall_theme('yellow')" class="c4"></em> 
        	<em onclick="wall_theme('blue')" class="c5"></em>
        	<em onclick="wall_theme('green')" class="c6"></em>
        	<em onclick="wall_theme('cyan')" class="c7"></em>
        	<em onclick="wall_theme('pink')" class="c8"></em> 
        	<em onclick="wall_theme('purple')" class="c9"></em>
        	<em onclick="wall_theme('brown')" class="c10"></em> 
        </div>
      </li>
    </ul>
    <input type="hidden" name="style" value="<?php if(isset($_GET['style'])){ echo trim($_GET['style']);}else{ echo 'brown';}?>">
  </div>
</div>
</body>
<script type="text/javascript">
	function wall_theme(style){
		$("input[name=style]").val(style);
		$(".theme").attr('href','<?php echo TEMPLATES_URL;?>'+'/css/wall_'+style+'.css');
	}
	
	function wall_href(href){
		var style = $('input[name=style]').val();
		location.href = href + style;
	}
	
	$('#theme').toggle(function(){
		$("#theme_menu").show();
	},function(){
		$("#theme_menu").hide();
	});

</script>
</html>
