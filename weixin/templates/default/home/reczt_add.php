<?php defined('InByShopWWI') or exit('Access Invalid!');?>

<div class="main_hd">
  <h2>添加文本推荐分类</h2>
  <p class="extra_info"><a href="index.php?act=store&op=gc_manage&wx_id=<?php echo intval($_GET['wx_id']); ?>&reply_type=click_reply">文本推荐分类管理</a></p>
</div>
<form id="add_form" method="post" enctype="multipart/form-data">
<input type="hidden" name="form_submit" value="ok" />
<input type="hidden" name="store_id" value="" />
<div class="main_bd">
<label class="label_box"><font style="color:red"></font>类型：</label>
  <div class="label_form"> 
	<span>
	<input type="radio" name="reply_type" id="reply_type_click" value="click_reply" checked ><label for="reply_type_click" style="cursor:pointer"> 点击回复素材</label>
	<input type="radio" name="reply_type" id="reply_type_auto" value="auto_reply" ><label for="reply_type_auto" style="cursor:pointer"> 自动回复素材</label>
	</span>     
  </div><br>
  <label class="label_box"><font style="color:red">*</font>主题：</label>
  <div class="label_form"> 
	<span>
	<input type="text" name="reply_title" value="" class="label_input">
	<label for='reply_title' class='error msg_invalid' style='display:none;'></label>
	</span>     
  </div><br>
  <div id="code">
  <label class="label_box"><font style="color:red">*</font>标识码：</label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="cr_code" value="" class="label_input">
	          	<label for='cr_code' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div><br></div>
  <label class="label_box">说明：</label>
  <div class="label_form"> 
	<textarea name="reply_note" style="width:350px;height:100px"></textarea>     
  </div><br>
  <div class="tip">注：请填写专题ID，点击“保存”按钮后将生成一个专题推荐的图文回复素材。</div>
        <div class="label_form"> 
          专题名称：<input type="text" name="zt_title" class="label_input"/><br><br>
		  专题描述：<textarea name="zt_desc" style="width:322px;height:100px"></textarea><br><br>
		  专题图片：<input type="file" name="zt_image"/><br><br>
		  专题链接：<?php echo WAP_SITE_URL."special.html?special_id="; ?><input type="text" name="zt_id" class="label_input" style="width: 80px" /><label for='zt_id' class='error msg_invalid' style='display:none;'></label>
        </div>
  <div class="btn_bar">
	<input class="btn_input" type="submit" value="保存">
  </div>
</div>
</form>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script>
<script type="text/javascript">
$(function(){
	$("#add_form").validate({
        rules: {
        	reply_title: {
				required:true
            },
            zt_id: {
				required:true
            }
        },
        messages:{
        	reply_title:{
        		required:'请填写标题'
            },
            zt_id:{
        		required:'请填写专题ID'
            }
        }
	});
	$('input[name="reply_type"]').change(function(){
		if ($(this).val() == "click_reply"){
			$('#code').show();
		} else {
			$('#code').hide();
		}
	});
})
</script>