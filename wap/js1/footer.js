﻿$(function (){
    var package = window.package || 'index';
    if (getQueryString('key') != '') {
        var key = getQueryString('key');
        var username = getQueryString('username');
        addCookie('key', key);
        addCookie('username', username);
    } else {
        var key = getCookie('key');
    }
    var html = '<div class="nctouch-footer-wrap posr">'
        +'<div class="nav-text">';
    if(key){
        html += '<a href="'+WapSiteUrl+'/tmpl/member/member.html">我的商城</a>'
            + '<a id="logoutbtn" href="javascript:void(0);">注销</a>'
            + '<a href="'+WapSiteUrl+'/tmpl/member/member_feedback.html">反馈</a>';
            
    } else {
        html += '<a href="'+WapSiteUrl+'/tmpl/member/login.html">登录</a>'
            + '<a href="'+WapSiteUrl+'/tmpl/member/register.html">注册</a>'
            + '<a href="'+WapSiteUrl+'/tmpl/member/login.html">反馈</a>';
    }
    html += '<a href="javascript:void(0);" class="gotop">返回顶部</a>'
        +'</div>'
        +'<div class="nav-pic">'
			+'<a href='+SiteUrl+'"/mall/index.php?app=mb_app" class="app"><span><i></i></span><p>客户端</p></a>'
			+'<a href="javascript:void(0);" class="touch"><span><i></i></span><p>触屏版</p></a>'
            +'<a href="'+SiteUrl+'" class="pc"><span><i></i></span><p>电脑版</p></a>'
         +'</div>'
		 +'<div class="copyright">'
    	 +'</div>';

    var tmp = '';var tmp1='';var tmp2='';var tmp3='';var tmp4='';
    switch(package){
        case 'index':
            tmp="_light";
            break;
        case '399package':
            tmp1="_light";
            break;
        case 'product':
            tmp2='_light';
            break;
        case 'designer':
            tmp3='_light';
            break;
        case 'memeber':
            tmp4='_light';
            break;
        // case 'packagestore':
        //     tmp3='_light';
        //     break;
        case'packagestore':
    }

    var html1 = '<a href="/wap/index.html"><img src="'+WapSiteUrl+'/images/footer_03'+ tmp + '.png"></a>'
            +'<a href="/wap/tmpl/399package.html"><img src="'+WapSiteUrl+'/images/footer_19'+ tmp1 +'.png"></a>'
            +'<a href="/wap/tmpl/product_first_categroy.html"><img src="'+WapSiteUrl+'/images/footer_21'+ tmp2 +'.png"></a>'
            +'<a href="/wap/tmpl/designer.html"><img src="'+WapSiteUrl+'/images/footer_23'+ tmp3 +'.png"></a>'
            +'<a href="/wap/tmpl/member/member.html"><img src="'+WapSiteUrl+'/images/footer_25'+ tmp4 +'.png"></a>';

    // 弹框的引入样式
    var html2 = '<form method="post" action="/mall/index.php?app=index&amp;wwi=shenqing" class="nc-login-form" id="shenqing2" name="form"><div class="word">请输入您的联系方式，我们会与你联系!</div>'
        +'<input type="text" placeholder="请输入您的称呼" name="username" maxlength="30"><br>'
        +'<input type="text" placeholder="请输入您的手机号码" name="mobile" id="mobile2" maxlength="11" >'
        +'<div class="layer_btn submit_yuyue">提交</div></form>';

    $('#layer').append(html2);
    $("#page").append(html1);
	$("#footer").append(html);
    var key = getCookie('key');
    $(".submit_yuyue").click(function(e){
        e.preventDefault();
        var form  = $(this).closest('form');
        type = '手机预约';
        username = form[0].username.value;
        mobile = form[0].mobile.value;
        if (username == "") {
            alert("请填写您的称呼！");
            return false;
        }
        if (mobile == "") {
            alert("手机号码不能为空！");
            return false;
        } else if (!mobile.match(/^1[3|4|5|7|8][0-9]\d{4,8}$/)){
            alert("手机号码格式有误！");
            return false;
        }

        param = "username="+username+"&mobile="+mobile;
        $.ajax({
            type: "POST",
            url: "/mall/index.php?app=index&wwi=shenqing",
            data: "form_submit=ok&" + param + "&type=" + type,
            cache: false,
            async: true,
            dataType: "json",
            success: function(ret){
                if (ret.state == "success") {
                    form[0].username.value = "";
                    form[0].mobile.value = "";
                    second = 0;
                    alert("申请成功，工作人员将会尽快与您联系！");
                    // $("#bm_pop_box .close").click();
                } else {
                    alert("申请失败：【" + ret.desc + "】");
                }
            },
            error: function(ret) {
                alert("发生了错误：【" + ret.desc + "】");
            },
            complete: function(ret) {

            }
        });
    });

	$('#logoutbtn').click(function(){
		var username = getCookie('username');
		var key = getCookie('key');
		var client = 'wap';
		$.ajax({
			type:'get',
			url:ApiUrl+'/index.php?app=logout',
			data:{username:username,key:key,client:client},
			success:function(result){
				if(result){
					delCookie('username');
					delCookie('key');
					location.href = WapSiteUrl;
				}
			}
		});
	});
    // 统计
    var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
    $('body').append(unescape("%3Cdiv class='none' %3E%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F2abfc0d2c6f703a09ffb16d42a0073fe' type='text/javascript'%3E%3C/script%3E%3C/div%3E"));
});
function wapShare(data){
    var sharedata;
    var ua = navigator.userAgent.toLowerCase();
    if ((ua.indexOf('micromessenger') > -1)) {
        require(['weixin_js'], function(wx) {
            sharedata = {
                title: data.title,
                desc: data.desc,
                link: data.link,
                imgUrl: data.imgUrl
            };
            $.getJSON(ApiUrl + '/index.php?app=wx&wwi=get_js_config', function(wxdata) {
                wx.config(JSON.parse(wxdata.datas));
                wx.ready(function() {
                    wx.onMenuShareAppMessage(sharedata);
                    wx.onMenuShareTimeline(sharedata);
                    wx.onMenuShareQQ(sharedata);
                    wx.onMenuShareWeibo(sharedata);
                });
            });
            wx.error(function(res){
                alert('微信操作失败: ' + JSON.stringify(res));
            });
        });
    }
}