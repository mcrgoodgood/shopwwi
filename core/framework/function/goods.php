<?php
/**
 * 商品图片统一调用函数
 *
 *
 *
 * @package    function
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @author	   ShopWWI Team
 * @since      File available since Release v1.1
 */

defined('InByShopWWI') or exit('Access Invalid!');

/**
 * 取得商品缩略图的完整URL路径，接收商品信息数组，返回所需的商品缩略图的完整URL
 *
 * @param array $goods 商品信息数组
 * @param string $type 缩略图类型  值为tiny,small,mid,max
 * @return string
 */
function thumb($goods, $type = ''){
	if (!in_array($type,array('tiny','small','mid','max','240x240'))) $type = 'small';
	if (is_array($goods)){
		if (array_key_exists('goods_images',$goods)){
			$goods['goods_image'] = $goods['goods_images'];
		}elseif (array_key_exists('apic_cover',$goods)){
			$goods['goods_image'] = $goods['apic_cover'];
		}
		if (empty($goods['goods_image'])){
			return UPLOAD_SITE_URL.'/'.defaultGoodsImage($type);
		}
		$a = explode('.',$goods['goods_image']);
		$ext = end($a);
		$file = str_ireplace(array('_tiny.'.$ext,'_small.'.$ext,'_mid.'.$ext,'_max.'.$ext,'_240x240.'.$ext),'',$goods['goods_image']);

		if (!file_exists(BASE_UPLOAD_PATH.'/'.ATTACH_GOODS.'/'.$file.($type==''?'':'_'.$type.'.'.$ext))){
            if (file_exists(BASE_UPLOAD_PATH.'/'.ATTACH_GOODS.'/'.$file.'_mid.'.$ext)){
                $type = 'mid';
            } else {
                return UPLOAD_SITE_URL.'/'.defaultGoodsImage($type);
            }
        }
		$thumb_host = UPLOAD_SITE_URL.'/'.ATTACH_GOODS;
		return $thumb_host.'/'.$file.($type==''?'':'_'.$type.'.'.$ext);
	}
}

/**
 * 取得商品缩略图的完整URL路径，接收商品名称
 *
 * @param string $file 商品名称
 * @param string $type 缩略图尺寸类型，值为tiny,small,mid,max
 * @return string
 */
function cthumb($file, $type = ''){
	if (!in_array($type,array('tiny','small','mid','max','240x240'))) $type = 'small';
	if (empty($file)){
		return UPLOAD_SITE_URL.'/'.defaultGoodsImage($type);
	}
	$a = explode('.',$file);
	$ext = end($a);
	$file = str_ireplace(array('_tiny.'.$ext,'_small.'.$ext,'_mid.'.$ext,'_max.'.$ext,'_240x240.'.$ext),'',$file);

	//本地存储时，增加判断文件是否存在，用默认图代替
    if (!file_exists(BASE_UPLOAD_PATH.'/'.ATTACH_GOODS.'/'.$file.($type==''?'':'_'.$type.'.'.$ext))){
        if (file_exists(BASE_UPLOAD_PATH.'/'.ATTACH_GOODS.'/'.$file.'_mid.'.$ext)){
            $type = 'mid';
        } else {
            return UPLOAD_SITE_URL.'/'.defaultGoodsImage($type);
        }
    }
	$thumb_host = UPLOAD_SITE_URL.'/'.ATTACH_GOODS;
	return $thumb_host.'/'.$file.($type==''?'':'_'.$type.'.'.$ext);
}
