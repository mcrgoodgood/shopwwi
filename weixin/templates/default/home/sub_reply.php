<?php defined('InByShopWWI') or exit('Access Invalid!');?>
<div class="main_hd">
  <h2><?php echo L('ar_sub_autoreply'); ?></h2>
</div>
<form id="add_form" method="post">
<input type="hidden" name="form_submit" value="ok" />
<input type="hidden" name="exc_type" value="<?php echo empty($output['rs_info'])?'add':'edit'; ?>" />
<input type="hidden" name="rs_id" value="<?php echo $output['rs_info']['rs_id']; ?>" />
<div class="main_bd">
	<ul>
		<li class="list_item">
	        <label class="label_box"><?php echo L('ar_reply').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<select name="reply_id">
	          	<option value="0"><?php echo L('ar_pls_choose_reply_-'); ?></option>
	          	<?php if(!empty($output['reply_list'])){ ?>
	          	<?php foreach ($output['reply_list'] as $k=>$v){ ?>
	          	<option value="<?php echo $v['reply_id']; ?>" <?php if($output['rs_info']['reply_id'] == $v['reply_id']){ ?>selected<?php } ?>><?php echo $v['reply_title']; ?></option>
	          	<?php }} ?>
	          	</select>
	          	<span style="margin-left:10px">
	          	<input name="reply_type" value="0" type="radio" id="reply_type_0" checked /><label for="reply_type_0" style="cursor:pointer;margin-left:3px" >全部</label>
	          	<input name="reply_type" value="1" type="radio" id="reply_type_1" /><label for="reply_type_1" style="cursor:pointer;margin-left:3px" >纯文字</label>
	          	<input name="reply_type" value="2" type="radio" id="reply_type_2" /><label for="reply_type_2" style="cursor:pointer;margin-left:3px" >图文</label>
	          	<input name="reply_type" value="3" type="radio" id="reply_type_3" /><label for="reply_type_3" style="cursor:pointer;margin-left:3px" >语音</label>
	          	</span>
	          	<label for='reply_id' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><?php echo L('nc_status').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<input type="radio" name="rs_status" value="1" id="rs_status_1" <?php if($output['rs_info']['rs_use'] == 1){ ?>checked<?php } ?>>&nbsp;<label for="rs_status_1" style="cursor:pointer"><?php echo L('nc_use'); ?></label>&nbsp;&nbsp;
	          	<input type="radio" name="rs_status" value="0" id="rs_status_0" <?php if($output['rs_info']['rs_use'] == 0){ ?>checked<?php } ?>>&nbsp;<label for="rs_status_0" style="cursor:pointer"><?php echo L('nc_not_use'); ?></label>
	          </span>     
	        </div>
         </li>
     
    </ul><div class="btn_bar">
	        <input type="submit" class="btn_input" value="<?php echo $lang['nc_save'];?>"></div>
</div>
</form>
<script type="text/javascript">
$(function(){
	$('input[name="reply_type"]').change(function(){
		var type = $(this).val();
		$.getJSON('index.php?act=autoreply&op=ajax_get_reply&wx_id=<?php echo intval($_GET['wx_id']); ?>&reply_type='+type, function(result){
			if(result.done){
				$('select[name="reply_id"]').empty();
				$('select[name="reply_id"]').append('<option value="0"><?php echo L('ar_pls_choose_reply_-'); ?></option>');
				$.each(result.reply_list,function(key,val){
					$('select[name="reply_id"]').append('<option value="'+val.reply_id+'">'+val.reply_title+'</option>');
				});
			}else{
				alert(result.msg);
			}
		});
	});
})
</script>