<?php
/**
 * 商城管理
 * @copyright  Copyright (c) 2007-2015 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
class storeControl extends BaseHomeControl{
	/**
	 * 文本推荐分类管理
	 */
	public function gc_manageOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		if (trim($_GET['reply_type']) == "click_reply") {
			$where_condition = array('wx_id'=>$wx_id,'cr_ext'=>1);
			if(trim($_GET['s_title']) != ''){
				$where_condition['cr_title'] = array('like','%'.trim($_GET['s_title']).'%');
			}
			$table = "click_reply";
			$order = "cr_addtime desc";
		} else {
			$where_condition = array('wx_id'=>$wx_id,'reply_ext'=>1);
			if(trim($_GET['s_title']) != ''){
				$where_condition['reply_title'] = array('like','%'.trim($_GET['s_title']).'%');
			}
			$table = "reply";
			$order = "reply_addtime desc";
		}
		$cr_list = $model->table($table)->where($where_condition)->page(10)->order($order)->select();
		Tpl::output('reply_list',$cr_list);
		Tpl::output('show_page',$model->showpage());
		Tpl::showpage('gc_manage_text');
	}
	
	/**
	 * 添加文本推荐分类
	 */
	public function recgc_text_addOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		$wx_info = $model->field('shop_data_type,store_id')->table('wxaccount')->where(array('wx_id'=>$wx_id))->find();
		if(chksubmit()){
			//表单验证
			if (trim($_POST['reply_title']) == "") {
				showMessage("请填写标题",'','error');
			}
			if (empty($_POST['recgc'])) {
				showMessage("请选择商品分类",'','error');
			}
			if ($_POST['reply_type'] == "reply_type_click" && trim($_POST['cr_code']) == "") {
				showMessage("请填写标识码",'','error');
			}
			//处理分类信息
			$gc_content = "";
			foreach ($_POST['recgc'] as $val) {
				if ($wx_info['shop_data_type'] == 2 ) {
					$gc_content .= "<a href='".WAP_SITE_URL."tmpl/store_goods.html?store_id=".$_POST['store_id']."&stc_id=".$val."' >".$_POST['recgn_'.$val]."</a><br>";
				} else {
					$gc_content .= "<a href='".WAP_SITE_URL."tmpl/product_list.html?gc_id=".$val."' >".$_POST['recgn_'.$val]."</a><br>";
				}
			}
			$insert_array = array();
			if ($_POST['reply_type'] == "click_reply") {
				$insert_array['cr_title'] = trim($_POST['reply_title']);
				$insert_array['cr_note'] = trim($_POST['reply_note']);
				$insert_array['cr_type'] = 1;
				$insert_array['cr_addtime'] = time();
				$insert_array['cr_addstaff'] = $_SESSION['admin_id'];
				$insert_array['wx_id'] = $wx_id;
				$insert_array['cr_code'] = trim($_POST['cr_code']);
				$insert_array['cr_ext'] = 1;
				$insert_array['cr_content'] = $gc_content;
				$rs = $model->table('click_reply')->insert($insert_array);
			} else {
				$insert_array['reply_title'] = trim($_POST['reply_title']);
				$insert_array['reply_note'] = trim($_POST['reply_note']);
				$insert_array['reply_type'] = 1;
				$insert_array['reply_addtime'] = time();
				$insert_array['reply_addstaff'] = $_SESSION['admin_id'];
				$insert_array['wx_id'] = $wx_id;
				$insert_array['reply_ext'] = 1;
				$insert_array['reply_content'] = $gc_content;
				$rs = $model->table('reply')->insert($insert_array);
			}
			if($rs){
				showMessage("文本商品分类推荐创建成功！",'index.php?act=store&op=gc_manage&wx_id='.$wx_id.'&reply_type='.$_POST['reply_type'],'succ');
			}else{
				showMessage("文本商品分类推荐创建失败",'','error');
			}
		}
		Tpl::output('type', $wx_info['shop_data_type']);
		Tpl::showpage('recgc_text_add');
	}
	
	/**
	 * ajax获取店铺分类
	 */
	public function ajax_getgcOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		//调取公众账号的接口信息
		$wx_info = $model->field('shop_data_type,store_id')->table('wxaccount')->where(array('wx_id'=>$wx_id))->find();
		if (empty($wx_info)) {
			echo json_encode(array('done'=>false,'msg'=>'微信公众号信息异常'));die;
		}
		if ($wx_info['shop_data_type'] == 2) {//平台商家
			$url = MALL_API_URL."index.php?act=store&op=store_goods_class&store_id=".$wx_info['store_id'];
		} else {//平台自营
			$url = MALL_API_URL."index.php?act=goods_class";
		}
		$res = @file_get_contents($url);
		$res = json_decode($res,true);
		if ($res['datas']['error'] != "") {
			echo json_encode(array('done'=>false,'msg'=>$res['datas']['error']));die;
		} else {
			if ($wx_info['shop_data_type'] == 2) {//平台商家
				$gc_list = array();
				if (!empty($res['datas']['store_goods_class'])) {
					foreach ($res['datas']['store_goods_class'] as $val) {
						if ($val['pid'] == 0) {
							 $gc_list[] = array(
							 		'gc_id'=>$val['id'],
							 		'gc_name'=>$val['name'],
							 		'gc_oriname'=>$val['name']
							 );
							 //添加子分类
							 foreach ($res['datas']['store_goods_class'] as $cv) {
							 	if ($cv['pid'] == $val['id']) {
							 		$gc_list[] = array(
							 				'gc_id'=>$cv['id'],
							 				'gc_name'=>"&nbsp;&nbsp;&nbsp;&nbsp;|- ".$cv['name'],
							 				'gc_oriname'=>$cv['name']
							 		); 
							 	}
							 }
						}
					}
				}
				echo json_encode(array('done'=>true,'gc_list'=>$gc_list,'store_id'=>$wx_info['store_id']));die;
			} else {//平台自营
				$gc_list = array();
				if (!empty($res['datas']['class_list'])) {
					foreach ($res['datas']['class_list'] as $val) {
						$gc_list[] = array(
								'gc_id'=>$val['gc_id'],
								'gc_name'=>$val['gc_name'],
								'gc_oriname'=>$val['gc_name'],
								'gc_parent_id'=>$val['gc_parent_id']
						);
					}
				}
				echo json_encode(array('done'=>true,'gc_list'=>$gc_list));die;
			}
		}
	}
	
	/**
	 * ajax获取平台商品子分类
	 */
	public function ajax_getmallgcOp(){
		$gc_id = intval($_GET['gc_id']);
		$degree = intval($_GET['dg']);
		$url = MALL_API_URL."index.php?act=goods_class&gc_id=".$gc_id;
		$res = @file_get_contents($url);
		$res = json_decode($res,true);
		if ($res['datas']['error'] != "") {
			echo json_encode(array('done'=>false,'msg'=>$res['datas']['error']));die;
		} else {
			if ($res['datas']['class_list'] == 0) {
				echo json_encode(array('done'=>false,'msg'=>'已无子分类'));die;
			} else {
				$gc_list = array();
				if (!empty($res['datas']['class_list'])) {
					foreach ($res['datas']['class_list'] as $val) {
						$gc_list[] = array(
								'gc_id'=>$val['gc_id'],
								'gc_name'=>(intval($_GET['goods'])!=1?($degree==1?"&nbsp;&nbsp;&nbsp;&nbsp;|- ":"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|- "):"").$val['gc_name'],
								'gc_oriname'=>$val['gc_name'],
								'degree'=>$degree+1
						);
					}
				}
				if (intval($_GET['goods']) == 1) {
					echo json_encode(array('done'=>true,'gc_list'=>$gc_list,'nowid'=>trim($_GET['nowid'])));die;
				} else {
					echo json_encode(array('done'=>true,'gc_list'=>$gc_list));die;
				}
			}
		}
	}

	/**
	 * 自定义菜单按钮推荐分类管理
	 */
	public function gc_manage_menuOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		if(chksubmit()){
			//表单验证
			if (empty($_POST['recgc'])) {
				showMessage("请选择商品分类",'','error');
			}
			if (count($_POST['recgc']) > 5) {
				showMessage("请不要选择超过5个商品分类",'','error');
			}
			//处理分类信息
			$gc_content = array();
			foreach ($_POST['recgc'] as $val) {
				$gc_content[] = array(
					'gc_id'=>$val,
					'gc_name'=>$_POST['recgn_'.$val],
					'url'=>$_POST['type'] == 2?WAP_SITE_URL."tmpl/store_goods.html?store_id=".$_POST['store_id']."&stc_id=".$val:WAP_SITE_URL."tmpl/product_list.html?gc_id=".$val
				);
			}
			if ($_POST['exctype'] == "insert") {
				$insert_array = array();
				$insert_array['wx_id'] = $wx_id;
				$insert_array['mr_addtime'] = time();
				$insert_array['mr_addstaff'] = $_SESSION['admin_id'];
				$insert_array['mr_content'] = serialize($gc_content);
				$rs = $model->table('menu_recgc')->insert($insert_array);
			} else {
				$rs = $model->table('menu_recgc')->where(array('wx_id'=>$wx_id))->update(array('mr_content'=>serialize($gc_content)));
			}
			if($rs){
				showMessage("自定义菜单商品分类推荐保存成功！",'index.php?act=store&op=gc_manage_menu&wx_id='.$wx_id,'succ');
			}else{
				showMessage("自定义菜单商品分类推荐保存失败",'','error');
			}
		}
		$mr_info = $model->table('menu_recgc')->where(array('wx_id'=>$wx_id))->find();
		Tpl::output('choose_gc', unserialize($mr_info['mr_content']));
		$wx_info = $model->field('shop_data_type,store_id')->table('wxaccount')->where(array('wx_id'=>$wx_id))->find();
		Tpl::output('type', $wx_info['shop_data_type']);
		Tpl::output('store_id', $wx_info['store_id']);
		Tpl::showpage('gc_manage_menu');
	}
	
	/**
	 * 商品管理
	 */
	public function goods_manageOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		if (trim($_GET['reply_type']) == "click_reply") {
			$where_condition = array('wx_id'=>$wx_id,'cr_ext'=>2);
			if(trim($_GET['s_title']) != ''){
				$where_condition['cr_title'] = array('like','%'.trim($_GET['s_title']).'%');
			}
			$table = "click_reply";
			$order = "cr_addtime desc";
		} else {
			$where_condition = array('wx_id'=>$wx_id,'reply_ext'=>2);
			if(trim($_GET['s_title']) != ''){
				$where_condition['reply_title'] = array('like','%'.trim($_GET['s_title']).'%');
			}
			$table = "reply";
			$order = "reply_addtime desc";
		}
		$cr_list = $model->table($table)->where($where_condition)->page(10)->order($order)->select();
		Tpl::output('reply_list',$cr_list);
		Tpl::output('show_page',$model->showpage());
		Tpl::showpage('goods_manage');
	}
	
	/**
	 * 添加图文推荐商品
	 */
	public function recgoods_textpic_addOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		if(chksubmit()){
			//表单验证
			if (trim($_POST['reply_title']) == "") {
				showMessage("请填写标题",'','error');
			}
			if ($_POST['choose_goods'] == "") {
				showMessage("请选择商品",'','error');
			}
			if ($_POST['reply_type'] == "reply_type_click" && trim($_POST['cr_code']) == "") {
				showMessage("请填写标识码",'','error');
			}
			//处理商品信息
			$goods_content = "";
			$choose_goods = explode("|||||", $_POST['choose_goods']);
			foreach ($choose_goods as $val) {
				$goods_info = explode(",", $val);
				$goods_content[] = array(
						'title'=>$goods_info[1],
						'desc'=>"",
						'picurl'=>$goods_info[2],
						'url'=>WAP_SITE_URL."tmpl/product_detail.html?goods_id=".$goods_info[0]	
				);
				unset($goods_info);
			}
			$goods_content = serialize($goods_content);
			$insert_array = array();
			if ($_POST['reply_type'] == "click_reply") {
				$insert_array['cr_title'] = trim($_POST['reply_title']);
				$insert_array['cr_note'] = trim($_POST['reply_note']);
				$insert_array['cr_type'] = 2;
				$insert_array['cr_addtime'] = time();
				$insert_array['cr_addstaff'] = $_SESSION['admin_id'];
				$insert_array['wx_id'] = $wx_id;
				$insert_array['cr_code'] = trim($_POST['cr_code']);
				$insert_array['cr_ext'] = 2;
				$insert_array['cr_content'] = $goods_content;
				$rs = $model->table('click_reply')->insert($insert_array);
			} else {
				$insert_array['reply_title'] = trim($_POST['reply_title']);
				$insert_array['reply_note'] = trim($_POST['reply_note']);
				$insert_array['reply_type'] = 2;
				$insert_array['reply_addtime'] = time();
				$insert_array['reply_addstaff'] = $_SESSION['admin_id'];
				$insert_array['wx_id'] = $wx_id;
				$insert_array['reply_ext'] = 2;
				$insert_array['reply_content'] = $goods_content;
				$rs = $model->table('reply')->insert($insert_array);
			}
			if($rs){
				showMessage("图文商品推荐创建成功！",'index.php?act=store&op=goods_manage&wx_id='.$wx_id.'&reply_type='.$_POST['reply_type'],'succ');
			}else{
				showMessage("图文商品推荐创建失败",'','error');
			}
		}
		$wx_info = $model->field('shop_data_type,store_id')->table('wxaccount')->where(array('wx_id'=>$wx_id))->find();
		Tpl::output('type', $wx_info['shop_data_type']);
		Tpl::output('store_id', $wx_info['store_id']);
		//调取商品分类（平台为一级分类）
		if ($wx_info['shop_data_type'] == 2) {//平台商家
			$url = MALL_API_URL."index.php?act=store&op=store_goods_class&store_id=".$wx_info['store_id'];
		} else {//平台自营
			$url = MALL_API_URL."index.php?act=goods_class";
		}
		$res = @file_get_contents($url);
		$res = json_decode($res,true);
		if ($wx_info['shop_data_type'] == 2) {//平台商家
			$gc_list = array();
			if (!empty($res['datas']['store_goods_class'])) {
				foreach ($res['datas']['store_goods_class'] as $val) {
					if ($val['pid'] == 0) {
						 $gc_list[] = array(
						 		'gc_id'=>$val['id'],
						 		'gc_name'=>$val['name']
						 );
						 //添加子分类
						 foreach ($res['datas']['store_goods_class'] as $cv) {
						 	if ($cv['pid'] == $val['id']) {
						 		$gc_list[] = array(
						 				'gc_id'=>$cv['id'],
						 				'gc_name'=>"&nbsp;&nbsp;&nbsp;&nbsp;|- ".$cv['name']
						 		); 
						 	}
						 }
					}
				}
			}
		} else {//平台自营
			$gc_list = array();
			if (!empty($res['datas']['class_list'])) {
				foreach ($res['datas']['class_list'] as $val) {
					$gc_list[] = array(
							'gc_id'=>$val['gc_id'],
							'gc_name'=>$val['gc_name']
					);
				}
			}
		}
		Tpl::output('gc_list', $gc_list);
		Tpl::showpage('recgoods_textpic_add');
	}
	
	/**
	 * ajax调取商品信息
	 */
	public function ajax_getgoodsOp(){
		$gc_id = intval($_GET['gc_id']);
		$keyword = trim($_GET['keyword']);
		$curpage = intval($_GET['curpage']);
		if (intval($_GET['type']) == 1) {
			$url = MALL_API_URL."index.php?act=goods&op=goods_list&page=10";
			if ($keyword != "") {
				$url .= "&keyword=".$keyword;
			} else {
				if ($gc_id != 0) {
					$url .= "&gc_id=".$gc_id;
				}
			}
		} else {
			$sid = intval($_GET['sid']);
			$url = MALL_API_URL."index.php?act=store&op=store_goods&store_id=".$sid."&stc_id=".$gc_id."&keyword=".$keyword."&page=10";
		}
		if ($curpage > 0) {
			$url .= "&curpage=".$curpage;
		}
		$res = @file_get_contents($url);
		$res = json_decode($res,true);
		if ($res['datas']['error'] != "") {
			echo json_encode(array('done'=>false,'msg'=>$res['datas']['error']));die;
		} elseif (empty($res['datas']['goods_list'])) {
			echo json_encode(array('done'=>false,'msg'=>'未找到相关商品信息'));die;
		} else {
			$pageshow = $curpage>0 ? $curpage."/".     $res['page_total'] : "1/".$res['page_total'];
			echo json_encode(array('done'=>true,'goods_list'=>$res['datas']['goods_list'],'allpage'=>$res['page_total'],'pageshow'=>$pageshow));die;
		}
	}
	
	/**
	 * 专题管理
	 */
	public function zt_manageOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		if (trim($_GET['reply_type']) == "click_reply") {
			$where_condition = array('wx_id'=>$wx_id,'cr_ext'=>3);
			if(trim($_GET['s_title']) != ''){
				$where_condition['cr_title'] = array('like','%'.trim($_GET['s_title']).'%');
			}
			$table = "click_reply";
			$order = "cr_addtime desc";
		} else {
			$where_condition = array('wx_id'=>$wx_id,'reply_ext'=>3);
			if(trim($_GET['s_title']) != ''){
				$where_condition['reply_title'] = array('like','%'.trim($_GET['s_title']).'%');
			}
			$table = "reply";
			$order = "reply_addtime desc";
		}
		$cr_list = $model->table($table)->where($where_condition)->page(10)->order($order)->select();
		Tpl::output('reply_list',$cr_list);
		Tpl::output('show_page',$model->showpage());
		Tpl::showpage('zt_manage');
	}
	
	/**
	 * 添加专题推荐
	 */
	public function reczt_addOp(){
		if(chksubmit()){
			$model = Model();
			$wx_id = intval($_GET['wx_id']);
			//表单验证
			if (trim($_POST['reply_title']) == "") {
				showMessage("请填写标题",'','error');
			}
			if ($_POST['zt_id'] == "") {
				showMessage("请填写专题",'','error');
			}
			if ($_POST['reply_type'] == "reply_type_click" && trim($_POST['cr_code']) == "") {
				showMessage("请填写标识码",'','error');
			}
			//处理专题数据
			$zt_content = array();
			if($_FILES['zt_image']['name'] != ''){
				$name_type=substr($_FILES['zt_image']['name'],-4,4);
				$file_name = md5(uniqid(rand(),true)).$name_type;
				move_uploaded_file($_FILES['zt_image']['tmp_name'],BASE_UPLOAD_PATH.'/reply/'.$file_name);
			}
			$zt_content[] = array(
				'title'=>trim($_POST['zt_title']),
				'desc'=>trim($_POST['zt_desc']),
				'picurl'=>$file_name!=''?UPLOAD_SITE_URL.'/reply/'.$file_name:'',
				'url'=>WAP_SITE_URL."special.html?special_id=".intval($_POST['zt_id'])
			);
			$zt_content = serialize($zt_content);
			$insert_array = array();
			if ($_POST['reply_type'] == "click_reply") {
				$insert_array['cr_title'] = trim($_POST['reply_title']);
				$insert_array['cr_note'] = trim($_POST['reply_note']);
				$insert_array['cr_type'] = 2;
				$insert_array['cr_addtime'] = time();
				$insert_array['cr_addstaff'] = $_SESSION['admin_id'];
				$insert_array['wx_id'] = $wx_id;
				$insert_array['cr_code'] = trim($_POST['cr_code']);
				$insert_array['cr_ext'] = 3;
				$insert_array['cr_content'] = $zt_content;
				$rs = $model->table('click_reply')->insert($insert_array);
			} else {
				$insert_array['reply_title'] = trim($_POST['reply_title']);
				$insert_array['reply_note'] = trim($_POST['reply_note']);
				$insert_array['reply_type'] = 2;
				$insert_array['reply_addtime'] = time();
				$insert_array['reply_addstaff'] = $_SESSION['admin_id'];
				$insert_array['wx_id'] = $wx_id;
				$insert_array['reply_ext'] = 3;
				$insert_array['reply_content'] = $zt_content;
				$rs = $model->table('reply')->insert($insert_array);
			}
			if($rs){
				showMessage("图文商品推荐创建成功！",'index.php?act=store&op=zt_manage&wx_id='.$wx_id.'&reply_type='.$_POST['reply_type'],'succ');
			}else{
				showMessage("图文商品推荐创建失败",'','error');
			}
		}
		Tpl::showpage('reczt_add');
	}
}