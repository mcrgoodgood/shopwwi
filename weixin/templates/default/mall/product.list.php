<?php include("header.php");?>
<div id="scrollerwp">
  <div id="scroller">
    <ul class="squares" id="thelist">
      <?php if(!empty($output['list'])){?>
      <?php foreach($output['list'] as $product){?>
      <li goods_id="<?php echo $product['goods_id']?>">
          <a>
            <div class="detail_list">
              <div class="picture"> <img src="<?php echo $product['goods_image'];?>"> </div>
              <div class="detail_info">
                <ul>
                  <li class="productTitle"><?php echo $product['goods_name'];?></li>
                  <li class="price"> ¥<span><?php echo $product['goods_store_price'];?></span>元 </li>
                  <li><span>已有<?php echo $product['goods_click'];?><?php echo $lang['nc_mall_view_person'];?></span></li>
                </ul>
              </div>
            </div>
        </a>
      </li>
      <?php }?>
      <?php }?>
    </ul>
    <div id="pullUp">
       <span class="pullUpLabel">点击获取更多商品</span>
    </div>
  </div>
</div>
<input type="hidden" name="offset" id="offset" value="1">
<script type="text/javascript">
	$(function(){
		$('#thelist li').tap(function(){
			var goods_id = $(this).attr('goods_id');
			location.href = '<?php echo $output['site']['mall_url'];?>/index.php?act=goods&goods_id='+goods_id;
		});

		$('.pullUpLabel').tap(function(){
 			var offset = $('#offset').val();
 			$.ajax({
 				type:'get',
 				url:SITE_URL+'/index.php?act=mall&op=productajax&wx_id='+wx_id+'&offset='+offset,
 				dataType:'json',
 				success:function(json){
 					var product = json.product;
 					$('#offset').val(json.offset);
 					$.each(product,function(i,val){
 						var html = '<li>'
 						      +'<a class="scroll_item">'
 						          +'<div class="detail_list">'
 						            +'<div class="picture"> <img src="'+val.goods_image+'"> </div>'
 						            +'<div class="detail_info">'
 						            + '<ul>'
 						                +'<li class="productTitle">'+val.goods_name+'</li>'
 						                +'<li class="price"> ¥<span>'+val.goods_store_price+'</span>元 </li>'
 						                +'<li>已有'+val.goods_click+'人浏览</span> </li>'
 						              +'</ul>'
 						            +'</div>'
 						          +'</div>'
 						        +'</a>'
 					      +' </li>';						
 						$('#thelist').append(html);
 					});
 				}
 			});
		});
	});
</script>
