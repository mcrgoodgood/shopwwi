<div class="main_hd">
  <h2><?php echo $lang['nc_weixin_wall_vote'];?></h2>
  <p class="extra_info"><a href="?act=activity&op=addVoteItem&wx_id=<?php echo intval($_GET['wx_id']);?>&vote_id=<?php echo intval($_GET['vote_id']);?>"><?php echo $lang['nc_add']?></a> </p>
</div>
<div class="main_bd">
  <div class="table_msg">
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell time asc"><?php echo $lang['nc_weixin_wall_vote_item_name'];?></th>
          <th class="table_cell time asc"><?php echo $lang['nc_weixin_wall_vote_item_content'];?></th>
          <th class="table_cell time asc"><?php echo $lang['nc_weixin_wall_vote_item_num'];?></th>
          <th class="table_cell desc"><?php echo $lang['nc_handle'];?></th>
        </tr>
      </thead>
      <tbody class="nc-tbody">
        <?php if(!empty($output['item'])){?>
        <?php foreach($output['item'] as $key=>$val){?>
        <tr>
          <td><?php if(mb_strlen($val['item_name'])>=30){ echo mb_substr($val['item_name'],0,30,'utf-8');}else{ echo $val['item_name']; }?></td>
          <td><?php if(mb_strlen($val['item_content'])>30){ echo mb_substr($val['item_content'],0,30,'utf-8').'...';}else{ echo $val['item_content'];}?></td>
          <td><?php echo $val['item_num'];?></td>
          <td class="last">
          	<a href="?act=activity&op=editVoteItem&wx_id=<?php echo intval($_GET['wx_id']);?>&vote_id=<?php echo intval($_GET['vote_id']);?>&item_id=<?php echo $val['item_id'];?>"><?php echo $lang['nc_edit'];?></a>
            <span>|</span>
          	<a href="javascript:;" onclick="javascript:deleteitem(<?php echo $val['item_id'];?>);"><?php echo $lang['nc_delete'];?></a>
          </td>
        </tr>
        <?php }?>
        <?php }else{?>
       	<tr>
			<td colspan="20" valign="middle" ><div class="norecord"><span><?php echo $lang['nc_record'];?></span></div></td>
		</tr>
        <?php }?>
      </tbody>
    </table>
  </div>
</div>

<script type="text/javascript">
	function deleteitem(item_id){
		if(confirm('<?php echo $lang['nc_weixin_confirm_delete_item'];?>')){
			location.href = "?act=activity&op=deleteVoteItem&wx_id=<?php echo intval($_GET['wx_id']);?>&vote_id=<?php echo intval($_GET['vote_id']);?>&item_id="+item_id;
		}
	}
</script>