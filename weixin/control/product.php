<?php
/**
 * 默认展示页面
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
define('MYSQL_RESULT_TYPE',1);
class productControl extends BaseHomeControl{
	
	public function __construct(){
		parent::__construct();
		Tpl::output('sign','product');
	}

	public function indexOp(){
		$this->listOp();
	}
	
	/*
	 * 商品列表
	 */
	public function listOp(){
		$model = Model();
		$list = $model->table('product')->where(array('wx_id'=>intval($_GET['wx_id'])))->page(15)->order("product_time desc")->select();
		Tpl::output('list',$list);
		Tpl::showpage('product.list');
	}
	
	/*
	 * 添加商品
	 */	
	public function addProductOp(){
		if(isset($_POST) && !empty($_POST)){	
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam = array(
					array("input"=>trim($_POST['product_name']),"require"=>"true","message"=>'商品名称不能为空'),
					array("input"=>trim($_POST['product_price']),"require"=>"true","message"=>'商品价格不能为空')
			);
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage($error,'','error');
			}
			
			$params			=	array();	
			if(!empty($_FILES['product_pic'])){//上传图片
				$name_type=substr($_FILES['product_pic']['name'],-4,4);
				$file_name = md5(uniqid(rand(),true)).$name_type;
				move_uploaded_file($_FILES['product_pic']['tmp_name'],BASE_UPLOAD_PATH.'/products/'.$file_name);
				$params['product_pic']  = $file_name;
			}
			
			
			$params['product_name']  = trim($_POST['product_name']);
			$params['product_price'] = $_POST['product_price'];
			$params['product_state'] = intval($_POST['product_state']);
			$params['product_recommend'] = intval($_POST['product_recommend']);
			$params['product_time']	 = time();
			$params['product_content'] = trim($_POST['product_content']);
			$params['wx_id']		 = intval($_GET['wx_id']);	
			
			$model = Model();
			$res = $model->table('product')->insert($params);
			if($res){
				showMessage('添加商品成功','?act=product&op=list&wx_id='.intval($_GET['wx_id']),'succ');
			}else{
				showMessage('添加商品失败','?act=product&op=list&wx_id='.intval($_GET['wx_id']),'error');
			}
		}
		Tpl::showpage('product.add');
	}
	
	
	/*
	 * 编辑导航
	 */	
	public function editProductOp(){
		if(isset($_POST) && !empty($_POST)){
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam = array(
					array("input"=>trim($_POST['product_name']),"require"=>"true","message"=>'商品名称不能为空'),
					array("input"=>trim($_POST['product_price']),"require"=>"true","message"=>'商品价格不能为空')
			);
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage($error,'','error');
			}
			
			$params			=	array();	
			if(!empty($_FILES['product_pic'])){//上传图片
				$name_type=substr($_FILES['product_pic']['name'],-4,4);
				$file_name = md5(uniqid(rand(),true)).$name_type;
				move_uploaded_file($_FILES['product_pic']['tmp_name'],BASE_UPLOAD_PATH.'/products/'.$file_name);
				$params['product_pic']  = $file_name;
			}
			
			$params['product_name']  = trim($_POST['product_name']);
			$params['product_price'] = $_POST['product_price'];
			$params['product_state'] = intval($_POST['product_state']);
			$params['product_recommend'] = intval($_POST['product_recommend']);
			$params['product_content'] = trim($_POST['product_content']);
			$params['wx_id']		 = intval($_GET['wx_id']);

			$condition	 = array();//条件
			$condition['product_id'] = intval($_POST['product_id']);
			$condition['wx_id']		 = intval($_GET['wx_id']);
			
			$model = Model();
			$res = $model->table('product')->where($condition)->update($params);
			if($res){
				showMessage('编辑商品成功','?act=product&op=list&wx_id='.intval($_GET['wx_id']),'succ');
			}else{
				showMessage('编辑商品失败','?act=product&op=list&wx_id='.intval($_GET['wx_id']),'error');
			}
		}
		
		$model = Model();
		$condition 		= array();
		$condition['product_id'] = intval($_GET['product_id']);
		$condition['wx_id']		 = intval($_GET['wx_id']);
		
		$product = $model->table('product')->where($condition)->find();
		if(empty($product)){
			showMessage('该商品不存在');
		}
		
		Tpl::output('product',$product);		
		Tpl::showpage('product.edit');
	}
	
	/*
	 * 删除商品
	 */	
	public function delProductOp(){		
		$model = Model();
		$res = $model->table('product')->where(array('wx_id'=>intval($_GET['wx_id']),'product_id'=>intval($_POST['product_id'])))->delete();
		//wx_id 微信ID product_id 商品ID
		
		if($res){
			showMessage('删除商品成功');
		}else{
			showMessage('删除商品失败');
		}	
	}
	
}