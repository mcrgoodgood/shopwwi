<div class="main_hd">
  <h2><?php echo $lang['nc_weixin_wall_vote'];?></h2>
</div>
<div class="main_bd">
  <form method="post" id="apply_form" action="?act=activity&op=addVoteItem&wx_id=<?php echo intval($_GET['wx_id']);?>&vote_id=<?php echo intval($_GET['vote_id']);?>">
    <ul>
      <li class="list_item">
        <label class="label_box"><?php echo $lang['nc_weixin_wall_vote_item_name'];?></label>
        <div class="label_form"> 
          <span>
          	<input type="text" class="label_input" name="item_name" id="item_name">
            <label for='item_name' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>

      <li class="list_item">
        <label class="label_box"><?php echo $lang['nc_weixin_wall_vote_item_color'];?></label>
        <div class="label_form"> 
          <span>
          	<input type="text" value="#92cddc" id="color" name="item_color">
            <label for='item_content' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>
      
      <li class="list_item">
        <label class="label_box"><?php echo $lang['nc_weixin_wall_vote_item_content'];?></label>
        <div class="label_form"> 
          <span>
          	<input type="text" class="label_input" name="item_content" id="item_content">
            <label for='item_content' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>
      
      <li>
        <div class="btn_bar">
        	<input type="submit" class="btn_input" value="<?php echo $lang['nc_save'];?>">
        </div>
      </li>
    </ul>
  </form>
</div>

<link rel="stylesheet" type="text/css" href="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<link rel="stylesheet" type="text/css" href="<?php echo TEMPLATES_URL; ?>/css/evol.colorpicker.css" rel="stylesheet">
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery-ui/jquery.ui.js"></script> 
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/evol.colorpicker.js"></script> 
<script type="text/javascript">
$(function(){	
	$('#color').colorpicker({
		displayIndicator: false
	});
	
	$("#apply_form").validate({
        rules: {
        	item_name: {
				required:true
            },
            item_content:{
            	required:true
            }
        },
        messages:{
        	item_name:{
        		required:'<?php echo $lang['nc_weixin_wall_vote_item_name_is_not_null'];?>'
            },
            item_content:{
            	required:'<?php echo $lang['nc_weixin_wall_vote_item_content_is_not_null'];?>'
            }
        }
	});
});
</script> 
