<?php
/**
 * 默认展示页面
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
define('MYSQL_RESULT_TYPE',1);
class articleControl extends BaseHomeControl{
	
	public function __construct(){
		parent::__construct();
	}

	public function indexOp(){
		$this->listOp();
	}
	
	/*
	 * 文章列表
	 */
	public function listOp(){		
		$model = Model();
		$list = $model->table('article')->where(array('wx_id'=>intval($_GET['wx_id'])))->order('article_id desc')->select();
		
		Tpl::output('list',$list);
		Tpl::output('show_page',$model->showpage());
		
		Tpl::output('sign','article');
		Tpl::showpage('article.list');
	}
	
	
	/*
	 * 新增文章
	 */
	public function addArticleOp(){
		if(isset($_POST) && !empty($_POST)){
			
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam	=	array(
					array("input"=>trim($_POST['article_title']),"require"=>"true","message"=>'文章标题不能为空')
			);
				
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage(Language::get('error').$error,'','error');
			}
			
			$params = array();
			$params['article_title'] = trim($_POST['article_title']);
			$params['article_content'] = $_POST['article_content'];
			$params['article_time']	 = time();
			$params['wx_id']		 = intval($_GET['wx_id']);	
			
			$model = Model();
			$res = $model->table('article')->insert($params);
			if($res){
				showMessage('添加文章成功','?act=article&wx_id='.intval($_GET['wx_id']),'succ');
			}else{
				showMessage('添加文章失败','?act=article&wx_id='.intval($_GET['wx_id']),'error');
			}
		}
		Tpl::output('sign','article');
		Tpl::showpage('article.add');		
	}

	/*
	 * 编辑文章
	 */	
	public function editArticleOp(){
		
		if(isset($_POST) && !empty($_POST)){
			
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam	=	array(
					array("input"=>trim($_POST['article_title']),"require"=>"true","message"=>'文章标题不能为空')
			);
			
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage(Language::get('error').$error,'','error');
			}
			
			$params = array();
			$params['article_title'] = trim($_POST['article_title']);
			$params['article_content'] = $_POST['article_content'];
			$params['article_time']	 = time();
			$params['wx_id']		 = intval($_GET['wx_id']);
				
			$model = Model();
			$condition = array();
			$condition['article_id'] = intval($_POST['article_id']);
						
			$res = $model->table('article')->where($condition)->update($params);
			if($res){
				showMessage('编辑文章成功','?act=article&wx_id='.intval($_GET['wx_id']),'succ');
			}else{
				showMessage('编辑文章失败','?act=article&wx_id='.intval($_GET['wx_id']),'error');
			}
		}
		
		$condition = array();//条件
		$condition['wx_id'] = intval($_GET['wx_id']);
		$condition['article_id'] = intval($_GET['article_id']);
		
		$model = Model();
		$article = $model->table('article')->where($condition)->find();
		Tpl::output('article',$article);
		Tpl::output('sign','article');
		Tpl::showpage('article.edit');
			
	}
	
	/*
	 * 删除文章
	 */
	public function delArticleOp(){
		$condition = array();
		$condition['wx_id'] = intval($_GET['wx_id']);
		$condition['article_id'] = array('in',trim($_POST['article_id']));
		
		
		$model = Model();
		$res = $model->table('article')->where($condition)->delete();
		
		if($res){
			showMessage('删除文章成功','?act=article&wx_id='.intval($_GET['wx_id']),'succ');
		}else{
			showMessage('删除文章失败','?act=article&wx_id='.intval($_GET['wx_id']),'error');
		}	
	}
	
}