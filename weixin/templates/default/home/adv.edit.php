<div class="main_hd">
  <h2><?php echo $lang['nc_adv_manage'];?></h2>
</div>
<div class="main_bd">
  <form method="post" id="apply_form" action="?act=adv&op=editAdv&wx_id=<?php echo intval($_GET['wx_id']);?>" enctype="multipart/form-data">
    <ul>
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo $lang['nc_adv_name'];?>:</label>
        <div class="label_form"> 
          <span>
          	<input type="text" class="label_input" name="adv_name" id="adv_name" value="<?php echo $output['adv']['adv_name'];?>">
            <label for='adv_name' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>
      
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo $lang['nc_adv_sort'];?>:</label>
        <div class="label_form">
          <span>
          	<input type="text" class="label_input" name="adv_sort" id="adv_sort" value="<?php echo $output['adv']['adv_sort'];?>">
            <label for='adv_sort' class='error msg_invalid' style='display:none;'></label>
          </span> 
        </div>
      </li>
      
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo $lang['nc_adv_state'];?>:</label>
        <div class="label_form">
          <span>
				<input type="radio" name="adv_state" value="0" <?php if($output['adv']['adv_state']=='0'){ echo 'checked';}?>>&nbsp;开启 &nbsp;&nbsp;
				<input type="radio" name="adv_state" value="1" <?php if($output['adv']['adv_state']=='1'){ echo 'checked';}?>>&nbsp;关闭
          </span> 
        </div>
      </li>     
      
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo $lang['nc_adv_pic'];?>:</label>
        <div class="label_form">
          <span>
			<input type="file" name="adv_pic" id="adv_pic">
			<label for='adv_pic' class='error msg_invalid' style='display:none;'></label>
          </span> 
        </div>
      </li>       
 
      <li class="list_item">
        <label class="label_box">&nbsp;</label>
        <div class="label_form">
          <span>
			<img src="<?php echo UPLOAD_SITE_URL;?>/adv/<?php echo $output['adv']['adv_pic'];?>">
          </span> 
        </div>
      </li>  
            
      <li>
        <div class="btn_bar">
        	<input type="hidden" name="adv_id" value="<?php echo $output['adv']['adv_id'];?>">
        	<input type="submit" class="btn_input" value="<?php echo $lang['nc_save'];?>">
        </div>
      </li>
    </ul>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script> 
<script type="text/javascript">
$(function(){	
	$("#apply_form").validate({//验证表单
        rules: {
        	adv_name: {
				required:true
            },
            adv_sort:{
            	required:true,
            	number:true,
            	min:0,
            	max:100
            },
            <?php if(empty($output['adv']['adv_pic'])){?>
            adv_pic:{
            	required:true,
            	accept : 'jpg|jpeg|gif|png'
            }
            <?php }?>
        },
        messages:{
        	adv_name:{
        		required:'<?php echo $lang['nc_adv_name_is_not_null'];?>'
            },
            adv_sort:{
            	required:'<?php echo $lang['nc_adv_sort_is_not_null'];?>',
            	number:'<?php echo $lang['nc_adv_sort_is_not_number'];?>',
            	min:'<?php echo $lang['nc_adv_sort_range'];?>',
            	max:'<?php echo $lang['nc_adv_sort_range'];?>'
            },
            <?php if(empty($output['adv']['adv_pic'])){?>
            adv_pic:{
            	required:'<?php echo $lang['nc_adv_pic_is_not_null'];?>',
            	accept : '<?php echo $lang['nc_adv_pic_is_not_format'];?>'				
            }
            <?php }?>
        }
	});
});
</script> 
