define(function(require, exports, module){
	module.exports = {
		src:[
			TEMPLATESURL+'/images/wdy/bg.jpg',
			TEMPLATESURL+'/images/wdy/ellipse.png',
			TEMPLATESURL+'/images/wdy/error.png',
			TEMPLATESURL+'/images/wdy/intro-bot.jpg',
			TEMPLATESURL+'/images/wdy/intro.jpg',
			TEMPLATESURL+'/images/wdy/load.gif',
			TEMPLATESURL+'/images/wdy/next.png',
			TEMPLATESURL+'/images/wdy/qus-bg_hei.jpg',
			TEMPLATESURL+'/images/wdy/qus-bg_hong.jpg',
			TEMPLATESURL+'/images/wdy/qus-sel.png',
			TEMPLATESURL+'/images/wdy/qus-tri.png',
			TEMPLATESURL+'/images/wdy/qus-unsel.png',
			TEMPLATESURL+'/images/wdy/res-bg.jpg',
			TEMPLATESURL+'/images/wdy/res-show.png',
			TEMPLATESURL+'/images/wdy/start.png',
			TEMPLATESURL+'/images/wdy/submit.png'
		]
	};
});