<div class="main_hd">
  <h2><?php echo $lang['nc_weixin_wall_activity'];?></h2>
  <p class="extra_info"> <a href="?act=activity&op=addActivity&wx_id=<?php echo intval($_GET['wx_id']);?>"><?php echo $lang['nc_add']?></a> </p>
</div>
<div class="main_bd">
  <div class="table_msg">
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell time asc"><?php echo $lang['nc_weixin_wall_activity_name'];?></th>
          <th class="table_cell goods"><?php echo $lang['nc_weixin_wall_add_time'];?></th>
          <th class="table_cell desc"><?php echo $lang['nc_handle'];?></th>
        </tr>
      </thead>
      <tbody class="nc-tbody">
        <?php if(!empty($output['list'])){?>
        <?php foreach($output['list'] as $key=>$val){?>
        <tr>
          <td><?php if(mb_strlen($val['activity_name'],'utf-8')>=20){ echo mb_substr($val['activity_name'],0,20,'utf-8').'...';}else{ echo $val['activity_name'];}?></td>
          <td><?php echo date("Y-m-d",$val['activity_time']);?></td>
          <td class="last"><a href="?act=wall&op=message&activity_id=<?php echo $val['activity_id'];?>" target="_blank"><?php echo $lang['nc_show'];?></a> <span>|</span> <a href="?act=wall&op=lottery&activity_id=<?php echo $val['activity_id'];?>" target="_blank"><?php echo $lang['nc_weixin_wall_activity_prize'];?></a> <span>|</span> <a href="?act=wall&op=rank&activity_id=<?php echo $val['activity_id']?>" target="_blank"><?php echo $lang['nc_weixin_wall_activity_rank'];?></a> <span>|</span> <a href="?act=activity&op=editActivity&wx_id=<?php echo intval($_GET['wx_id']);?>&activity_id=<?php echo $val['activity_id']?>"><?php echo $lang['nc_edit'];?></a> <span>|</span> <a href="javascript:void(0);" onclick="javascript:deleteitem(<?php echo $val['activity_id'];?>);"><?php echo $lang['nc_delete']?></a> <span>|</span> <a href="?act=activity&op=message&wx_id=<?php echo intval($_GET['wx_id']);?>&activity_id=<?php echo $val['activity_id'];?>"><?php echo $lang['nc_weixin_wall_message_view_message'];?></a> <span>|</span> <a href="?act=activity&op=lotterylist&wx_id=<?php echo intval($_GET['wx_id'])?>&activity_id=<?php echo $val['activity_id'];?>"><?php echo $lang['nc_weixin_wall_message_view_prize'];?></a></td>
        </tr>
        <?php }?>
        <?php }else{?>
        <tr>
          <td colspan="20" valign="middle" ><div class="norecord"><span><?php echo $lang['nc_record'];?></span></div></td>
        </tr>
        <?php }?>
      </tbody>
    </table>
  </div>
  <div class="pagination"> <?php echo $output['show_page'];?> </div>
</div>
<script type="text/javascript">
	function deleteitem(activity_id){
		if(confirm('<?php echo $lang['nc_weixin_confirm_delete_item'];?>')){
			location.href = "?act=activity&op=delActivity&wx_id=<?php echo intval($_GET['wx_id']);?>&activity_id="+activity_id;
		}
	}
</script> 
