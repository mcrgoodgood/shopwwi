
import MobileLayout from '../pages/layouts/Default.vue';

import DefaultIndex from '../pages/default/Index.vue';

export default {
  path: '/m',
  component: MobileLayout,
  children: [
    { path: '/', component: DefaultIndex },
  ]
};