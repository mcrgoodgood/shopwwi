<?php
/**
 * 商城板块初始化文件
 *
 *
 *
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
//define('APP_ID','shop');
define('BASE_PATH',str_replace('\\','/',dirname(__FILE__)));
define('BASE_ROOT_PATH',str_replace('\\','/',dirname(dirname(__FILE__))));
define('BRANCH_NAME','');//项目分支目录

if (!@include(BASE_PATH.'/global.php')) exit('global.php isn\'t exists!');
if (!@include(BASE_CORE_PATH.'/shopwwi.php')) exit('shopwwi.php isn\'t exists!');

//define('APP_SITE_URL',SHOP_SITE_URL);
define('RESOURCE_SITE_URL',RESOURCE_SITE_URL);
define('TEMPLATES_URL',SHOP_SITE_URL.'/templates/'.TPL_NAME);
define('BASE_TPL_PATH',BASE_PATH.'/templates/'.TPL_NAME);

//框架扩展
require(BASE_PATH.'/framework/function/core.php');

if (!@include(BASE_PATH.'/control/control.php')) exit('control.php isn\'t exists!');

Base::run();
?>