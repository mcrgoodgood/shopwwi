<?php
/**
 * 核心文件
 *
 * 核心初始化类，不允许继承
 *
 * @package    core
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi5.com
 * @author	   ShopWWI Team
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
final class Base{

	const CPURL = '';

	/**
	 * 运行
	 */
	public static function run(){
			self::cp();			
			//配置信息	
			global $setting_config;
			self::parse_conf($setting_config);
			define('MD5_KEY',md5($setting_config['md5_key']));

			if(function_exists('date_default_timezone_set')){
				if (is_numeric($setting_config['time_zone'])){
					@date_default_timezone_set('Asia/Shanghai');
				}else{
					@date_default_timezone_set($setting_config['time_zone']);
				}				
			}

			//开始会话
			self::start_session();
			
			//输出到模板
			Tpl::output('setting_config',$setting_config);
			
			Language::read('core_lang_index');
	
			//执行 control
			self::control();
	}

	/**
	 * 取得配置信息
	 */
	private static function parse_conf(&$setting_config){
		$nc_config = $GLOBALS['config'];
		//处理从数据库配置
		if(is_array($nc_config['db']['slave']) && !empty($nc_config['db']['slave'])){
			//如果为读写分离模式则随机选择一台从数据库服务器
			$dbslave = $nc_config['db']['slave'];
			$sid     = array_rand($dbslave);
			$nc_config['db']['slave'] = $dbslave[$sid];
		}else{
			//否则从主数据库读
			$nc_config['db']['slave'] = $nc_config['db'][1];
		}

		//处理主数据库配置
		$nc_config['db']['master'] = $nc_config['db'][1];
		//该行必须在H函数使用之前
		$setting_config = $nc_config;
		//取得基本配置,此处只支持文件缓存
		$setting = ($setting = H('setting')) ? $setting : H('setting',true);
		$setting['shopwwi_version'] = '<span class="vol"><font class="b">Shop</font><font class="o">WWI</font><em></em></span>';
		$setting_config = array_merge_recursive($setting,$nc_config);
	}

	/**
	 * 控制器调度
	 *
	 */
	private static function control(){
		//判断是否是系统后台的内容
		if (defined('BRANCH_NAME') && BRANCH_NAME != ''){
			$act_file = realpath(BASE_PATH.DS.BRANCH_NAME.DS.'control'.DS.$_GET['act'].'.php');
		}else {
			//二级域名
			$act_file = realpath(BASE_PATH.'/control/'.$_GET['act'].'.php');
		}
		if (is_file($act_file)){
			require($act_file);
			$class_name = $_GET['act'].'Control';
			if (class_exists($class_name)){
				$main = new $class_name();
				$function = $_GET['op'].'Op';

				if (method_exists($main,$function)){
					$main->$function();
				}elseif (method_exists($main,'indexOp')){
					$main->indexOp();
				}else {
					$error = "Base Error: function $function not in $class_name!";
					throw_exception($error);
				}
			}else {
				$error = "Base Error: class $class_name isn't exists!";
				throw_exception($error);
			}
		}else {
			$error = "Base Error: access file isn't exists!";
			throw_exception($error);
		}
	}

	/**
	 * 开启session
	 *
	 */
	private static function start_session(){
		if ($GLOBALS['setting_config']['subdomain_suffix']){
			$subdomain_suffix = $GLOBALS['setting_config']['subdomain_suffix'];
		}else{
			if (preg_match("/^[0-9.]+$/",$_SERVER['HTTP_HOST'])){
				$subdomain_suffix = $_SERVER['HTTP_HOST'];
			}else{
				$split_url = explode('.',$_SERVER['HTTP_HOST']);
				if($split_url[2] != '') unset($split_url[0]);
				$subdomain_suffix = implode('.',$split_url);
			}
		}
		if ($subdomain_suffix != 'localhost'){
			@ini_set('session.cookie_domain', $subdomain_suffix);
		}
		//开启以下配置支持session信息存信memcache
		/*@ini_set("session.save_handler", "memcache");
		@ini_set("session.save_path", C('memcache.1.host').':'.C('memcache.1.port'));*/

		//默认以文件形式存储session信息
		session_save_path(BASE_DATA_PATH.'/session');
		session_start();
	}
	
	public static function autoload($class){
		$class = strtolower($class);
		if (ucwords(substr($class,-5)) == 'Class' ){
			if (!@include_once(BASE_PATH.'/framework/libraries/'.substr($class,0,-5).'.class.php')){
				exit("Class Error: {$class}.isn't exists!");
			}
		}elseif (ucwords(substr($class,0,5)) == 'Cache' && $class != 'cache'){
			if (!@include_once(BASE_CORE_PATH.'/framework/cache/'.substr($class,0,5).'.'.substr($class,5).'.php')){
				exit("Class Error: {$class}.isn't exists!");
			}
		}elseif ($class == 'db'){
			if (!@include_once(BASE_CORE_PATH.'/framework/db/'.strtolower(DBDRIVER).'.php')){
				exit("Class Error: {$class}.isn't exists!");
			}
		}else{
			if (!@include_once(BASE_CORE_PATH.'/framework/libraries/'.$class.'.php')){
				exit("Class Error: {$class}.isn't exists!");
			}
		}
	}

	/**
	 * 合法性验证
	 *
	 */
	private static function cp(){
		if (self::CPURL == '') return;
		if (strpos(self::CPURL,'||') !== false){
			$a = explode('||',self::CPURL);
			foreach ($a as $v) {
				$d = strtolower(stristr($_SERVER['HTTP_HOST'],$v));
				if ($d == strtolower($v)){
					return;
				}else{
					continue;
				}
			}
			header('location: ');exit();			
		}else{
			$d = strtolower(stristr($_SERVER['HTTP_HOST'],self::CPURL));
			if ($d != strtolower(self::CPURL)){
				header('location: ');exit();
			}
		}
	}
}