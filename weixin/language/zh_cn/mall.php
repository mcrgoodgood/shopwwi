<?php
defined('InByShopWWI') or exit('Access Invalid!');

$lang['nc_mall_nav_goodslist']	= '商品列表';
$lang['nc_mall_nav_mallnews']	= '商城动态';
$lang['nc_mall_nav_mallintro']  = '商城介绍';
$lang['nc_mall_nav_contactus']	= '联系我们';
$lang['nc_mall_nav_aboutus']	= '关于我们';
$lang['nc_mall_nav_more']		= '更多信息';
$lang['nc_mall_view_person']	= '人浏览';
	
