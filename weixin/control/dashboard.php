<?php
/**
 * 公共账号Dashboard界面
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
class dashboardControl extends BaseHomeControl{
	public function indexOp(){
		
		$model = Model();
		$activity_count = $model->table('activity')->where(array('activity_wx_id'=>intval($_GET['wx_id'])))->count();
		Tpl::output('activity_count',$activity_count);//活动数
		
		$lottery_count = $model->table('lottery')->where(array('wx_id'=>intval($_GET['wx_id'])))->count();
		Tpl::output('lottery_count',$lottery_count);//抽奖数
		
		$fans_count = $model->table('fans')->where(array('wx_id'=>intval($_GET['wx_id'])))->count();
		Tpl::output('fans_count',$fans_count);//粉丝数
		
		$vote_count = $model->table('vote')->where(array('vote_wx_id'=>intval($_GET['wx_id'])))->count();
		Tpl::output('vote_count',$vote_count);//投票活动数
		
		Tpl::showpage('wx_dashboard');
	}
}