/* 多级选择相关函数，如选择，分类选择
 * common_select
 */

/* 商品分类选择函数 */
function gcategoryInit(divId) {
    $('#'+divId).on('click', '[data-id]', gcategoryChange); // select的onchange事件
    window.onerror = function(){return true;}; //屏蔽jquery报错
}
var oBtn = false;
function gcategoryChange(e) {
    oBtn = true;
    var $this = $(this);
    var _parent = $this.parent().parent();
    // 删除后面的select
    _parent.nextAll("tr").remove();

    $(this).addClass('selected').siblings('a').removeClass('selected');
    // ajax请求下级分类
    var url = SITEURL + '/index.php?app=index&wwi=josn_class&callback=?';

    var id = $this.data('id');
    var $parent = $(e.delegateTarget);
    var level = (/\d/).test($parent.attr('id')) || '';

    $.getJSON(url, {'gc_id':id}, function(data){
        if (data && data.length>0 && oBtn){
            oBtn = false;
            level++;
            var _parent = $this.parent().parent();
            $('<tr><th  class="w100"><strong>分类：</strong></th><td colspan="2" id="gc'+ level +'"></td></tr>').insertAfter(_parent);

            for (var i = 0; i < data.length; i++) {
                _parent.next('tr').find('td').eq(0).append('<a href="javascript:;" data-id="' + data[i].gc_id + '">' + data[i].gc_name + '<a/> ');
            }
            $('#gc'+ level).on('click', '[data-id]', gcategoryChange);
        }
    });
}
