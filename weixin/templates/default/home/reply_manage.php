<?php defined('InByShopWWI') or exit('Access Invalid!');?>

<div class="main_hd">
  <h2><?php echo L('ar_reply_manage'); ?></h2>
  <p class="extra_info"><a href="index.php?act=autoreply&op=reply_add&wx_id=<?php echo intval($_GET['wx_id']); ?>"><?php echo L('nc_add'); ?></a></p>
</div>
<div class="main_bd">
  <div class="zt"> <span><?php echo L('ar_subject').L('nc_colon'); ?></span>
    <input name="s_title" type="text" class="label_input_zt" value="<?php echo trim($_GET['s_title']); ?>"/>
    <span>类型：</span>
    <select name="s_type" style="float:left">
    	<option value="0">请选择类型</option>
    	<option value="1" <?php if (intval($_GET['s_type']) == 1) { ?>selected<?php } ?> >纯文字</option>
    	<option value="2" <?php if (intval($_GET['s_type']) == 2) { ?>selected<?php } ?> >图文</option>
    	<option value="3" <?php if (intval($_GET['s_type']) == 3) { ?>selected<?php } ?> >语音</option>
    </select>
    <input type="button" class="search-button" id="search" style="margin-left: 10px"/>
  </div>
  <div class="table_msg">
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell" style="width:35%"><?php echo L('ar_subject'); ?></th>
          <th class="table_cell" style="width:10%"><?php echo L('ar_type'); ?></th>
          <th class="table_cell" style="width:20%"><?php echo L('ar_addtime'); ?></th>
          <th class="table_cell" style="width:15%"><?php echo L('nc_handle'); ?></th>
        </tr>
      </thead>
      <tbody class="nc-tbody">
        <?php if(!empty($output['reply_list'])){?>
        <?php foreach($output['reply_list'] as $key=>$val){?>
        <tr>
          <td><?php echo $val['reply_title']; ?></td>
          <td><?php 
			switch ($val['reply_type']) {
				case 1:
					echo "纯文字";
					break;
				case 2:
					echo "图文";
					break;
				case 3:
					echo "语音";
					break;
			}
          ?></td>
          <td><?php echo date('Y-m-d H:i:s',$val['reply_addtime']); ?></td>
          <td class="last"><a href="index.php?act=autoreply&op=reply_edit&reply_id=<?php echo $val['reply_id']; ?>&wx_id=<?php echo intval($_GET['wx_id']); ?>"><?php echo L('nc_edit'); ?></a> <span>|</span> <a href="javascript:if(confirm('<?php echo L('ar_reply_del_confirm'); ?>'))window.location.href='index.php?act=autoreply&op=reply_del&reply_id=<?php echo $val['reply_id']; ?>&wx_id=<?php echo intval($_GET['wx_id']); ?>';"><?php echo L('nc_delete'); ?></a></td>
        </tr>
        <?php }?>
        <?php }else { ?>
        <tr>
          <td colspan="15" style="text-align:center"><?php echo L('ar_no_record'); ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
  <div class="pagination"> <?php echo $output['show_page'];?> </div>
</div>
<script type="text/javascript">
$(function(){
	$('#search').click(function(){
		var s_title = $('input[type="text"][name="s_title"]').val();
		var s_type = $('select[name="s_type"]').val();
		window.location.href = 'index.php?act=autoreply&op=reply_manage&wx_id=<?php echo intval($_GET['wx_id']); ?>&s_title='+s_title+'&s_type='+s_type;
	});
})
</script>