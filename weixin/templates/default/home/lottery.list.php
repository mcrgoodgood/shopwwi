<div class="main_hd">
  <h2><?php echo $lang['nc_weixin_wall_lottery'];?></h2>
  <p class="extra_info"></p>
</div>
<div class="main_bd">
  <div class="table_msg">
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell time asc"><?php echo $lang['nc_weixin_wall_activity_name'];?></th>
          <th class="table_cell tradeId"><?php echo $lang['nc_weixin_wall_activity_user_name'];?></th>
          <th class="table_cell goods" width="22%"><?php echo $lang['nc_weixin_wall_activity_lottery_time'];?></th>
        </tr>
      </thead>
      <tbody class="nc-tbody">
        <?php if(!empty($output['lottery'])){?>
        <?php foreach($output['lottery'] as $key=>$val){?>
        <tr>
          <td><?php if(mb_strlen($val['activity_name'],'utf-8')>=30){ echo mb_substr($val['activity_name'],0,30,'utf-8').'...'; }else{ echo $val['activity_name'];}?></td>
          <td><?php echo $val['user_name'];?></td>
          <td class="al last"><?php echo date("Y-m-d H:i",$val['lottery_time']);?></td>
        </tr>
        <?php }?>
        <?php }else{?>
       	<tr>
			<td colspan="20" valign="middle" ><div class="norecord"><span><?php echo $lang['nc_record'];?></span></div></td>
		</tr>
        <?php }?>
      </tbody>
    </table>
 
  </div> <div class="pagination"> <?php echo $output['show_page'];?> </div>
</div>
  