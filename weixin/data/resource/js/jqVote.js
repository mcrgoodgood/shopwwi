/*投票显示定义--JqVote类*/
//voteDiv:JQUERY对象，要渲染的DOM
//label投票的总标题，如果不需要则传入null;
//voteWidth 数字，完整投票容器的总宽度;
//titleWidth 数字，每个投票项标题的宽度(默认为100%，选择默认时，传入null);
//slipWidth 数字，每个投票量BAR的总宽度;
//lineHeigth数字，每个投票行的行高(默认为0，表示自动适应，但在IE6下，必须设一个大于0的值！)
//units:详细数据显示单位,不需要单位时，传入null
//havePercent:boolean,决定了是否要显示百分率
//投票数量条BAR一律设定高为11px
function JqVote(voteDiv,label,voteWidth,titleWidth,slipWidth,lineHeigth,units,havePercent){
	//初始化背景框
	this.$voteDiv=voteDiv;
	this.$voteDiv.addClass("vote_bg");
	this.$voteDiv.css({width:voteWidth+"px"});
	//初始化标题
	if(label!=null){
		this.$voteDiv.prepend("<div class='vote_label'>"+label+"</div>");
	}
	//初始化容器框
	this.$voteDiv.append("<div class='vote_blank'></div>");
	this.$voteBlank=this.$voteDiv.children(".vote_blank");
	//设定杂项参数
	this.$titleWidth=titleWidth;
	this.$slipWidth=slipWidth;
	this.$lineHeigth=lineHeigth;
	this.$units=units;
	this.$havePercent=havePercent;
	this.$totalNum=null;
	
	//以下为类内方法提供调用
	//设置标题
	this.setLabel=function(label){
		if(this.$voteDiv.children(".vote_label")==null)
			this.$voteDiv.prepend("<div class='vote_label'>"+label+"</div>");
		else
			this.$voteDiv.children(".vote_label").html(label);
	};
	//载入数据，可以全部载入，或者单独载入某种属性值，依传入的JSON格式而定,通用方法
	this.loadData=function(json){
		//只有当this.$totalNum不为空时，才能载入显示投票数量,否则，需要重新运算
		if(json.totalNum!=null)
			this.$totalNum=json.totalNum;
		else{
			this.$totalNum=0;
			for(i=0;i<json.list.length;++i){
				this.$totalNum+=json.list[i].num;
			}
		}
		var step=this.$totalNum/this.$slipWidth;
		var obj;
		for(i=0;i<json.list.length;++i){
			obj=this.$voteBlank.children("#"+json.list[i].id);
			//如果投票项已经存在
			if(obj.length!=0){
				text="";
				if(json.inputType!=null&&json.inputName!=null)
					text+="<input type='"+json.inputType+"' name='"+json.inputName+"' value='"+json.list[i].inputValue+"'/>";
				text+=json.list[i].title;
				if(json.list[i].title!=null)	obj.children(".title").html(text);
				if(json.list[i].num!=null){
					obj.children(".slip_bg").html("<div class='slip_bar' style='"
					+(json.list[i].color!=null?  "background-color:"+json.list[i].color+";" : "")
					+"width:"+(Math.round(json.list[i].num/step)+2)+"px;'><em><b></b></em></div>");
					text=json.list[i].num+this.$units;
					if(this.$havePercent){
						text+="("+(json.list[i].num/this.totalNum)*100;
						text=text.substring(0,text.indexOf(".") + 3);
						text+="%)";
					}
					obj.children(".note").text(text);
				}
			}
			//如果投票项还不存在
			else{
				strDom="<div id='"+json.list[i].id+"' class='vote_line'"
				+(this.$lineHeigth==0? "":" style='height:"+this.$lineHeigth+"px'")
				+">";
				strDom+="<div class='title'"+(this.$titleWidth==null? "":" style='width:"+this.$titleWidth+"px'")+">"
				+((json.inputType!=null&&json.inputName!=null)? "<input type='"+json.inputType+"' name='"+json.inputName+"' value='"+json.list[i].inputValue+"'/>":"")+json.list[i].title+"</div>";
				strDom+="<div class='slip_bg' style='width:"+this.$slipWidth+"px;"
				+(this.$titleWidth!=null? "":"margin-left:40px;")+"'>";
				strDom+="<div class='slip_bar' style='background-color:"+json.list[i].color+";width:"
				+(Math.round(json.list[i].num/step)+2)+"px;'><em><b></b></em></div>";
				text=json.list[i].num+this.$units;
				if(this.$havePercent){
					text+="("+(json.list[i].num/this.$totalNum)*100;
					if(text.indexOf(".")>-1) 
						text=text.substring(0,text.indexOf(".") + 3);
					text+="%)";
				}
				strDom+="</div><div class='note'>"+text+"</div></div>";
				this.$voteBlank.append(strDom);
			}
		}
	};
	//清空所有结果
	this.clearAllData=function(){
		this.$voteBlank.empty();
	};
	//删除投票栏中，第N个显示结果行，（当为第一行时，N=1）
	this.delSlipLine=function(n){
		if(n>0)	this.$voteBlank.children("div:eq("+(n-1)+")").remove();
	};
	//远程获取要显示的JSON数据
	this.ajaxLoadData=function(type,url,param,timeout,callback){
		loadData=this.loadData;
		$.ajax({
			type: type,//传输方式，post或者是get
			url: url,//目标远程地址
			data: param,//要传的参数JSON格式
			dataType:"json",
			timeout:timeout,//超时时间设置（单位毫秒）
			error: function(XMLHttpRequest, textStatus, errorThrown){
				alert("对不起,数据装载失败!\n"+errorThrown);
			},
			success: function(msg,textStatus){
				loadData(msg);
				callback;
			}
		});
	};
	this.ajaxLoadData=function(type,url,param,timeout){
		loadData=this.loadData;
		$.ajax({
			type: type,//传输方式，post或者是get
			url: url,//目标远程地址
			data: param,//要传的参数JSON格式
			dataType:"json",
			timeout:timeout,//超时时间设置（单位毫秒）
			error: function(XMLHttpRequest, textStatus, errorThrown){
				alert("对不起,数据装载失败!\n"+errorThrown);
			},
			success: function(msg,textStatus){
				loadData(msg);
			}
		});
	};
}

//将此类的构造函数加入至JQUERY对象中
jQuery.extend({
	createJqVote: function(voteDiv,label,voteWidth,titleWidth,slipWidth,lineHeigth,units,havePercent) {
		return new JqVote(voteDiv,label,voteWidth,titleWidth,slipWidth,lineHeigth,units,havePercent);
  }
}); 