<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="<?php echo TEMPLATES_URL; ?>/css/css.css" />
	<title>微调研-<?php echo $output['dyinfo']['item_name'];?></title>
    <style>
    .l-wrap-load,.l-wrap-index{background:url(<?php echo TEMPLATES_URL; ?>/images/wdy/bg.jpg) no-repeat 0 0;}
	.l-wrap-qus{background:url(<?php echo TEMPLATES_URL; ?>/images/wdy/qus-tri.png) no-repeat 100% 100%,url(<?php echo TEMPLATES_URL; ?>/images/wdy/qus-bg_hei.jpg) no-repeat 0 0;}
    </style>
    <script type="text/javascript">
    	var $STCONFIG = {
			VERSION 	: '1.0',
			SHAREIT 	: {
				title : '分享标题', 
				con : '分享内容',
				link : document.URL, 
				img  : "图片地址"	
			}
		};
		if(/Android (\d+\.\d+)/.test(navigator.userAgent)){
			var version = parseFloat(RegExp.$1);
			if(version > 2.3){
				var phoneScale = parseInt(window.screen.width) / 640;
				document.write('<meta name="viewport" content="width=640, minimum-scale = '+ phoneScale +', maximum-scale = '+ phoneScale +', target-densitydpi=device-dpi">');
			}else{
				document.write('<meta name="viewport" content="width=640, target-densitydpi=device-dpi">');
			}
		}else{
			document.write('<meta name="viewport" content="width=640, user-scalable=no, target-densitydpi=device-dpi">');
		}
		if(navigator.userAgent.indexOf('MicroMessenger') >= 0){
			document.addEventListener('WeixinJSBridgeReady', function() {
			});
		}
		var TEMPLATESURL = "<?php echo TEMPLATES_URL;?>";
    </script>
</head>
<body>
	<section class="l-wrap l-wrap-load l-load-show J-load">
		<div class="l-load-box">
			<div class="l-load-img"></div>
			<div class="l-load-txt">&nbsp;抽奖载入中...</div>
		</div>
	</section>
	<section class="l-wrap l-wrap-index l-animation J-index">
		<div class="l-index-c">
        	<h1><?php echo $output['lot']['lot_title'];?></h1>
            <?php if($output['result']['is_lot']){?>            
            <p>
           		<?php $a = array('一','二','三','四','五','六','七','八','九','十');
				$k=$output['result']['prize']-1;
				$lot = unserialize($output['lot']['lot_info']);
				$goods = $lot[$k]['jp'];
				?>
            	恭喜您获得【<font color="#FF0000"><?php echo $a[$k];?>等奖</font>】，奖品为【<font color="#FF0000"><?php echo $goods;?></font>】一件
            </p>
            <?php }else{?>
            <p>
            	很遗憾您没有中奖，敬请期待我们下次活动吧！
            </p>
            <?php }?>
        </div>
	</section>
    <script src="<?php echo TEMPLATES_URL; ?>/js/wdy/sea.js?version=1.0"></script>
	<script src="<?php echo TEMPLATES_URL; ?>/js/wdy/modules/app/main.js?version=1.0"></script>
</body>
</html>