<?php
/**
 * 人工客服管理
 * */
defined('InByShopWWI') or exit('Access Invalid!');
define('MYSQL_RESULT_TYPE',1);
class serviceControl extends BaseHomeControl{
	var $wx_id;
	public function __construct(){
		parent::__construct();
		$this->wx_id = intval($_REQUEST['wx_id']);
		if($this->wx_id <= 0){
			showMessage('参数错误','','error');
		}
	}

	public function indexOp(){
		$model = Model();
		if(chksubmit()){
			$arr = array();
			$arr['value'] = intval($_POST['service_check']);
			$model->table('setting')->where(array('name'=>'service_check'))->update($arr);
		}
		$setting = $model->table('setting')->where(array('name'=>'service_check'))->find();
		$tsm = array();
		$tsm[$setting['name']] = $setting['value'];
		Tpl::output('setting',$tsm);
		Tpl::showpage('service_seting');
	}
}