	
define(function(require, exports, module){
	//引用相关模块
	var $ 		= require('lib/zepto/zepto'),
		  $ 		= require('lib/zepto/touch'),
		  iscroll = require('lib/iscroll/iscroll');
	//对外提供接口
	module.exports = {
		//初始化
		init : function () {
			if ($(".J-result-scroll").length>0){
				new iscroll('.J-result-scroll', { mouseWheel: true})
			}
		}
	}
});