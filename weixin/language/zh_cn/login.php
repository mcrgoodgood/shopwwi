<?php
defined('InByShopWWI') or exit('Access Invalid!');
/**
 * index
 */
$lang['login_index_title_01']					= 'ShopWWI电商门户2013'; 
$lang['login_index_title_02']					= '系统管理中心'; 
$lang['login_index_title_03']					= '资讯&nbsp;&nbsp;|&nbsp;&nbsp;购物&nbsp;&nbsp;|&nbsp;&nbsp;社交&nbsp;&nbsp;|&nbsp;&nbsp;分享&nbsp;&nbsp;|&nbsp;&nbsp;统计'; 
$lang['login_index_username_null']				= '用户名不能为空';
$lang['login_index_password_null']				= '密码不能为空';
$lang['login_index_checkcode_null']				= '验证码不能为空';
$lang['login_index_checkcode_wrong']			= '验证码输入错误，请重新输入';
$lang['login_index_not_admin']					= '您不是管理员，不能进入后台';
$lang['login_index_username_password_wrong']	= '帐号密码错误';
$lang['login_index_need_login']					= '您需要登录后才可以使用本功能';
$lang['login_index_username']					= '帐号';
$lang['login_index_password']					= '密码';
$lang['login_index_password_pattern']			= '密码不少于6个字符';
$lang['login_index_checkcode']					= '输入验证';
$lang['login_index_checkcode_pattern']			= '验证码为4个字符';
$lang['login_index_close_checkcode']			= '关闭';
$lang['login_index_change_checkcode']			= '看不清,点击更换验证码';
$lang['login_index_button_login']				= '登录';
$lang['login_index_shopwwi']						= '天津市网城创想科技有限责任公司';