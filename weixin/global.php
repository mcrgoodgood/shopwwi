<?php
/**
 * 入口文件
 *
 * 统一入口，进行初始化信息
 * 
 *
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com/
 * @link       http://www.shopwwi3.com/
 * @since      File available since Release v1.1
 */

error_reporting(E_ALL & ~E_NOTICE);

define('DS','/');
define('InByShopWWI',true);

define('StartTime',microtime(true));
define('TIMESTAMP',time());

define('BASE_CORE_PATH',BASE_ROOT_PATH.'/core');
define('BASE_DATA_PATH',BASE_PATH.'/data');
define('BASE_CACHE_PATH',BASE_DATA_PATH.'/cache');

define('DIR_SHOP','shop');
define('DIR_RESOURCE','data/resource');
define('DIR_UPLOAD','data/upload');
define('DIR_CACHE','data/cache');

define('ATTACH_PATH','');
define('ATTACH_COMMON','common');
define('ATTACH_AVATAR','avatar');
define('ATTACH_GOODS','goods');
define('ATTACH_REPLY','reply');
define('ATTACH_LINK','link');
define('ATTACH_LOGIN','login');
define('ATTACH_ARTICLE','article');
define('ATTACH_BRAND','brand');
define('ATTACH_ADV','adv');
define('ATTACH_WATERMARK','watermark');
define('ATTACH_SPEC','spec');
define('ATTACH_REC_POSITION','rec_position');
define('ATTACH_REFUND','refund');

if(!file_exists(BASE_PATH.'/install/lock')){
	@header("Location:install/index.php");
	exit;
}
