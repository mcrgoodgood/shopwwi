<?php defined('InByShopWWI') or exit('Access Invalid!');?>
<div class="main_hd">
  <h2><?php echo L('menu_click_reply_add'); ?></h2>
  <p class="extra_info"><a href="index.php?act=menu&op=click_reply_manage&wx_id=<?php echo intval($_GET['wx_id']); ?>">点击事件回复管理</a></p>
</div>
<form id="add_form" method="post" enctype="multipart/form-data">
<input type="hidden" name="form_submit" value="ok" />
<div class="main_bd">
	<ul>
		<li class="list_item">
	        <label class="label_box"><font style="color:red">*</font><?php echo L('menu_subject').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="cr_title" value="" class="label_input">
	          	<label for='cr_title' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><font style="color:red">*</font><?php echo L('menu_code').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<input type="text" name="cr_code" value="" class="label_input">
	          	<label for='cr_code' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><?php echo L('menu_intro').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <textarea name="cr_note" style="width:350px;height:100px"></textarea>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><font style="color:red">*</font><?php echo L('menu_type').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<select name="cr_type">
	          	<option value="1"><?php echo L('menu_pure_text'); ?></option>
	          	<option value="2"><?php echo L('menu_image_text'); ?></option>
	          	</select>
	          	<a href="javascript:;" id="add_group"><?php echo L('menu_add_one_group'); ?></a>
	          	<label for='cr_type' class='error msg_invalid' style='display:none;'></label>
	          </span>     
	        </div>
         </li>
         <li class="list_item text_reply">
	        <label class="label_box"><?php echo L('menu_reply').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <textarea name="reply_content" style="width:350px;height:180px"></textarea>  
	        </div>
         </li>
         <div id="textimg_container">
         <li class="list_item textimg_reply">
         	<label class="label_box"><?php echo L('menu_di'); ?>1<?php echo L('menu_zu').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <?php echo L('menu_title').L('nc_colon'); ?><input type="text" name="reply_title[]" class="label_input"/><br><br>
			  <?php echo L('menu_desc').L('nc_colon'); ?><textarea name="reply_desc[]" style="width:322px;height:100px"></textarea><br><br>
			  <?php echo L('menu_pic').L('nc_colon'); ?><input type="file" name="reply_file[]"/><br><br>
			  <?php echo L('menu_url').L('nc_colon'); ?><input type="text" name="reply_url[]" class="label_input"/>
	        </div>
         </li>
         </div>
         <li>
	        <div class="btn_bar">
	        <input type="submit" class="btn_input" value="<?php echo $lang['nc_save'];?>"></div>
	     </li>
    </ul>
</div>
</form>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script>
<script type="text/javascript">
$(function(){
	$('.textimg_reply').hide();
	$('#add_group').hide();
	$('select[name="cr_type"]').change(function(){
		if($(this).val() == 1){
			$('.text_reply').show();
			$('.textimg_reply').hide();
			$('#add_group').hide();
		}else{
			$('.text_reply').hide();
			$('.textimg_reply').show();
			$('#add_group').show();
		}
	});
	var num = 2;
	$('#add_group').click(function(){
		if(num <= 10){
			$('#textimg_container').append('<li class="list_item textimg_reply"><label class="label_box"><?php echo L('menu_di'); ?>'+num+'<?php echo L('menu_zu').L('nc_colon'); ?></label><div class="label_form"><?php echo L('menu_title').L('nc_colon'); ?><input type="text" name="reply_title[]" class="label_input"/><br><br><?php echo L('menu_desc').L('nc_colon'); ?><textarea name="reply_desc[]" style="width:322px;height:100px"></textarea><br><br><?php echo L('menu_pic').L('nc_colon'); ?><input type="file" name="reply_file[]"/><br><br><?php echo L('menu_url').L('nc_colon'); ?><input type="text" name="reply_url[]" class="label_input"/></div></li>');
			num++;
		}
	});
	$("#add_form").validate({
        rules: {
        	cr_title: {
				required:true
            },
            cr_type:{
            	required:true
            },
            cr_code:{
            	required:true
            }
        },
        messages:{
        	cr_title:{
        		required:'<?php echo L('menu_subject_must_write'); ?>'
            },
            cr_type:{
            	required:'<?php echo L('menu_type_must_choose'); ?>'
            },
            cr_code:{
            	required:'<?php echo L('menu_code_must_write'); ?>'
            }
        }
	});
})
</script>