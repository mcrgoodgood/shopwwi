<?php
/**
 * 默认展示页面（公共账号管理）
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
class indexControl extends Control{
	public function __construct(){
		$this->checkLogin();
		Tpl::setDir('home');
		Tpl::setLayout('index_layout');
		Language::read('wx');
		if(!C('site_status')) halt(C('closed_reason'));
	}
	/**
	 * 公共账号列表
	 * 
	 */
	public function indexOp(){
		$model = Model();
		//调取公共账号信息
		$where_condition = true;
		if($_SESSION['admin_is_super'] == 0){
			$admin_info = $model->table('admin')->where(array('admin_id'=>$_SESSION['admin_id']))->find();
			$where_condition = array('wx_id'=>array('in',$admin_info['admin_wx_id']));
		}
		$wx_list = $model->table('wxaccount')->where($where_condition)->select();
		Tpl::output('wx_list',$wx_list);
		Tpl::showpage('wx_list');
	}
	/**
	 * 添加新公共账号（只有超管有此权限）
	 */
	public function wx_addOp(){
		$model = Model();
		if(chksubmit()){
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam = array(
				array("input"=>trim($_POST['wx_name']),"require"=>"true","message"=>L('wx_account_name_must_write')),
				array("input"=>trim($_POST['wx_ori_id']),"require"=>"true","message"=>L('wx_ori_id_must_write')),
				array("input"=>trim($_POST['wx_account']),"require"=>"true","message"=>L('wx_account_must_write')),
				array("input"=>trim($_POST['wx_appid']),"require"=>"true","message"=>L('wx_appid_must_write')),
				array("input"=>trim($_POST['wx_appsecret']),"require"=>"true","message"=>L('wx_appsecret_must_write'))
			);
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage($error,'','error');
			}
			if(trim($_POST['shop_data_type']) == "2" && trim($_POST['store_id']) == ""){
				showMessage("平台商家微信公众账户请填写店铺ID",'','error');
			}
			//自动生成token
			$_POST['wx_token'] = strrev(md5(trim($_POST['wx_appid']).time()));
			unset($_POST['form_submit']);
			$res = $model->table('wxaccount')->insert($_POST);
			if($res){				
				$params = array();
				$params['wx_id'] = $res;//站点信息			
				$model->table('site')->insert($params);
				
				$title_arr = array(1=>'联系我们',2=>'关于我们',3=>'商城介绍');//系统文章
				foreach($title_arr as $key=>$val){
					$arr = array();
					$arr['title'] = $val;
					$arr['publish_time'] = time();
					$arr['type']  = $key;
					$arr['wx_id'] = $res;
					
					$model->table('basic')->insert($arr);
					unset($arr);
				}				
				
				showMessage(L('wx_account_add_succ'),'index.php','succ');
			}else{
				showMessage(L('wx_account_add_failed'),'','error');
			}
		}
		Tpl::showpage('wx_add');
	}
	/**
	 * 编辑公共账号
	 */
	public function wx_editOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		if($wx_id <= 0){
			showMessage(L('wrong_argument'),'','error');
		}
		if(chksubmit()){
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam = array(
				array("input"=>trim($_POST['wx_name']),"require"=>"true","message"=>L('wx_account_name_must_write')),
				array("input"=>trim($_POST['wx_ori_id']),"require"=>"true","message"=>L('wx_ori_id_must_write')),
				array("input"=>trim($_POST['wx_account']),"require"=>"true","message"=>L('wx_account_must_write')),
				array("input"=>trim($_POST['wx_appid']),"require"=>"true","message"=>L('wx_appid_must_write')),
				array("input"=>trim($_POST['wx_appsecret']),"require"=>"true","message"=>L('wx_appsecret_must_write'))
			);
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage($error,'','error');
			}
			if(trim($_POST['shop_data_type']) == "2" && trim($_POST['store_id']) == ""){
				showMessage("平台商家微信公众账户请填写店铺ID",'','error');
			}
			//自动生成token
			if (intval($_POST['remake_token']) == 1) {
				$_POST['wx_token'] = strrev(md5(trim($_POST['wx_appid']).time()));
			}
			unset($_POST['remake_token']);
			unset($_POST['form_submit']);
			$res = $model->table('wxaccount')->where(array('wx_id'=>$wx_id))->update($_POST);
			if($res){
				showMessage(L('wx_account_edit_succ'),'index.php','succ');
			}else{
				showMessage(L('wx_account_edit_failed'),'','error');
			}
		}
		$wx_info = $model->table('wxaccount')->where(array('wx_id'=>$wx_id))->find();
		Tpl::output('wx_info',$wx_info);
		Tpl::showpage('wx_edit');
	}
	/**
	 * 删除公共账号（只有超管有此权限）
	 */
	public function wx_delOp(){
		$model = Model();
		$wx_id = intval($_GET['wx_id']);
		if($wx_id <= 0){
			showMessage(L('wrong_argument'),'','error');
		}
		$res = $model->table('wxaccount')->where(array('wx_id'=>$wx_id))->delete();
		if($res){
			showMessage(L('wx_account_del_succ'),'index.php','succ');
		}else{
			showMessage(L('wx_account_del_failed'),'','error');
		}
	}
}