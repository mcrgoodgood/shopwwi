<?php defined('InByShopWWI') or exit('Access Invalid!');?>

<div class="main_hd">
  <h2>自定义菜单按钮推荐分类管理</h2>
  <p class="extra_info"><a href="index.php?act=store&op=gc_manage&wx_id=<?php echo intval($_GET['wx_id']); ?>">文本推荐分类管理</a></p>
</div>
<form id="add_form" method="post" enctype="multipart/form-data">
<input type="hidden" name="form_submit" value="ok" />
<input type="hidden" name="type" value="<?php echo $output['type']; ?>" />
<input type="hidden" name="store_id" value="<?php echo $output['store_id']; ?>" />
<input type="hidden" name="exctype" value="<?php if ($output['choose_gc'] != "") {?>update<?php }else{ ?>insert<?php } ?>" />
<div class="main_bd">
<label class="label_box"><font style="color:red"></font>已选择分类：</label>
  <div class="label_form"> 
	<span><b>
	<?php if (!empty($output['choose_gc'])) { ?>
	<?php foreach ($output['choose_gc'] as $val) { ?>
	<?php echo $val['gc_name']."&nbsp;&nbsp;"; ?>
	<?php }} ?>
	</b></span>     
  </div><br>
  <div class="tip">注：请勾选需要推荐的商品分类，点击“保存”按钮后将生成一组自定义子菜单，<b>最多勾选5个商品分类</b>。</div>
  <div class="table_msg"  style="width:500px">
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell" style="width:10%"><input type="checkbox" id="checkAll" /></th>
          <th class="table_cell" style="width:90%">商品分类名称</th>
        </tr>
      </thead>
      <tbody class="nc-tbody" id="gc_body"></tbody>
    </table>
  </div>
  <div class="btn_bar">
	<input class="btn_input" type="submit" value="保存">
  </div>
  <div class="loading">商品分类加载中，请稍候...</div>
</div>
</form>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script>
<script type="text/javascript">
$(function(){
	$('.table_msg').hide();
	$('.btn_bar').hide();
	//ajax加载商品分类
	$.getJSON('index.php?act=store&op=ajax_getgc&wx_id=<?php echo intval($_GET['wx_id']); ?>', function(result){
    	if(result.done){
        	$.each(result.gc_list, function(key,val){
        		$('#gc_body').append("<tr id='tr_"+ val.gc_id +"' ><td><input type='checkbox' name='recgc[]' value='"+ val.gc_id +"' /><input type='hidden' name='recgn_"+ val.gc_id +"' value='"+ val.gc_oriname +"' /></td><td style='text-align:left<?php if ($output['type'] == 1) { ?>;cursor:pointer<?php } ?>' gcid='"+ val.gc_id +"' pid='"+ val.gc_parent_id +"' class='clickable' dg='1' state='close' >"+ val.gc_name +"</td></tr>");
            });
            $('input[name="store_id"]').val(result.store_id);
        	$('.table_msg').show();
        	$('.btn_bar').show();
        	$('.loading').hide();
    	} else {
        	$('.loading').html(result.msg);
    	}
    });
	$("#add_form").validate({
        rules: {
        	reply_title: {
				required:true
            }
        },
        messages:{
        	reply_title:{
        		required:'请填写标题'
            }
        }
	});
	$('#checkAll').click(function(){
		if($(this).attr('checked') == 'checked'){
			$('input[name="recgc[]"]').attr('checked','checked');
		}else{
			$('input[name="recgc[]"]').removeAttr('checked');
		}
	});
	$('input[name="reply_type"]').change(function(){
		if ($(this).val() == "click_reply"){
			$('#code').show();
		} else {
			$('#code').hide();
		}
	});
	<?php if ($output['type'] == 1) { ?>
	var gcload_lock = false;
	$('.clickable').live('click',function(){
		var gcid = $(this).attr("gcid");
		var gc_parent_id = $(this).attr("pid");
		var degree = $(this).attr("dg");
		var state = $(this).attr("state");
		if (state == "close") {
			if (!gcload_lock) {
				gcload_lock = true;
				$('#tr_'+gcid).after("<tr class='loading_tr' ><td></td><td style='text-align:center' >正在加载子分类，请稍候...</td></tr>");
				$('tr[pid="'+gcid+'"]').remove();
				$('tr[gpid="'+gcid+'"]').remove();
				//ajax加载平台子分类
				$.getJSON('index.php?act=store&op=ajax_getmallgc&wx_id=<?php echo intval($_GET['wx_id']); ?>&gc_id='+gcid+'&dg='+degree, function(result){
			    	if(result.done){
			        	$.each(result.gc_list, function(key,val){
			        		$('#tr_'+gcid).after("<tr id='tr_"+ val.gc_id +"' pid='"+gcid+"' gpid='"+gc_parent_id+"'><td><input type='checkbox' name='recgc[]' value='"+ val.gc_id +"' /><input type='hidden' name='recgn_"+ val.gc_id +"' value='"+ val.gc_oriname +"' /></td><td style='text-align:left<?php if ($output['type'] == 1) { ?>;cursor:pointer<?php } ?>' gcid='"+ val.gc_id +"' pid='"+ gcid +"' class='clickable' dg='"+ val.degree +"' state='close' >"+ val.gc_name +"</td></tr>");
			            });
			        	$('td[gcid="'+gcid+'"]').attr("state","open");
			    	} else {
				    	alert(result.msg);
			    	}
			    	$('.loading_tr').hide();
			    	gcload_lock = false;
			    });
			}
		} else {
			$('tr[pid="'+gcid+'"]').remove();
			$('tr[gpid="'+gcid+'"]').remove();
			$(this).attr("state","close");
		}
	});
	<?php } ?>
})
</script>