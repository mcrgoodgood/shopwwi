<?php
defined('InByShopWWI') or exit('Access Invalid!');
$config = array();
$config['shop_site_url'] 	 = 'http://www.shopwwi.net/weixin';
$config['version'] 		= '2013720';
$config['setup_date'] 	= '2017-12-07 02:14:46';
$config['gip'] 			= 0;
$config['dbdriver'] 	= 'mysqli';
$config['tablepre']		= 'shopwwi_';
$config['db'][1]['dbhost']  	= 'localhost';
$config['db'][1]['dbport']		= '3306';
$config['db'][1]['dbuser']  	= 'root';
$config['db'][1]['dbpwd'] 	 	= 'root';
$config['db'][1]['dbname']  	= 'shopwwi';
$config['db'][1]['dbcharset']   = 'UTF-8';
$config['db']['slave'] 		= array();
$config['session_expire'] 	= 3600;
$config['lang_type'] 		= 'zh_cn';
$config['cookie_pre'] 		= '590A_';
$config['tpl_name'] 		= 'default';
$config['thumb']['save_type'] 		= 1;
$config['thumb']['cut_type'] = 'gd';
$config['thumb']['impath'] = '';
$config['cache']['type'] 			= 'file';
$config['memcache']['prefix']      	= '';
$config['memcache'][1]['port']     	= 11211;
$config['memcache'][1]['host']     	= '127.0.0.1';
$config['memcache'][1]['pconnect'] 	= 0;
$config['debug'] 			        = false;
$config['syn_site'][] = 'shop';
$config['app_key']['shop'] = '493c5ff069641a914537745fd5a96800';
$config['wap_site_url'] = '';
$config['mall_api_url'] = '';