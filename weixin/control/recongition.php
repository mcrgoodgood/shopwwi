<?php
/**
 * 渠道管理
 * */

defined('InByShopWWI') or exit('Access Invalid!');
class recongitionControl extends BaseHomeControl{
	public function __construct(){
		parent::__construct();
		$this->wx_id = intval($_REQUEST['wx_id']);
		if($this->wx_id <= 0){
			showMessage('参数错误','','error');
		}
	}

	/**
	 * 渠道列表
	 * */
	public function indexOp(){
		$model = Model();
		$list = $model->table('channel')->select();
		Tpl::output('list',$list);
		Tpl::showpage('channel_list');
	}

	/**
	 * 添加渠道信息
	 * */
	public function addOp(){
		if(chksubmit()){
			$obj_validate = new Validate();
			$obj_validate->validateparam = array(
					array("input"=>$_POST["chn_name"],		"require"=>"true", "message"=>'渠道名称不能为空'),
			);
			$error = $obj_validate->validate();
			if ($error != ''){
				showDialog($error);
			}else {
				$model = Model();
				$insert_array = array();
				$insert_array['channel_name'] = trim($_POST['chn_name']);
				$insert_array['wx_id'] = intval($_POST['wx_id']);
				$insert_array['keyword'] = trim($_POST['chn_key']);
				$insert_array['insert_time'] = time();
				$res = $model->table('channel')->insert($insert_array);
				if($res){
					showMessage('添加成功','?act=recongition&op=index&wx_id='.$this->wx_id,'succ');
				}else{
					showMessage('添加失败','?act=recongition&op=index&wx_id='.$this->wx_id,'error');
				}
			}
		}
	    Tpl::showpage('channel_add');
	}

	/**
	 * 删除渠道信息
	 * */
	public function delOp(){
	    $chn_id = intval($_GET['chn_id']);
	    if($chn_id <= 0){
	    	showMessage('参数错误','?act=recongition&op=index&wx_id='.$this->wx_id,'error');
	    }
	    $model = Model();
	    $res = $model->table('channel')->where(array('channel_id'=>$chn_id))->delete();
	    if($res){
	    	showMessage('删除成功','?act=recongition&op=index&wx_id='.$this->wx_id,'succ');
	    }else{
	    	showMessage('删除失败','?act=recongition&op=index&wx_id='.$this->wx_id,'error');
	    }
	}

	/**
	 * 获取渠道二维码
	 * */
	public function get_queenOp(){
		$chn_id = intval($_GET['chn_id']);
		if($chn_id <= 0){
			showMessage('参数错误','?act=recongition&op=index&wx_id='.$this->wx_id,'error');
		}
		$model = Model();
		$wxinfo = $model->table('wxaccount')->where(array('wx_id'=>$this->wx_id))->find();
		$channl = $model->table('channel')->where(array('channel_id'=>$chn_id))->find();
		$count_chn = $model->table('channel')->where(array('wx_id'=>$this->wx_id))->count();
		$url = 'https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token='.$wxinfo['wx_accesstoken'];
		$sub_data = array();
		$sub_data['action_name'] = 'QR_LIMIT_SCENE';
		$sub_data['action_info'] = array('scene'=>array(
				'scene_id'=>$count_chn+1,
		));
		if($channl['keyword'] != ''){
		    $sub_data['action_info']['scene']['keyword'] = $channl['keyword'];
		}
		$str_data = json_encode($sub_data);
		$result = curl_post($url,$str_data);
		if(!$result['nc_status'] || $result['data']['errcode'] == '40001' || $result['data']['errcode'] == '42001' || $result['data']['errcode'] == '42002' || $result['data']['errcode'] == '42003'){
		    $url2 = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='.$wxinfo['wx_appid'].'&secret='.$wxinfo['wx_appsecret'];
		    $res = curl_get($url2);
		    $json=json_decode($res);
		    $wxinfo['wx_accesstoken'] = $json->access_token;
		    $model->table('wxaccount')->where(array('wx_id'=>$this->wx_id))->update(array('wx_accesstoken'=>$wxinfo['wx_accesstoken']));
		    $url = 'https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token='.$wxinfo['wx_accesstoken'];
		    $result = curl_post($url,$str_data);
		    echo 'kk<br>';
		}
		if($result['nc_status']){
		    $model->table('channel')->where(array('channel_id'=>$chn_id))->update(array('channel_images'=>$result['data']['ticket']));
		}
		echo '<script>location.href="?act=recongition&op=index&wx_id='.$this->wx_id.'"</script>';
	}

	/**
	 * ajax
	 * */
	public function ajaxOp(){
	    $branch = trim($_REQUEST['branch']);
	    switch($branch){
	        case 'chn_change':
	        	$model = Model();
	        	$chn_id = intval($_GET['chn_id']);
	        	$update = intval($_GET['state']);
	        	$model->table('channel')->where(array('channel_id'=>$chn_id))->update(array('status'=>$update));
	        	echo '<script>location.href="?act=recongition&op=index&wx_id='.$this->wx_id.'"</script>';
	        	break;
	    }
	}
}