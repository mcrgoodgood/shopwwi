<div class="main_hd">
  <h2><?php echo $lang['nc_weixin_wall_activity'];?></h2>
</div>
<div class="main_bd">
  <form method="post" id="apply_form" action="?act=activity&op=addActivity&wx_id=<?php echo intval($_GET['wx_id']);?>">
 	 <input type="hidden" name="form_submit" value="ok" />
    <ul>
      <li class="list_item">
        <label class="label_box"><?php echo $lang['nc_weixin_wall_activity_name'];?></label>
        <div class="label_form"> 
          <span>
          	<input type="text" class="label_input" name="activity_name" id="activity_name">
            <label for='activity_name' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>
      <li class="list_item">
        <label class="label_box"><?php echo $lang['nc_weixin_wall_activity_content'];?></label>
        <div class="label_form">
          <?php showEditor('activity_content','','550px','300px','','true',false);?>
        </div>
      </li>
      <li>
        <div class="btn_bar">
        <input type="submit" class="btn_input" value="<?php echo $lang['nc_save'];?>"></div>
      </li>
    </ul>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script> 
<script type="text/javascript">
$(function(){	
	$("#apply_form").validate({//验证表单
        rules: {
        	activity_name: {
				required:true
            }
        },
        messages:{
        	activity_name:{
        		required:'<?php echo $lang['nc_weixin_wall_activity_name_is_not_null'];?>'
            }
        }
	});
});
</script> 
