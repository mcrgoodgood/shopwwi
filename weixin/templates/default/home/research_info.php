<style>
.main_bd{ padding:20px 10px;}
.table_msg table tbody tr td.tl,.table_msg thead th.tl{ text-align:left;}
.w80 { width:80px;}
</style>
<div class="main_hd">
  <h2><?php echo '微调研活动';?></h2>
  <p class="extra_info"> <a href="?act=research&op=list&wx_id=<?php echo intval($_GET['wx_id']);?>"><?php echo '返回列表';?></a> </p>
</div>
<div class="main_bd">
  <div class="table_msg">
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell tl time asc"><?php echo '问题';?></th>
          <th class="table_cell tl goods"><?php echo '答案';?></th>
          <th class="table_cell goods w80"><?php echo '参与人数';?></th>
        </tr>
      </thead>
      <tbody class="nc-tbody">
        <?php if(!empty($output['list'])){?>
        <?php foreach($output['list'] as $key=>$val){
				$i=1;
				$anwser = unserialize($val['qanwser']);
				$acount = unserialize($val['acount']);
				foreach($anwser as $k=>$v){					
					if($i == 1){
			?>
        <tr>
          <td class="tl" rowspan="<?php echo count($anwser);?>"><label title="<?php echo $val['qtitle'];?>"><?php if(mb_strlen($val['qtitle'],'utf-8')>=12){ echo mb_substr($val['qtitle'],0,12,'utf-8').'...';}else{ echo $val['qtitle'];}?></label></td>
          <td class="tl"><?php echo $v;?></td>
          <td><?php echo $acount[$k];?></td>
        </tr>
        <?php }else{?>
        <tr>
         <td class="tl"><?php echo $v;?></td>
          <td><?php echo $acount[$k];?></td>
        </tr>
        <?php }?>
        <?php $i++; }?>
        <?php }?>
        <?php }else{?>
        <tr>
          <td colspan="20" valign="middle" ><div class="norecord"><span><?php echo $lang['nc_record'];?></span></div></td>
        </tr>
        <?php }?>
      </tbody>
    </table>
  </div>
  <div class="pagination"> <?php echo $output['page'];?> </div>
</div>