<?php defined('InByShopWWI') or exit('Access Invalid!');?>

<div class="main_hd">
  <h2><?php echo L('fans_group_manage'); ?></h2>
  <p class="extra_info"><a href="index.php?act=fans&op=fans_list&wx_id=<?php echo intval($_GET['wx_id']); ?>"><?php echo L('fans_fans_manage'); ?></a></p>
</div>
<div class="main_bd">
  <div class="table_msg">
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell" ><?php echo L('fans_group_name'); ?></th>
          <th class="table_cell" style="width:20%"><?php echo L('fans_num'); ?></th>
        </tr>
      </thead>
      <tbody class="nc-tbody">
        <?php if(is_array($output['group_list']) && !empty($output['group_list'])){?>
        <?php foreach($output['group_list'] as $key=>$val){?>
        <tr height="40px">
          <td><?php if($val['id']==0 || $val['id']==1 || $val['id']==2){ ?>
            <?php echo $val['name']; ?>
            <?php }else{ ?>
            <input class="label_input_zt02" type="text" value="<?php echo $val['name']; ?>" group_id="<?php echo $val['id']; ?>"/>
            <a class="save_edit" group_id="<?php echo $val['id']; ?>" style="cursor:pointer"><?php echo L('fans_change'); ?></a>
            <?php } ?></td>
          <td class="last"><?php echo $val['count']; ?></td>
        </tr>
        <?php }?>
        <?php }else { ?>
        <tr>
          <td colspan="15" style="text-align:center"><?php echo L('fans_get_group_info_failed'); ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
  <div class="zt" style="margin-top:30px;"> <span><?php echo L('fans_group_add'); ?>：</span>
    <input name="new_group" class="label_input_zt" type="text" />
    <input type="button" class="newadd-button" id="group_add" />
  </div>
</div>
<script type="text/javascript">
$(function(){
	$('#group_add').click(function(){
		$(this).attr("disabled",true).val("<?php echo L('fans_saving'); ?>");
		$.getJSON('index.php?act=fans&op=group_add&wx_id=<?php echo intval($_GET['wx_id']); ?>',{
				'new_group':$('input[name="new_group"]').val()
			},function(result){
	        if(result.done){
		        alert('<?php echo L('fans_group_add_succ'); ?>');
	            window.location.href = 'index.php?act=fans&op=group_list&wx_id=<?php echo intval($_GET['wx_id']); ?>';
	        }else{
	        	$('#group_add').attr("disabled",false).val("<?php echo L('fans_group_add'); ?>");
	            alert('<?php echo L('fans_group_add_failed'); ?>('+result.msg+')');
	        }
	    });
	});
	$('.save_edit').click(function(){
		var group_id = $(this).attr("group_id");
		var group_name = $('input[type="text"][group_id="'+group_id+'"]').val();
		$.getJSON('index.php?act=fans&op=group_edit&wx_id=<?php echo intval($_GET['wx_id']); ?>',{
			'group_name':group_name,
			'group_id':group_id
		},function(result){
	        if(result.done){
		        alert('<?php echo L('fans_group_name_edit_succ'); ?>');
	            window.location.href = 'index.php?act=fans&op=group_list&wx_id=<?php echo intval($_GET['wx_id']); ?>';
	        }else{
	            alert('<?php echo L('fans_group_name_edit_failed'); ?>('+result.msg+')');
	        }
    	});
	});
})
</script>