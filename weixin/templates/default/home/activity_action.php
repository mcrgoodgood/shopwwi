<style>
.main_bd{ padding:20px 10px;}
.titles{ padding-bottom:10px; color:#888;}
.titles ul{ list-style:none; border-bottom:1px #CCC solid; }
.titles ul li { list-style:none; display:inline-block; margin-bottom:-1px; line-height:25px; margin-left:5px;}
.titles a{ color:#888; text-decoration:none; display:block;  padding:5px 10px; line-height:25px; background:#EEE; border:1px #EEE solid;  border-bottom:1px #CCC solid; }
.titles ul li a.curr{ color:#000; background:#CCC;}
</style>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/fushionCharts/JSClass/FusionCharts.js"></script>
<div class="main_hd">
  <h2><?php echo '粉丝行为分析';?></h2>
</div>
<div class="main_bd">
  <div class="titles">
  	<ul>
    	<li><a class="curr" href="?act=fans_activity&op=index&wx_id=<?php echo intval($_GET['wx_id']);?>">7日行为统计分析</a></li>
        <li><a href="?act=fans_activity&op=trend&wx_id=<?php echo intval($_GET['wx_id']);?>">趋势对比</a></li>
    </ul>
  </div>
  <div>
  <div id="chartdiv1" align="center"></div>
  </div>
  <div class="table_msg">
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell time asc"><?php echo '名称';?></th>
          <th class="table_cell goods"><?php echo '次数';?></th>
         <!-- <th class="table_cell goods"><?php echo '详情';?></th>-->
        </tr>
      </thead>
      <tbody class="nc-tbody">
      <?php $str = '';?>
        <?php if(!empty($output['list'])){?>
        <?php foreach($output['list'] as $key=>$val){
			$str .= '<set label="'.$val['act_content'].'" value="'.$val['num'].'"/>';
			?>
        <tr>
          <td><?php echo $val['act_content'];?></td>
          <td><?php echo $val['num'];?></td>
          <!--<td><a href="javascript:void(0);" onClick="chkInfo(<?php echo $val['act_content'];?>)">详情</a></td>-->
        </tr>
        <?php }?>
        <?php }else{?>
        <tr>
          <td colspan="20" valign="middle" ><div class="norecord"><span><?php echo $lang['nc_record'];?></span></div></td>
        </tr>
        <?php }?>
      </tbody>
    </table>
  </div>
  <div class="pagination"> <?php echo $output['page'];?> </div>
</div>
<script type="text/javascript">
	var chart = new FusionCharts("<?php echo RESOURCE_SITE_URL;?>/js/fushionCharts/Charts/Pie3D.swf", "ChartId", "600", "500", "0", "1");
	chart.setDataXML('<chart borderThickness="0" caption="7日内粉丝行为分析" baseFontColor="666666" baseFont="宋体" baseFontSize="14" bgColor="FFFFFF" bgAlpha="0" showBorder="0" bgAngle="360" pieYScale="90"  pieSliceDepth="5" smartLineColor="666666"><?php echo $str;?></chart>');
	chart.render("chartdiv1");
</script>
<script>
function chkInfo(con){}
</script>
