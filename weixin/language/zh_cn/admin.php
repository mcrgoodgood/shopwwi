<?php
defined('InByShopWWI') or exit('Access Invalid!');

$lang['wx_admin_admin_account_must_write'] = '管理员账号必须填写';
$lang['wx_admin_admin_password_must_write'] = '管理员密码必须填写';
$lang['wx_admin_admin_truename_must_write'] = '管理员真实姓名必须填写';
$lang['wx_admin_admin_account_add_succ'] = '管理员账号添加成功！';
$lang['wx_admin_admin_account_add_failed'] = '管理员账号添加失败';
$lang['wx_admin_admin_account_edit_succ'] = '管理员账号编辑成功！';
$lang['wx_admin_admin_account_edit_failed'] = '管理员账号编辑失败';
$lang['wx_admin_admin_account_del_succ'] = '管理员删除成功！';
$lang['wx_admin_admin_account_del_failed'] = '管理员删除失败';
$lang['wx_admin_ori_pwd_must_write'] = '原密码必须填写';
$lang['wx_admin_new_pwd_must_write'] = '新密码必须填写';
$lang['wx_admin_renew_pwd_must_write'] = '重复新密码必须填写';
$lang['wx_admin_ori_pwd_not_correct'] = '原密码输入不正确';
$lang['wx_admin_pwd_not_match'] = '两次密码输入不一致，请重新输入';
$lang['wx_admin_password_edit_succ'] = '密码修改成功！';
$lang['wx_admin_password_edit_failed'] = '密码修改失败';

$lang['wx_admin_admin_account_list'] = '管理员账号列表';
$lang['wx_admin_admin_account'] = '管理员账号';
$lang['wx_admin_admin_truename'] = '管理员姓名';
$lang['wx_admin_last_login_time'] = '最后登录时间';
$lang['wx_admin_never_login'] = '从未登陆';
$lang['wx_admin_password_edit'] = '修改密码';
$lang['wx_admin_no_record'] = '暂无相关记录';
$lang['wx_admin_admin_account_del_confirm'] = '确认删除该管理员吗？';
$lang['wx_admin_add_admin'] = '添加管理员';
$lang['wx_admin_edit_admin'] = '编辑管理员';
$lang['wx_admin_account'] = '账号';
$lang['wx_admin_password'] = '密码';
$lang['wx_admin_truename'] = '姓名';
$lang['wx_admin_manage'] = '管理';