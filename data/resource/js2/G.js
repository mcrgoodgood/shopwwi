define('G', [IsMobile ? 'zepto' : 'jquery', IsMobile ? 'layer.mobile' : 'layer'], function($, layer) {
    var G = {
        load: function() {
            return IsMobile ? layer.open({type: 2}) : layer.load(1);
        },
        close: function(index) {
            return layer.close(index);
        },
        open: function(config) {
            if (IsMobile) {
                config = $.extend(true, {}, {
                    anim: 'up',
                    style: 'position:fixed; bottom:0; left:0; width: 100%; height: 100%; border:none; overflow: auto;'
                }, config)
                if (config.title && typeof config.title == 'string') { // 移动头部
                    config.content = '' +
                        '<header id="header" class="nctouch-product-header fixed">' +
                            '<div class="header-wrap">' +
                            '<div class="header-l"> <a href="javascript:;" onclick="G.close($(this).closest(\'.layui-m-layer\').attr(\'index\'))"> <i class="back"></i> </a> </div>' +
                            '<div class="header-inp1">' + config.title + '</div>' +
                        '</header>' + config.content;
                    delete config.title;
                }
            }
            return layer.open(config)
        }
    };

    return window.G = G;
});