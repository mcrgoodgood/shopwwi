<style>
.main_bd{ padding:20px 10px;}
.w40{ width:40px !important;}
.w200{ width:200px !important;}
.w80{ width:80px !important;}
</style>
<div class="main_hd">
  <h2><?php echo '微调研活动-抽奖管理';?></h2>
  <p class="extra_info"> <a href="?act=research&op=list&wx_id=<?php echo intval($_GET['wx_id']);?>"><?php echo '返回列表';?></a> </p>
</div>
<div class="main_bd">
  <div style="padding-bottom:10px;">
  	<b>活动名称：</b><font style="color:#F00"><?php echo $output['lot']['lot_title'];?></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <b>活动关键词：</b><font style="color:#F00"><?php echo $output['lot']['lot_keyword'];?></font>
    <br />
    <b>本次活动奖品总数：</b><font style="color:#F00"><?php echo $output['lot_count'];?></font>&nbsp;个 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <b>中奖人数：</b><font style="color:#F00"><?php echo $output['islot'];?></font>&nbsp;人 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <b>已发奖品：</b><font style="color:#F00"><?php echo $output['send_count'];?></font>&nbsp;件
  </div>
  <div class="table_msg">
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell w40">序号</th>
          <th class="table_cell w200"><?php echo '抽奖人';?></th>
          <th class="table_cell goods"><?php echo '抽奖时间';?></th>
          <th class="table_cell goods"><?php echo '中奖等级';?></th>
          <th class="table_cell w80"><?php echo '发奖状态';?></th>
          <th class="table_cell w80">发奖时间</th>
          <th class="table_cell"><?php echo $lang['nc_handle'];?></th>
        </tr>
      </thead>
      <tbody class="nc-tbody">
        <?php if(!empty($output['list'])){?>
        <?php foreach($output['list'] as $key=>$val){?>
        <tr>
          <td><?php echo $key;?></td>
          <td><?php if(mb_strlen($val['wxchat_id'],'utf-8')>=50){ echo mb_substr($val['wxchat_id'],0,50,'utf-8').'...';}else{ echo $val['wxchat_id'];}?></td>
          <td><?php echo date("Y-m-d H:i:s",$val['insert_time']);?></td>
          <td><?php echo $val['prize'];?></td>
          <td><?php echo ($val['send_stat'])>0?'已发':'未发';?></td>
          <td><?php echo (strstr($val['send_time'])!='' && $val['send_time'] >0)?data('Y-m-d H:i:s',$val['send_time']):'';?></td>
          <td class="last">
          <?php if($val['send_stat'] == 0){?>
          <a href="?act=research&op=lot_send&record_id=<?php echo $val['record_id'];?>&res_id=<?php echo $_GET['res_id'];?>&wx_id=<?php echo intval($_GET['wx_id']);?>"><?php echo '发奖';?></a> <span>|</span>
          <?php }else{?>
           <a href="?act=research&op=chanle_lot&record_id=<?php echo $val['record_id'];?>&res_id=<?php echo $_GET['res_id'];?>&wx_id=<?php echo intval($_GET['wx_id']);?>"><?php echo '取消发奖';?></a> <span>|</span> 
           <?php }?>
           <a href="javascript:void(0);" onClick="deleteitem(<?php echo $val['record_id']?>)"><?php echo '删除';?></a></td>
        </tr>
        <?php }?>
        <?php }else{?>
        <tr>
          <td colspan="20" valign="middle" ><div class="norecord"><span><?php echo $lang['nc_record'];?></span></div></td>
        </tr>
        <?php }?>
      </tbody>
    </table>
  </div>
  <div class="pagination"> <?php echo $output['page'];?> </div>
</div>
<script type="text/javascript">
	function deleteitem(activity_id){
		if(confirm('<?php echo '确认要删除吗？'?>')){
			location.href = "?act=research&op=del_lot&res_id=<?php echo $_GET['res_id'];?>&wx_id=<?php echo intval($_GET['wx_id']);?>&record_id="+activity_id;
		}
	}
</script> 
