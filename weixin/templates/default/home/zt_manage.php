<?php defined('InByShopWWI') or exit('Access Invalid!');?>

<div class="main_hd">
  <h2>专题推荐管理</h2>
  <p class="extra_info"><a href="index.php?act=store&op=reczt_add&wx_id=<?php echo intval($_GET['wx_id']); ?>">添加专题推荐</a></p>
</div>
<div class="main_bd">
  <div class="zt"> <span>主题：</span>
    <input name="s_title" type="text" class="label_input_zt" value="<?php echo trim($_GET['s_title']); ?>"/>
    <input type="button" class="search-button" id="search" />
    <span style="margin-left:10px">
	<input type="radio" name="reply_type" id="reply_type_click" value="click_reply" <?php if (trim($_GET['reply_type']) == "click_reply") { ?>checked<?php } ?> ><label for="reply_type_click" style="cursor:pointer"> 点击回复素材</label>
	<input type="radio" name="reply_type" id="reply_type_auto" value="auto_reply" <?php if (trim($_GET['reply_type']) == "auto_reply") { ?>checked<?php } ?> ><label for="reply_type_auto" style="cursor:pointer"> 自动回复素材</label>
	</span>
  </div>
  <div class="table_msg">
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell" style="width:35%">主题</th>
          <th class="table_cell" style="width:10%">类型</th>
          <th class="table_cell" style="width:20%">添加时间</th>
          <th class="table_cell" style="width:15%">操作</th>
        </tr>
      </thead>
      <tbody class="nc-tbody">
        <?php if(!empty($output['reply_list'])){?>
        <?php foreach($output['reply_list'] as $key=>$val){?>
        <?php if (trim($_GET['reply_type']) == "click_reply") { ?>
        <tr>
          <td><?php echo $val['cr_title']; ?></td>
          <td>点击回复</td>
          <td><?php echo date('Y-m-d H:i:s',$val['cr_addtime']); ?></td>
          <td class="last"><a href="index.php?act=menu&op=click_reply_edit&cr_id=<?php echo $val['cr_id']; ?>&wx_id=<?php echo intval($_GET['wx_id']); ?>"><?php echo L('nc_edit'); ?></a> <span>|</span> <a href="javascript:if(confirm('您确定要删除该图文商品推荐吗？'))window.location.href='index.php?act=menu&op=click_reply_del&cr_id=<?php echo $val['cr_id']; ?>&wx_id=<?php echo intval($_GET['wx_id']); ?>&fromg=1';"><?php echo L('nc_delete'); ?></a></td>
        </tr>
        <?php } else { ?>
        <tr>
          <td><?php echo $val['reply_title']; ?></td>
          <td>自动回复</td>
          <td><?php echo date('Y-m-d H:i:s',$val['reply_addtime']); ?></td>
          <td class="last"><a href="index.php?act=autoreply&op=reply_edit&reply_id=<?php echo $val['reply_id']; ?>&wx_id=<?php echo intval($_GET['wx_id']); ?>"><?php echo L('nc_edit'); ?></a> <span>|</span> <a href="javascript:if(confirm('您确定要删除该图文商品推荐吗？'))window.location.href='index.php?act=autoreply&op=reply_del&reply_id=<?php echo $val['reply_id']; ?>&wx_id=<?php echo intval($_GET['wx_id']); ?>&fromg=1';"><?php echo L('nc_delete'); ?></a></td>
        </tr>
        <?php } ?>
        <?php }?>
        <?php }else { ?>
        <tr>
          <td colspan="15" style="text-align:center">暂无相关记录</td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
  <div class="pagination"> <?php echo $output['show_page'];?> </div>
</div>
<script type="text/javascript">
$(function(){
	$('#search').click(function(){
		var s_title = $('input[type="text"][name="s_title"]').val();
		var r_type = $('input[name="reply_type"]:checked').val();
		window.location.href = 'index.php?act=store&op=zt_manage&wx_id=<?php echo intval($_GET['wx_id']); ?>&s_title='+s_title+'&reply_type='+r_type;
	});
	$('input[name="reply_type"]').change(function(){
		var r_type = $('input[name="reply_type"]:checked').val();
		window.location.href = 'index.php?act=store&op=zt_manage&wx_id=<?php echo intval($_GET['wx_id']); ?>&reply_type='+r_type;
	});
})
</script>