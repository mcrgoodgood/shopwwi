<?php
/**
 * 管理员账号管理
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
class adminControl extends Control{
	public function __construct(){
		$this->checkLogin();
		Tpl::setDir('home');
		Tpl::setLayout('index_layout');
		Language::read('common,admin');
		if(!C('site_status')) halt(C('closed_reason'));
	}
	/**
	 * 管理员列表
	 */
	public function indexOp(){
		$model = Model();
		$admin_list = $model->table('admin')->where(true)->select();
		Tpl::output('admin_list',$admin_list);
		Tpl::showpage('admin_list');
	}
	/**
	 * 新增管理员
	 */
	public function admin_addOp(){
		$model = Model();
		if($_SESSION['admin_is_super'] != 1){
			showMessage("您无权限新增管理员账号",'','error');
		}
		if(chksubmit()){
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam = array(
				array("input"=>trim($_POST['admin_name']),"require"=>"true","message"=>L('wx_admin_admin_account_must_write')),
				array("input"=>trim($_POST['admin_password']),"require"=>"true","message"=>L('wx_admin_admin_password_must_write')),
				array("input"=>trim($_POST['admin_truename']),"require"=>"true","message"=>L('wx_admin_admin_truename_must_write'))
			);
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage($error,'','error');
			}
			$insert_array = array();
			$insert_array['admin_name'] = trim($_POST['admin_name']);
			$insert_array['admin_truename'] = trim($_POST['admin_truename']);
			$insert_array['admin_password'] = md5(trim($_POST['admin_password']));
			if(!empty($_POST['choose_wx'])){
				foreach ($_POST['choose_wx'] as $val){
					$insert_array['admin_wx_id'] .= $val.',';
				}
			}
			$insert_array['admin_wx_id'] = trim($insert_array['admin_wx_id'],',');
			$res = $model->table('admin')->insert($insert_array);
			if($res){
				$model = $model->table('wxaccount')->where(array('wx_id'=>array('in',$insert_array['admin_wx_id'])))->update(array('admin_id'=>$res));
				showMessage(L('wx_admin_admin_account_add_succ'),'index.php?act=admin','succ');
			}else{
				showMessage(L('wx_admin_admin_account_add_failed'),'','error');
			}
		}
		//调取公众账号列表
		$wx_list = $model->field('wx_id,wx_name,admin_id,shop_data_type')->table('wxaccount')->where(true)->select();
		Tpl::output('wx_list',$wx_list);
		Tpl::showpage('admin_add');
	}
	/**
	 * 编辑管理员
	 */
	public function admin_editOp(){
		$model = Model();
		$admin_id = intval($_GET['admin_id']);
		if($admin_id <= 0){
			showMessage(L('wrong_argument'),'','error');
		}
		if($_SESSION['admin_is_super'] != 1){
			showMessage("您无权限修改管理员信息",'','error');
		}
		if(chksubmit()){
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam = array(
				array("input"=>trim($_POST['admin_name']),"require"=>"true","message"=>L('wx_admin_admin_account_must_write')),
				array("input"=>trim($_POST['admin_truename']),"require"=>"true","message"=>L('wx_admin_admin_truename_must_write'))
			);
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage($error,'','error');
			}
			$update_array = array();
			$update_array['admin_name'] = trim($_POST['admin_name']);
			$update_array['admin_truename'] = trim($_POST['admin_truename']);
			if(!empty($_POST['choose_wx'])){
				foreach ($_POST['choose_wx'] as $val){
					$update_array['admin_wx_id'] .= $val.',';
				}
			}
			$update_array['admin_wx_id'] = trim($update_array['admin_wx_id'],',');
			$res = $model->table('admin')->where(array('admin_id'=>$admin_id))->update($update_array);
			if($res){
				if($_POST['old_admin_wx_id'] != ''){
					$model->table('wxaccount')->where(array('wx_id'=>array('in',$_POST['old_admin_wx_id'])))->update(array('admin_id'=>0));
				}
				if($update_array['admin_wx_id'] != ''){
					$model->table('wxaccount')->where(array('wx_id'=>array('in',$update_array['admin_wx_id'])))->update(array('admin_id'=>$admin_id));
				}
				showMessage(L('wx_admin_admin_account_edit_succ'),'index.php?act=admin','succ');
			}else{
				showMessage(L('wx_admin_admin_account_edit_failed'),'','error');
			}
		}
		//调取管理员账号信息
		$admin_info = $model->table('admin')->where(array('admin_id'=>$admin_id))->find();
		Tpl::output('admin_info',$admin_info);
		Tpl::output('old_choose_wx',explode(',', $admin_info['admin_wx_id']));
		//调取公众账号列表
		$wx_list = $model->field('wx_id,wx_name,admin_id,shop_data_type')->table('wxaccount')->where(true)->select();
		Tpl::output('wx_list',$wx_list);
		Tpl::showpage('admin_edit');
	}
	/**
	 * 删除管理员
	 */
	public function admin_delOp(){
		$model = Model();
		$admin_id = intval($_GET['admin_id']);
		if($admin_id <= 0){
			showMessage(L('wrong_argument'),'','error');
		}
		if($_SESSION['admin_is_super'] != 1){
			showMessage("您无权限删除管理员",'','error');
		}
		$admin_info = $model->table('admin')->where(array('admin_id'=>$admin_id))->find();
		if($admin_info['admin_wx_id'] != ''){
			$model->table('wxaccount')->where(array('wx_id'=>array('in',$admin_info['admin_wx_id'])))->update(array('admin_id'=>0));
		}
		$res = $model->table('admin')->where(array('admin_id'=>$admin_id))->delete();
		if($res){
			showMessage(L('wx_admin_admin_account_del_succ'),'index.php?act=admin','succ');
		}else{
			showMessage(L('wx_admin_admin_account_del_failed'),'','error');
		}
	}
	/**
	 * 修改密码
	 */
	public function pwd_editOp(){
		$model = Model();
		$admin_id = intval($_GET['admin_id']);
		if($admin_id <= 0){
			showMessage(L('wrong_argument'),'','error');
		}
		if($_SESSION['admin_is_super'] != 1 && $_SESSION['admin_id'] != $admin_id){
			showMessage("您无权限修改该管理员密码",'','error');
		}
		if(chksubmit()){
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam = array(
				array("input"=>trim($_POST['ori_password']),"require"=>"true","message"=>L('wx_admin_ori_pwd_must_write')),
				array("input"=>trim($_POST['new_password']),"require"=>"true","message"=>L('wx_admin_new_pwd_must_write')),
				array("input"=>trim($_POST['re_new_password']),"require"=>"true","message"=>L('wx_admin_renew_pwd_must_write'))
			);
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage($error,'','error');
			}
			$admin_info = $model->table('admin')->where(array('admin_id'=>$admin_id))->find();
			if($admin_info['admin_password'] != md5(trim($_POST['ori_password']))){
				showMessage(L('wx_admin_ori_pwd_not_correct'),'','error');
			}
			if(trim($_POST['new_password']) != trim($_POST['re_new_password'])){
				showMessage(L('wx_admin_pwd_not_match'),'','error');
			}
			$res = $model->table('admin')->where(array('admin_id'=>$admin_id))->update(array('admin_password'=>md5(trim($_POST['new_password']))));
			if($res){
				showMessage(L('wx_admin_password_edit_succ'),'index.php?act=admin','succ');
			}else{
				showMessage(L('wx_admin_password_edit_failed'),'','error');
			}
		}
		Tpl::showpage('pwd_edit');
	}
}