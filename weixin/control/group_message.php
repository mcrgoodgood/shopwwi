<?php
/**
 * 群发消息管理
 * */
defined('InByShopWWI') or exit('Access Invalid!');
define('MYSQL_RESULT_TYPE',1);
class group_messageControl extends BaseHomeControl{
	var $wx_id;
	public function __construct(){
		parent::__construct();
		$this->wx_id = intval($_REQUEST['wx_id']);
		if($this->wx_id <= 0){
			showMessage('参数错误','','error');
		}
	}

	/**
	 * 群发消息（通过群发接口）
	 * */
    public function indexOp(){
    	$model = Model();
    	if(chksubmit()){
			$this->_group_msg($_POST);
    	}
    	$resource = $model->table('reply')->where(array('wx_id'=>$this->wx_id,'reply_type'=>2))->select();
    	//调取公众账号的接口信息
		$wx_info = $model->field('wx_appid,wx_appsecret,wx_accesstoken')->table('wxaccount')->where(array('wx_id'=>$this->wx_id))->find();
		$access_token = $wx_info['wx_accesstoken'];
		if($access_token == ''){
			$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $this->wx_id);
		}
		$url = 'https://api.weixin.qq.com/cgi-bin/groups/get?access_token='.$access_token;
		$res = @file_get_contents($url);
		$res = json_decode($res,true);
		if($res['errcode'] == '40001' || $res['errcode'] == '42001' || $res['errcode'] == '42002' || $res['errcode'] == '42003'){
			$access_token = $this->get_accesstoken($wx_info['wx_appid'], $wx_info['wx_appsecret'], $this->wx_id);
			$url = 'https://api.weixin.qq.com/cgi-bin/groups/get?access_token='.$access_token;
			$res = @file_get_contents($url);
			$res = json_decode($res,true);
		}
		if($res['errcode'] != ''){
			showMessage('分组信息拉取失败','','error');
		}else{
			Tpl::output('group_list',$res['groups']);
		}
		Tpl::output('resource',$resource);
        Tpl::showpage('service_index');
    }

    /**
     * 群发消息（通过客服接口）
     * */
    public function serviceOp(){
        Tpl::showpage('service_service');
    }

    /**
     * 发送记录
     * */
    public function listOp(){
		$model = Model();
		$list = $model->table('group_message')->select();
		Tpl::output('list',$list);
        Tpl::showpage('service_list');
    }

    /**
     * 帮助说明
     * */
    public function helpOp(){
        Tpl::showpage('service_help');
    }

    /**
     * 删除群发消息
     * */
    public function delOp(){
        $msg_id = intval($_GET['msg_id']);
        if($msg_id <= 0){
            showMessage('参数错误','?act=group_message&op=index&wx_id='.$this->wx_id,'error');
        }
        $model = Model();
        $message = $model->table('group_message')->where(array('msg_id'=>$msg_id))->find();
        $res = $model->table('group_message')->where(array('msg_id'=>$msg_id))->delete();
        if($message['wx_msg_id'] != ''){
            //发送成功的消息删除公众平台数据
            $this->_del_msg($message['wx_msg_id']);
        }
        if($res){
        	showMessage('删除成功','?act=group_message&op=list&wx_id='.$this->wx_id,'succ');
        }else{
        	showMessage('删除失败','?act=group_message&op=list&wx_id='.$this->wx_id,'error');
        }
    }

    /**
     * 删除群发消息
     * */
	private function _del_msg($msg_id){
	    $model = Model();
    	$wxinfo = $model->table('wxaccount')->where(array('wx_id'=>$this->wx_id))->find();
    	$url = "https://api.weixin.qq.com/cgi-bin/message/mass/delete?access_token=".$wxinfo['wx_accesstoken'];
    	$data = '{"msg_id":'.$msg_id.'}';
    	$result = curl_post($url,$data);
    	if(!$result['nc_status'] || $result['data']['errcode'] == '40001' || $result['data']['errcode'] == '42001' || $result['data']['errcode'] == '42002' || $result['data']['errcode'] == '42003'){
    		$wxinfo['wx_accesstoken'] = $this->_get_access_token();
    		$url = "https://api.weixin.qq.com/cgi-bin/message/mass/delete?access_token=".$wxinfo['wx_accesstoken'];
    		$result = curl_post($url,$data);
    	}
	}

    /**
     * 消息群发
     * */
    private function _group_msg($msg){
        $model = Model();
        $reply = $model->table('reply')->where(array('reply_id'=>$msg['resource']))->find();
        $group = array();
        $group['msg_title'] = $reply['reply_title'];
        $group['send_group'] = intval($msg['group']);
        $group['send_time'] = time();
        $group['res_id'] = $msg['resource'];
        $res = $model->table('group_message')->insert($group);
        $wxinfo = $model->table('wxaccount')->where(array('wx_id'=>$this->wx_id))->find();
        if($res){
        	//消息处理
        	$result = $this->_msg_action($wxinfo['wx_accesstoken'],$reply);
        	if($result['data']['errcode'] == '45009'){
        		showMessage('已达到消息发送上限','?act=group_message&op=index&wx_id='.$this->wx_id,'error');
        	}
        	if(!$result['nc_status'] || $result['data']['errcode'] == '40001' || $result['data']['errcode'] == '42001' || $result['data']['errcode'] == '42002' || $result['data']['errcode'] == '42003'){
        		$wxinfo['wx_accesstoken'] = $this->_get_access_token();
        		$result = $this->_msg_action($wxinfo['wx_accesstoken'],$reply);
        	}
        	if($result['nc_status']){
        	    $data = $result['data'];
        	    $up_data = array(
        	    		'created_at' => $data['created_at'],
        	    		'media_id' => $data['media_id'],
        	    		'type' => $data['type']
        	    );
        	    $model->table('group_message')->where(array('msg_id'=>$res))->update($up_data);
        	    //消息发送
				$this->_send_msg($wxinfo['wx_accesstoken'],$group,$up_data,$res);
        	}
        }else{
        	showMessage('消息发送失败','?act=group_message&op=index&wx_id='.$this->wx_id,'error');
        }

    }

    /**
     * 消息发送
     * */
    private function _send_msg($access_token,$group,$up_data,$msg_id){
        $url = "https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=".$access_token;
        //$data = '{"filter":{"is_to_all":false,"group_id":"100"},"mpnews":{"media_id":"MFRlu07rtJkMOQ7Y3ndXO3WH8uC6O4CMIC9s_-O-TJ86jn41vVnvc3I3J2gRx8vr"},"msgtype":"mpnews"}';
		$tmp = array(
				'filter' => array(
					'is_to_all' => false,
					'group_id' => $group['send_group']
				),
				'mpnews' => array('media_id' => $up_data['media_id']),
				'msgtype' => 'mpnews'
		);
		$data = json_encode($tmp);
        $result = curl_post($url,$data);
        /* echo '<pre>';
        echo $data.'<br>';
        echo $url.'<br>';
        print_r($result);
        exit; */
    	if(!$result['nc_status'] || $result['data']['errcode'] == '40001' || $result['data']['errcode'] == '42001' || $result['data']['errcode'] == '42002' || $result['data']['errcode'] == '42003'){
        	$access_token = $this->_get_access_token();
        	$url = "https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=".$access_token;
        	$result = curl_post($url,$data);
        }
        if($result['nc_status'] && $result['data']['errcode'] == '0'){
            $model = Model();
            $model->table('group_message')->where(array('msg_id'=>$msg_id))->update(array('wx_msg_id'=>$result['data']['msg_id']));
            showMessage('消息已发送','?act=group_message&op=list&wx_id='.$this->wx_id,'succ');
        }else{
        	if($result['data']['errcode'] == '42004'){
        		showMessage('已达到消息发送上限','?act=group_message&op=index&wx_id='.$this->wx_id,'error');
        	}else{
        		showMessage('消息发送失败','?act=group_message&op=index&wx_id='.$this->wx_id,'error');
        	}
        }
    }

    /**
     * 消息数据处理
     * */
    private function _msg_action($access_token,$param){
        $url = "https://api.weixin.qq.com/cgi-bin/media/uploadnews?access_token=".$access_token;
        $article = unserialize($param['reply_content']);
        $i = 0;
        $data = '{
   "articles": [';
        $comm = '';
        foreach($article as $k => $v){
        	if($i == 10) break;
        	$str = '';
            if(isset($v['media_id']) && isset($v['created_at']) && $v['created_at']+3600*24*2.9 > time()){
				if($i == 0){
					$tmp['show_cover_pic'] = 1;
					$str .= $comm.'{
            "thumb_media_id":"'.$v['media_id'].'",
            "author":"",
			"title":"'.$v['title'].'",
			"content_source_url":"'.$v['url'].'",
			"content":"'.$v['desc'].'",
			"digest":"'.$v['desc'].'",
            "show_cover_pic":"1"
		 }';
				}else{
					$str .= $comm.'{
            "thumb_media_id":"'.$v['media_id'].'",
            "author":"",
			"title":"'.$v['title'].'",
			"content_source_url":"'.$v['url'].'",
			"content":"'.$v['desc'].'",
			"digest":"'.$v['desc'].'",
            "show_cover_pic":"0"
		 }';
				}
				$i++;
            }else{
				$file = $this->_upload_file($access_token,$v);
				if(!$file['nc_status'] || $file['data']['errcode'] == '40001' || $file['data']['errcode'] == '42001' || $file['data']['errcode'] == '42002' || $file['data']['errcode'] == '42003'){
					$access_token = $this->_get_access_token();
					$file = $this->_upload_file($access_token,$v);
				}
				if($file['nc_status']){
					$v['media_id'] = $file['data']['media_id'];
					$v['created_at'] = $file['data']['created_at'];
					if($i == 0){
						$tmp['show_cover_pic'] = 1;
						$str .= $comm.'{
	            "thumb_media_id":"'.$v['media_id'].'",
	            "author":"",
				"title":"'.$v['title'].'",
				"content_source_url":"'.$v['url'].'",
				"content":"'.$v['desc'].'",
				"digest":"'.$v['desc'].'",
	            "show_cover_pic":"1"
			 }';
					}else{
						$str .= $comm.'{
	            "thumb_media_id":"'.$v['media_id'].'",
	            "author":"",
				"title":"'.$v['title'].'",
				"content_source_url":"'.$v['url'].'",
				"content":"'.$v['desc'].'",
				"digest":"'.$v['desc'].'",
	            "show_cover_pic":"0"
			 }';
					}
					$i++;
				}
            }
            $data .= $str;
            $article[$k] = $v;
            $comm = ',';
        }
        $data .= '   ]
}';
        $model = Model();
        $model->table('reply')->where(array('reply_id'=>$param['reply_id']))->update(array('reply_content'=>serialize($article)));
        return curl_post($url,$data);
    }

    /**
     * 上传媒体资源
     * */
    private function _upload_file($access_token,$info){
        $url = "http://file.api.weixin.qq.com/cgi-bin/media/upload?access_token={$access_token}&type=image";
        $img = str_replace(UPLOAD_SITE_URL,BASE_UPLOAD_PATH,$info['picurl']);
        $data = array('media'=>'@'.$img);
        return curl_post($url,$data);
    }

    /**
     * 获取ACCESS_TOKEN
     * */
    private function _get_access_token(){
    	$model = Model();
    	$wxinfo = $model->table('wxaccount')->where(array('wx_id'=>$this->wx_id))->find();
    	$appids = $wxinfo['wx_appid'];
    	$appsecret = $wxinfo['wx_appsecret'];
    	$url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='.$appids.'&secret='.$appsecret;
    	$res = curl_get($url);
    	$json=json_decode($res);
    	$model->table('wxaccount')->where(array('wx_id'=>$this->wx_id))->update(array('wx_accesstoken'=>$json->access_token));
    	return $json->access_token;
    }
}