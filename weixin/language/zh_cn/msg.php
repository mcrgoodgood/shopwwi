<?php
defined('InByShopWWI') or exit('Access Invalid!');

$lang['msg_request_failed'] = '远程请求失败';

$lang['msg_manage'] = '消息管理';
$lang['msg_nickname'] = '昵称';
$lang['msg_content'] = '内容';
$lang['msg_message_content'] = '消息内容';
$lang['msg_important_msg'] = '重要消息';
$lang['msg_send_time'] = '发送时间';
$lang['msg_important'] = '重要';
$lang['msg_send_msg'] = '发消息';
$lang['msg_cancel_important'] = '取消重要';
$lang['msg_set_important'] = '设为重要';
$lang['msg_no_record'] = '暂无相关记录';
$lang['msg_send'] = '发送';
$lang['msg_send_msg_not_be_null'] = '发送的消息不能为空';
$lang['msg_send_msg_succ'] = '消息发送成功！';
$lang['msg_send_msg_failed'] = '消息发送失败';