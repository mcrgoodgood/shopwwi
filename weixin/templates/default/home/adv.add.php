<div class="main_hd">
  <h2><?php echo $lang['nc_adv_manage'];?></h2>
</div>
<div class="main_bd">
  <form method="post" id="apply_form" action="?act=adv&op=addAdv&wx_id=<?php echo intval($_GET['wx_id']);?>" enctype="multipart/form-data">
    <ul>
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo $lang['nc_adv_name'];?>:</label>
        <div class="label_form"> 
          <span>
          	<input type="text" class="label_input" name="adv_name" id="adv_name">
            <label for='adv_name' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>
      
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo $lang['nc_adv_sort'];?>:</label>
        <div class="label_form">
          <span>
          	<input type="text" class="label_input" name="adv_sort" id="adv_sort">
            <label for='adv_sort' class='error msg_invalid' style='display:none;'></label>
          </span> 
        </div>
      </li>
      
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo $lang['nc_adv_state'];?>:</label>
        <div class="label_form">
          <span>
				<input type="radio" name="adv_state" value="0" checked>&nbsp;<?php echo $lang['nc_adv_open'];?> &nbsp;&nbsp;
				<input type="radio" name="adv_state" value="1">&nbsp;<?php echo $lang['nc_adv_close'];?>
          </span> 
        </div>
      </li>     
      
      <li class="list_item">
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo $lang['nc_adv_pic'];?>:</label>
        <div class="label_form">
          <span>
			<input type="file" name="adv_pic" id="adv_pic">
			<label for='adv_pic' class='error msg_invalid' style='display:none;'></label>
          </span> 
        </div>
      </li>       
      
      <li>
        <div class="btn_bar">
        <input type="submit" class="btn_input" value="<?php echo $lang['nc_save'];?>"></div>
      </li>
    </ul>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script> 
<script type="text/javascript">
$(function(){	
	$("#apply_form").validate({//验证表单
        rules: {
        	adv_name: {
				required:true
            },
            adv_sort:{
            	required:true,
            	number:true,
            	min:0,
            	max:100
            },
            adv_pic:{
            	required:true,
            	accept:'jpg|jpeg|gif|png'
            }
        },
        messages:{
        	adv_name:{
        		required:'<?php echo $lang['nc_adv_name_is_not_null'];?>'
            },
            adv_sort:{
            	required:'<?php echo $lang['nc_adv_sort_is_not_null'];?>',
            	number:'<?php echo $lang['nc_adv_sort_is_not_number'];?>',
            	min:'<?php echo $lang['nc_adv_sort_range'];?>',
            	max:'<?php echo $lang['nc_adv_sort_range'];?>'
            },
            adv_pic:{
            	required:'<?php echo $lang['nc_adv_pic_is_not_null'];?>',
            	accept:'<?php echo $lang['nc_adv_pic_is_not_format'];?>'				
            }
        }
	});
});
</script> 
