<style>
.main_bd{ padding:20px 10px;}
</style>
<div class="main_hd">
  <h2><?php echo '渠道管理';?></h2>
  <p class="extra_info"> <a href="?act=recongition&op=add&wx_id=<?php echo intval($_GET['wx_id']);?>"><?php echo $lang['nc_add']?></a> </p>
</div>
<div class="main_bd">
  <div class="table_msg">
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell time asc"><?php echo '渠道名称';?></th>
          <th class="table_cell goods"><?php echo '关键词';?></th>
          <th class="table_cell goods"><?php echo '使用次数';?></th>
          <th class="table_cell goods"><?php echo '渠道二维码';?></th>
          <th class="table_cell goods"><?php echo '状态';?></th>
          <th class="table_cell"><?php echo $lang['nc_handle'];?></th>
        </tr>
      </thead>
      <tbody class="nc-tbody">
        <?php if(!empty($output['list'])){?>
        <?php foreach($output['list'] as $key=>$val){?>
        <tr>
          <td><?php if(mb_strlen($val['channel_name'],'utf-8')>=12){ echo mb_substr($val['channel_name'],0,12,'utf-8').'...';}else{ echo $val['channel_name'];}?></td>
          <td><?php echo $val['keyword'];?></td>
          <td><?php echo $val['use_times'];?></td>
          <td><?php if($val['channel_images']!=''){?>
          	<a href="https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=<?php echo $val['channel_images']?>" target="_blank">查看二维码</a>
            <?php }else{?>
            <a href="?act=recongition&op=get_queen&wx_id=<?php echo intval($_GET['wx_id']);?>&chn_id=<?php echo intval($val['channel_id'])?>">获取二维码</a>
            <?php }?>
          </td>
          <td><?php echo $val['status']?'已启用':'已停用';?></td>
          <td class="last">
          <?php if($val['status'] == 1){?>
           <a href="javascript:void(0);" onClick="change_chn(<?php echo $val['channel_id'];?>,0)"><?php echo '停用';?></a> <span>|</span>
          <?php }else{?>
           <a href="javascript:void(0);" onClick="change_chn(<?php echo $val['channel_id'];?>,1)"><?php echo '启用';?></a> <span>|</span>
          <?php }?>
          <a href="javascript:void(0);" onClick="deleteitem(<?php echo $val['channel_id']?>)"><?php echo '删除';?></a>
          </td>
        </tr>
        <?php }?>
        <?php }else{?>
        <tr>
          <td colspan="20" valign="middle" ><div class="norecord"><span><?php echo $lang['nc_record'];?></span></div></td>
        </tr>
        <?php }?>
      </tbody>
    </table>
  </div>
  <div class="pagination"> <?php echo $output['page'];?> </div>
</div>
<script type="text/javascript">
	function deleteitem(activity_id){
		if(confirm('<?php echo '确认要删除吗？'?>')){
			location.href = "?act=recongition&op=del&wx_id=<?php echo intval($_GET['wx_id']);?>&chn_id="+activity_id;
		}
	}
	function change_chn(chn_id,state){
		location.href = "?act=recongition&op=ajax&branch=chn_change&wx_id=<?php echo intval($_GET['wx_id']);?>&chn_id="+chn_id+"&state="+state;
	}
	
</script> 
