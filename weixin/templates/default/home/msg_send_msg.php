<?php defined('InByShopWWI') or exit('Access Invalid!');?>
<div class="main_hd">
  <h2><?php echo L('msg_send_msg'); ?></h2>
  <p class="extra_info"><a href="index.php?act=msg&op=msg_manage&wx_id=<?php echo intval($_GET['wx_id']); ?>"><?php echo L('msg_manage'); ?></a></p>
</div>
<div class="main_bd">
    <ul>
   		 <li class="list_item">
	        <label class="label_box"><?php echo L('msg_nickname').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <span>
	          	<?php echo $output['fans_info']['fans_nickname']; ?>
	          </span>     
	        </div>
         </li>
         <li class="list_item">
	        <label class="label_box"><?php echo L('msg_message_content').L('nc_colon'); ?></label>
	        <div class="label_form"> 
	          <textarea id="msg" style="width:400px;height:180px"></textarea>     
	        </div>
         </li>
         <li>
	        <div class="btn_bar">
	        <input type="submit" class="btn_input" value="<?php echo L('msg_send'); ?>"></div>
	     </li>
    </ul>
</div>
<script type="text/javascript">
$(function(){
	$('.btn_input').click(function(){
		if($('#msg').val() == ''){
			alert('<?php echo L('msg_send_msg_not_be_null'); ?>');
		}else{
			$.getJSON('index.php?act=msg&op=send_msg&wx_id=<?php echo intval($_GET['wx_id']); ?>',{
				'form_submit':'ok',
				'msg':$('#msg').val(),
				'openid':'<?php echo $output['fans_info']['fans_openid']; ?>'
			},function(result){
	        if(result.done){
	        	alert('<?php echo L('msg_send_msg_succ'); ?>');
	        }else{
	            alert('<?php echo L('msg_send_msg_failed'); ?>('+result.msg+')');
	        }
	    	});	
		}
	});
})
</script>