<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="<?php echo TEMPLATES_URL; ?>/css/css.css" />
	<title>微调研-<?php echo $output['dyinfo']['item_name'];?></title>
    <style>
    .l-wrap-load,.l-wrap-index{background:url(<?php echo TEMPLATES_URL; ?>/images/wdy/bg.jpg) no-repeat 0 0;}
	.l-wrap-qus{background:url(<?php echo TEMPLATES_URL; ?>/images/wdy/qus-tri.png) no-repeat 100% 100%,url(<?php echo TEMPLATES_URL; ?>/images/wdy/qus-bg_hei.jpg) no-repeat 0 0;}
    </style>
    <script type="text/javascript">
    	var $STCONFIG = {
			VERSION 	: '1.0',
			SHAREIT 	: {
				title : '分享标题', 
				con : '分享内容',
				link : document.URL, 
				img  : "图片地址"	
			}
		};
		if(/Android (\d+\.\d+)/.test(navigator.userAgent)){
			var version = parseFloat(RegExp.$1);
			if(version > 2.3){
				var phoneScale = parseInt(window.screen.width) / 640;
				document.write('<meta name="viewport" content="width=640, minimum-scale = '+ phoneScale +', maximum-scale = '+ phoneScale +', target-densitydpi=device-dpi">');
			}else{
				document.write('<meta name="viewport" content="width=640, target-densitydpi=device-dpi">');
			}
		}else{
			document.write('<meta name="viewport" content="width=640, user-scalable=no, target-densitydpi=device-dpi">');
		}
		if(navigator.userAgent.indexOf('MicroMessenger') >= 0){
			document.addEventListener('WeixinJSBridgeReady', function() {
			});
		}
		var TEMPLATESURL = "<?php echo TEMPLATES_URL;?>";
    </script>
</head>
<body>
	<section class="l-wrap l-wrap-index l-animation J-index">
		<div class="l-index-c">
        	<h1><?php echo $output['res']['item_name'];?></h1>
            <p>
            	感谢您对本次【<?php echo $output['res']['item_name'];?>】调研的支持！我们已经收到您的意见。
            </p>
        </div>
	</section>
</body>
</html>