<style>
.main_bd{ padding:20px 10px;}
.table_msg table tbody tr td{ padding-left:0;}
</style>
<div class="main_hd">
  <h2><?php echo '微调研活动';?></h2>
  <p class="extra_info"> <a href="?act=research&op=add_research&wx_id=<?php echo intval($_GET['wx_id']);?>"><?php echo $lang['nc_add']?></a> </p>
</div>
<div class="main_bd">
  <div class="table_msg">
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell time asc"><?php echo '调研标题';?></th>
          <th class="table_cell goods"><?php echo '关键词';?></th>
          <th class="table_cell goods"><?php echo '开始时间';?></th>
          <th class="table_cell goods"><?php echo '结束时间';?></th>
          <th class="table_cell goods"><?php echo '参与人数';?></th>
          <th class="table_cell"><?php echo $lang['nc_handle'];?></th>
        </tr>
      </thead>
      <tbody class="nc-tbody">
        <?php if(!empty($output['list'])){?>
        <?php foreach($output['list'] as $key=>$val){?>
        <tr>
          <td><?php if(mb_strlen($val['item_name'],'utf-8')>=12){ echo mb_substr($val['item_name'],0,12,'utf-8').'...';}else{ echo $val['item_name'];}?></td>
          <td><?php echo $val['keyword'];?></td>
          <td><?php echo date("Y-m-d H:i:s",$val['start_time']);?></td>
          <td><?php echo date("Y-m-d H:i:s",$val['end_time']);?></td>
          <td><?php echo $val['item_num'];?></td>
          <td class="last"><a href="?act=research&op=research&res_id=<?php echo $val['item_id'];?>&wx_id=<?php echo intval($_GET['wx_id']);?>"><?php echo '奖项设置';?></a> <span>|</span> <a href="?act=research&op=edit_research&res_id=<?php echo $val['item_id'];?>&wx_id=<?php echo intval($_GET['wx_id']);?>"><?php echo '修改';?></a> <span>|</span> <a href="javascript:void(0);" onClick="deleteitem(<?php echo $val['item_id']?>)"><?php echo '删除';?></a> <span>|</span> <a href="?act=research&op=info&res_id=<?php echo $val['item_id']?>&wx_id=<?php echo intval($_GET['wx_id']);?>"><?php echo '活动统计';?></a> <span>|</span> <a href="?act=research&op=lottery&res_id=<?php echo $val['item_id']?>&wx_id=<?php echo intval($_GET['wx_id']);?>"><?php echo '抽奖管理';?></a></td>
        </tr>
        <?php }?>
        <?php }else{?>
        <tr>
          <td colspan="20" valign="middle" ><div class="norecord"><span><?php echo $lang['nc_record'];?></span></div></td>
        </tr>
        <?php }?>
      </tbody>
    </table>
  </div>
  <div class="pagination"> <?php echo $output['page'];?> </div>
</div>
<script type="text/javascript">
	function deleteitem(activity_id){
		if(confirm('<?php echo '确认要删除吗？'?>')){
			location.href = "?act=research&op=del&wx_id=<?php echo intval($_GET['wx_id']);?>&res_id="+activity_id;
		}
	}
</script> 
