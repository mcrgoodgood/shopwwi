<?php
/**
 * 登录 验证 退出 操作
 *
 *
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
class loginControl extends BaseHomeControl {
	/**
	 * 不进行父类的登录验证，所以增加构造方法重写了父类的构造方法
	 */
	public function __construct(){
		Tpl::setDir('home');
		import('function.seccode');
		Language::read('common,layout,login');
	}
	/**
	 * 登陆
	 */
	public function loginOp(){
		if (chksubmit()){
			Security::checkToken();
			//登录验证
			$obj_validate = new Validate();
			$obj_validate->validateparam = array(
			array("input"=>$_POST["user_name"],		"require"=>"true", "message"=>L('login_index_username_null')),
			array("input"=>$_POST["password"],		"require"=>"true", "message"=>L('login_index_password_null')),
			array("input"=>$_POST["captcha"],		"require"=>"true", "message"=>L('login_index_checkcode_null')),
			);
			$error = $obj_validate->validate();
			if ($error != ''){
				showDialog(L('error').$error);
			}else {
				if (!checkSeccode($_POST['nchash'],$_POST['captcha'])){
					showDialog(L('login_index_checkcode_wrong').$error);
				}
				$model = Model();
				$where	= array();
				$where['admin_name']	= $_POST['user_name'];
				$where['admin_password']= md5(trim($_POST['password']));
				$admin_info = $model->table('admin')->where($where)->find();
				if(!empty($admin_info)) {
					$update_info	= array(
					'admin_id'=>$admin_info['admin_id'],
					'admin_login_num'=>($admin_info['admin_login_num']+1),
					'admin_login_time'=>TIMESTAMP
					);
					$model->table('admin')->where(array('admin_id'=>$admin_info['admin_id']))->update($update_info);
					$_SESSION['is_login']	= '1';
					$_SESSION['admin_id']	= $admin_info['admin_id'];
					$_SESSION['admin_truename'] = $admin_info['admin_truename'];
					$_SESSION['admin_is_super'] = $admin_info['admin_is_super'];
					@header('Location: index.php');exit;
				}else {
					showDialog(L('login_index_username_password_wrong'),'index.php?act=login&op=login');
				}
			}
		}
		Tpl::output('nchash',substr(md5(ADMIN_SITE_URL.$_GET['act'].$_GET['op']),0,8));
		Tpl::output('html_title',L('login_index_need_login'));
		Tpl::showpage('login','login_layout');
	}
	/**
	 * 退出
	 */
	public function logoutOp(){
		session_unset();
		session_destroy();
		@header("Location: index.php");
		exit;
	}
}