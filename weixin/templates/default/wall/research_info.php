<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/css.css">
	<script type="text/javascript">
		var $STCONFIG = {
			VERSION 	: '1.0',
			SHAREIT 	: {
				title : '分享标题', 
				con : '分享内容',
				link : document.URL, 
				img  : "图片地址"	
			}
		};
		if(/Android (\d+\.\d+)/.test(navigator.userAgent)){
			var version = parseFloat(RegExp.$1);
			if(version > 2.3){
				var phoneScale = parseInt(window.screen.width) / 640;
				document.write('<meta name="viewport" content="width=640, minimum-scale = '+ phoneScale +', maximum-scale = '+ phoneScale +', target-densitydpi=device-dpi">');
			}else{
				document.write('<meta name="viewport" content="width=640, target-densitydpi=device-dpi">');
			}
		}else{
			document.write('<meta name="viewport" content="width=640, user-scalable=no, target-densitydpi=device-dpi">');
		}
		if(navigator.userAgent.indexOf('MicroMessenger') >= 0){
			document.addEventListener('WeixinJSBridgeReady', function() {
			});
		}
	</script>
	<title>艾奇奖</title>
</head>
<body>
	<section class="l-wrap l-wrap-load l-load-show J-load">
		<div class="l-load-box">
			<div class="l-load-img"></div>
			<div class="l-load-txt"></div>
		</div>
	</section>
	<section class="l-wrap l-wrap-index l-animation J-index">
		<div class="l-index-c"></div>
		<a class="l-btn l-start-btn J-start-btn" href="javascript:;"></a>
		<div class="l-cooperate">艾奇奖技术支持伙伴<br/>时趣 SocialTouch</div>
	</section>
	<section class="l-wrap-qus l-wrap-qus1 J-qus">
		<div class="l-qus1-pet l-qus-pet"></div>
		<div class="l-qus-box">
			<div class="l-qus1-tit"></div>
			<div class="l-qus-ans">
				<div class="l-qus1-ans-a fix J-ans-sel" data-sel="a">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
				<div class="l-qus1-ans-b fix J-ans-sel" data-sel="b">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
				<div class="l-qus1-ans-c fix J-ans-sel" data-sel="c">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
				<div class="l-qus1-ans-d fix J-ans-sel" data-sel="d">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
			</div>
		</div>
		<div class="l-error J-error"></div>
		<div class="l-qus-next J-next"></div>
	</section>
	<section class="l-wrap-qus l-wrap-qus2 J-qus">
		<div class="l-qus2-pet l-qus-pet"></div>
		<div class="l-qus-box">
			<div class="l-qus2-tit"></div>
			<div class="l-qus-ans">
				<div class="l-qus2-ans-a fix J-ans-sel" data-sel="a">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
				<div class="l-qus2-ans-b fix J-ans-sel" data-sel="b">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
				<div class="l-qus2-ans-c fix J-ans-sel" data-sel="c">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
				<div class="l-qus2-ans-d fix J-ans-sel" data-sel="d">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
			</div>
		</div>
		<div class="l-error J-error"></div>
		<div class="l-qus-next J-next"></div>
	</section>
	<section class="J-qus l-wrap-qus l-wrap-qus3">
		<div class="l-qus3-pet l-qus-pet"></div>
		<div class="l-qus-box">
			<div class="l-qus3-tit"></div>
			<div class="l-qus-ans">
				<div class="l-qus3-ans-a fix J-ans-sel" data-sel="a">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
				<div class="l-qus3-ans-b fix J-ans-sel" data-sel="b">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
				<div class="l-qus3-ans-c fix J-ans-sel" data-sel="c">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
				<div class="l-qus3-ans-d fix J-ans-sel" data-sel="d">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
			</div>
		</div>
		<div class="l-error J-error"></div>
		<div class="l-qus-next J-next"></div>
	</section>
	<section class="J-qus l-wrap-qus l-wrap-qus4">
		<div class="l-qus4-pet l-qus-pet"></div>
		<div class="l-qus-box">
			<div class="l-qus4-tit"></div>
			<div class="l-qus-ans">
				<div class="l-qus4-ans-a fix J-ans-sel" data-sel="a">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
				<div class="l-qus4-ans-b fix J-ans-sel" data-sel="b">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
				<div class="l-qus4-ans-c fix J-ans-sel" data-sel="c">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
			</div>
		</div>
		<div class="l-error J-error"></div>
		<div class="l-qus-next J-next"></div>
	</section>
	<section class="J-qus l-wrap-qus l-wrap-qus5">
		<div class="l-qus5-pet l-qus-pet"></div>
		<div class="l-qus-box">
			<div class="l-qus5-tit"></div>
			<div class="l-qus-ans">
				<div class="l-qus5-ans-a fix J-ans-sel" data-sel="a">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
				<div class="l-qus5-ans-b fix J-ans-sel" data-sel="b">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
				<div class="l-qus5-ans-c fix J-ans-sel" data-sel="c">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
			</div>
		</div>
		<div class="l-error J-error"></div>
		<div class="l-qus-next J-next"></div>
	</section>
	<section class="J-qus l-wrap-qus l-wrap-qus6">
		<div class="l-qus6-pet l-qus-pet"></div>
		<div class="l-qus-box">
			<div class="l-qus6-tit"></div>
			<div class="l-qus-ans">
				<div class="l-qus6-ans-a fix J-ans-sel" data-sel="a">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
				<div class="l-qus6-ans-b fix J-ans-sel" data-sel="b">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
				<div class="l-qus6-ans-c fix J-ans-sel" data-sel="c">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
				<div class="l-qus6-ans-d fix J-ans-sel" data-sel="d">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
			</div>
		</div>
		<div class="l-error J-error"></div>
		<div class="l-qus-next J-next"></div>
	</section>
	<section class="J-qus l-wrap-qus l-wrap-qus7">
		<div class="l-qus7-pet l-qus-pet"></div>
		<div class="l-qus-box">
			<div class="l-qus7-tit"></div>
			<div class="l-qus-ans">
				<div class="l-qus7-ans-a fix J-ans-sel" data-sel="a">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
				<div class="l-qus7-ans-b fix J-ans-sel" data-sel="b">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
				<div class="l-qus7-ans-c fix J-ans-sel" data-sel="c">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
				<div class="l-qus7-ans-d fix J-ans-sel" data-sel="d">
					<span class="l-qus-ans-sel"></span>
					<span class="l-qus-ans-txt"></span>
				</div>
			</div>
		</div>
		<div class="l-error J-error"></div>
		<div class="l-qus-tj J-tj J-next"></div>
	</section>
	<section class="J-res l-wrap-res">
		<div class="l-res-logo"></div>
		<div class="l-res-con J-res-con"></div>
		<div class="l-res-btn J-show-score"></div>
	</section>
	<section class="l-wrap-share l-wrap-res J-share">
		<div class="l-share-img"></div>
	</section>
	<section class="l-intro-sec J-intro-box">
		<div class="l-wrap-intro" id="J-intro-scroll">
			<div class="l-intro-scroll">
				<div class="l-intro-box"></div>
				<div class="l-intro-bot"></div>
			</div>
		</div>
		<a class="l-intro-fix J-intro-fix" href="http://www.eciawards.org/"></a>
	</section>
	<script src="<?php echo TEMPLATES_URL; ?>/js/wdy/sea.js?version=1.0"></script>
	<script src="<?php echo TEMPLATES_URL; ?>/js/wdy/modules/app/main.js?version=1.0"></script>
</body>
</html>