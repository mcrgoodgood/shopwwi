<?php defined('InByShopWWI') or exit('Access Invalid!');?>
<div class="main_hd">
  <h2><?php echo L('wx_admin_admin_account_list'); ?></h2>
  <p class="extra_info"><a href="index.php?act=admin&op=admin_add"><?php echo L('nc_add'); ?></a></p>
</div>
<div class="main_bd">
<div class="table_msg">
  <table cellspacing="0" cellpadding="0">
    <thead class="nc-thead">
      <tr>
        <th class="table_cell" style="width:20%"><?php echo L('wx_admin_admin_account'); ?></th>
        <th class="table_cell" style="width:20%"><?php echo L('wx_admin_admin_truename'); ?></th>
        <th class="table_cell"><?php echo L('wx_admin_last_login_time'); ?></th>
        <th class="table_cell" style="width:25%"><?php echo L('nc_handle'); ?></th>
      </tr>
    </thead>
    <tbody class="nc-tbody">
		<?php if(!empty($output['admin_list'])){?>
		<?php foreach($output['admin_list'] as $key=>$val){?>
		<tr>
		  <td><?php echo $val['admin_name']; ?></td>
		  <td><?php echo $val['admin_truename']; ?></td>
		  <td><?php echo $val['admin_login_time']!=0?date('Y-m-d H:i:s',$val['admin_login_time']):L('wx_admin_never_login'); ?></td>
		  <td class="last"><a href="index.php?act=admin&op=pwd_edit&admin_id=<?php echo $val['admin_id']?>"><?php echo L('wx_admin_password_edit'); ?></a><?php if($val['admin_is_super'] != 1){ ?>&nbsp;|&nbsp;<a href="index.php?act=admin&op=admin_edit&admin_id=<?php echo $val['admin_id']?>"><?php echo $lang['nc_edit'];?></a>&nbsp;|&nbsp;<a href="javascript:if(confirm('<?php echo L('wx_admin_admin_account_del_confirm'); ?>'))window.location.href='index.php?act=admin&op=admin_del&admin_id=<?php echo $val['admin_id'];?>';"><?php echo $lang['nc_delete']?></a><?php } ?></td>
		</tr>
		<?php }?>
		<?php }else { ?>
        <tr>
          <td colspan="15" style="text-align:center"><?php echo L('wx_admin_no_record'); ?></td>
        </tr>
        <?php } ?>
    </tbody>  
  </table>
</div>
</div>