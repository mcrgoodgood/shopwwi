<style>
.main_bd{ padding:20px 10px;}
.w300 { width:300px !important;}
</style>
<div class="main_hd">
  <h2><?php echo '微调研活动-奖项设置';?></h2>
  <p class="extra_info"> <a href="?act=research&op=list&wx_id=<?php echo intval($_GET['wx_id']);?>"><?php echo '返回列表';?></a> </p>
</div>
<div class="main_bd">
  <div class="table_msg">
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell time asc w300"><?php echo '活动标题';?></th>
          <th class="table_cell goods"><?php echo '关键词';?></th>
          <th class="table_cell goods"><?php echo '开始时间/结束时间';?></th>
          <th class="table_cell goods"><?php echo '参与人数';?></th>
          <th class="table_cell"><?php echo $lang['nc_handle'];?></th>
        </tr>
      </thead>
      <tbody class="nc-tbody">
        <?php if(!empty($output['info'])){
				$val = $output['info'];
			?>
        <tr>
          <td><?php if(mb_strlen($val['lot_title'],'utf-8')>=12){ echo mb_substr($val['lot_title'],0,12,'utf-8').'...';}else{ echo $val['lot_title'];}?></td>
          <td><?php echo $val['lot_keyword'];?></td>
          <td><?php echo date("Y-m-d",$val['start_time']);?><br /><?php echo date("Y-m-d",$val['end_time']);?></td>
          <td><?php echo $val['joinnum'];?></td>
          <td class="last"><a href="?act=research&op=res_edit&lot_id=<?php echo $val['lot_id'];?>&wx_id=<?php echo intval($_GET['wx_id']);?>"><?php echo '修改';?></a> <span>|</span> <a href="javascript:void(0);" onClick="deleteitem(<?php echo $val['lot_id']?>)"><?php echo '删除';?></a></td>
        </tr>
        <?php }else{?>
        <tr>
          <td colspan="20" valign="middle" ><div class="norecord"><span><?php echo $lang['nc_record'];?></span></div></td>
        </tr>
        <?php }?>
      </tbody>
    </table>
  </div>
  <div class="pagination"> <?php echo $output['page'];?> </div>
</div>
<script type="text/javascript">
	function deleteitem(activity_id){
		if(confirm('<?php echo '确认要删除吗？'?>')){
			location.href = "?act=research&op=res_del&wx_id=<?php echo intval($_GET['wx_id']);?>&lot_id="+activity_id;
		}
	}
</script> 
