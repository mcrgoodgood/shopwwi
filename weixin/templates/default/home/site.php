<div class="main_hd">
  <h2><?php echo $lang['nc_site_setting'];?></h2>
</div>
<div class="main_bd">
  <form method="post" id="apply_form" action="?act=site&op=editSite&wx_id=<?php echo intval($_GET['wx_id']);?>">
    <ul>
      <li class="list_item">
      
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo $lang['nc_site_name'];?>:</label>
        <div class="label_form"> 
          <span>
          	<input type="text" class="label_input" name="site_name" id="site_name" value="<?php echo $output['site']['site_name'];?>">
            <label for='site_name' class='error msg_invalid' style='display:none;'></label>
          </span>     
        </div>
      </li>
      
      <li class="list_item">     
        <label class="label_box"> <font style="color:red">*&nbsp;</font><?php echo $lang['nc_site_keyword'];?>:</label>
        <div class="label_form">
          <span>
          	<input type="text" class="label_input" name="seo_keyword" id="seo_keyword" value="<?php echo $output['site']['seo_keyword'];?>">
            <label for='seo_keyword' class='error msg_invalid' style='display:none;'></label>
          </span>   
        </div>
      </li>

      <li class="list_item">
      
        <label class="label_box"><font style="color:red">*&nbsp;</font><?php echo $lang['nc_site_url'];?>:</label>
        <div class="label_form">
          <span>
          	<input type="text" class="label_input" name="mall_url" id="mall_url" value="<?php echo $output['site']['mall_url'];?>">
            <label for='mall_url' class='error msg_invalid' style='display:none;'></label>
          </span>   
        </div>
      </li> 
 
      <li class="list_item">
      
        <label class="label_box">&nbsp;</label>
        <div class="label_form">
          <span>
			<input onclick="javascript:test_connect();"  class="update-link" type="button">
          </span>   
        </div>
      </li> 
      
      <li class="list_item">
      
        <label class="label_box"><?php echo $lang['nc_site_content'];?>:</label>
        <div class="label_form">
          <span>
			<textarea name="seo_content" style="width:350px;height:100px"><?php echo $output['site']['seo_content'];?></textarea>   
          </span>   
        </div>
      </li>
            
      <li>
        <div class="btn_bar">
        	<input type="hidden" name="site_id" value="<?php echo $output['site']['site_id']?>">
        	<input type="submit" class="btn_input" value="<?php echo $lang['nc_save'];?>">
        </div>
      </li>
    </ul>
  </form>
</div>
<script type="text/javascript" src="<?php echo RESOURCE_SITE_URL;?>/js/jquery.validation.min.js"></script> 
<script type="text/javascript">
function test_connect(){
	var url = $('#mall_url').val();	
	$.ajax({
		type:'get',
		url:'index.php?act=site&op=test&wx_id=<?php echo intval($_GET['wx_id'])?>&url='+url,
		dataType:'json',
		success:function(json){
			if(json.result){
				alert(json.msg);
				return false;	
			}else{
				alert(json.msg);
				return false;
			}
		}
	});
}

$(function(){	
	$("#apply_form").validate({//验证表单
        rules: {
        	site_name:{
				required:true
            },
            seo_keyword:{
            	required:true
            },
            mall_url:{
            	required:true,
            	url:true
            }
        },
        messages:{
        	site_name:{
        		required:'<?php echo $lang['nc_site_name_is_not_null'];?>'
            },
            seo_keyword:{
            	required:'<?php echo $lang['nc_site_keyword_is_not_null'];?>'
            },
            mall_url:{
            	required:'<?php echo $lang['nc_site_url_is_not_null'];?>',
            	url:'<?php echo $lang['nc_site_url_is_not_format'];?>'
            }           
        }
	});
});
</script> 