<div class="main_hd">
  <h2>活动消息</h2>
  <p class="extra_info"></p>
</div>
<div class="main_bd">
  <div class="table_msg">
  	<form method="post" id="list_form">
    <table cellspacing="0" cellpadding="0">
      <thead class="nc-thead">
        <tr>
          <th class="table_cell"></th>
          <th class="table_cell time asc"><?php echo $lang['nc_weixin_wall_message_nick_name'];?></th>
          <th class="table_cell tradeId"><?php echo $lang['nc_weixin_wall_message_message'];?></th>
          <th class="table_cell goods"><?php echo $lang['nc_weixin_wall_message_state'];?></th>
		  <th class="table_cell goods"><?php echo $lang['nc_weixin_wall_message_time'];?></th>
          <th class="table_cell desc"><?php echo $lang['nc_handle'];?></th>
        </tr>
      </thead>
      <tbody class="nc-tbody">
        <?php if(!empty($output['message'])){?>
        <?php foreach($output['message'] as $key=>$val){?>
        <tr>
          <td><input type='checkbox' value="<?php echo $val['message_id']?>" class="checkitem"></td>
          <td><?php echo $val['user_name'];?></td>
          <td><?php if(mb_strlen($val['message_content'],'utf-8')>=20){ echo mb_substr($val['message_content'],0,20,'utf-8').'...';}else{ echo $val['message_content'];}?></td>
          <td>
			<?php if($val['state'] == 1){	//1.待审核?>
			<?php echo $lang['nc_weixin_wall_message_wait_audit'];?>
			<?php }elseif($val['state'] == 2){	//2.审核通过?>
			<?php echo $lang['nc_weixin_wall_message_yes_audit'];?>
			<?php }else{	//3.审核不通过?>
			<?php echo $lang['nc_weixin_wall_message_no_audit'];?>
			<?php }?>
		  </td>
          <td>
			<?php echo date("Y-m-d H:i",$val['message_time']);?>
          </td>
		  <td class="last"><a href="?act=activity&op=messageAudit&wx_id=<?php echo intval($_GET['wx_id']);?>&message_id=<?php echo $val['message_id'];?>"><?php echo $lang['nc_weixin_wall_message_audit'];?></a></td>
        </tr>
        <?php }?>
        <?php }else{?>
<tr>
			<td colspan="20" valign="middle" ><div class="norecord"><span><?php echo $lang['nc_record'];?></span></div></td>
		</tr>
        <?php }?>
      </tbody>
  <tfoot class="nc-footer">
       <tr>
      	<td class="table_cell">
      		<input type='checkbox' class="checkall">
      	</td>
      	<td colspan='6' class="table_cell last">       
	        <a onclick="javascript:submit_delete_batch(2);"  type='button' class="btn" href="JavaScript:void(0);"><span>审核通过</span></a>
	        <a onclick="javascript:submit_delete_batch(3);" type='button' class="btn" href="JavaScript:void(0);"><span>审核不通过</span></a>		
      	</td>
        </tr>
      </tfoot>
    </table>
    <input type="hidden" name="message_id" value="">
    </form>
  </div><div class="pagination"> <?php echo $output['show_page'];?> </div>
</div>

<script type="text/javascript">
	$(function(){
		$('.checkall').click(function(){
			if($(this).attr('checked') == 'checked'){
				$('.checkitem').attr('checked','checked');
			}else{
				$('.checkitem').removeAttr('checked');
			}
		});
	});
	
	function submit_delete_batch(type){
	    /* 获取选中的项 */
	    var items = '';
	    $('.checkitem:checked').each(function(){
	        items += this.value + ',';
	    });
	    if(items != '') {
	        items = items.substr(0, (items.length - 1));
	        submit_delete(items,type);
	    } else {
	        alert('请选择选项');
	    }
	}

	function submit_delete(id,type){
	    if(confirm('确认审核操作?')) {
	       	$('#list_form').attr('action','index.php?act=activity&op=batchaudit&type='+type+'&wx_id='+'<?php echo intval($_GET['wx_id']);?>');
	        $("input[name=message_id]").val(id);
	        $('form').submit();
	    }
	}
</script>




