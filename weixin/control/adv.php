<?php
/**
 * 默认展示页面
 * @copyright  Copyright (c) 2007-2013 ShopWWI Inc. (http://www.shopwwi1.com)
 * @license    http://www.shopwwi2.com
 * @link       http://www.shopwwi.com
 * @since      File available since Release v1.1
 */
defined('InByShopWWI') or exit('Access Invalid!');
define('MYSQL_RESULT_TYPE',1);
class advControl extends BaseHomeControl{
	
	public function __construct(){
		parent::__construct();
		Tpl::output('sign','adv');
	}

	public function indexOp(){
		$this->listOp();
	}
	
	/*
	 * 广告管理
	 */
	public function listOp(){
		$model = Model();
		$list = $model->table('adv')->where(array('wx_id'=>intval($_GET['wx_id'])))->page(15)->order("adv_sort desc")->select();
		Tpl::output('list',$list);
		Tpl::showpage('adv.list');
	}
	
	/*
	 * 添加商品
	 */	
	public function addAdvOp(){
		if(isset($_POST) && !empty($_POST)){
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam	=	array(
					array("input"=>trim($_POST['adv_name']),"require"=>"true","message"=>'广告名称不能为空'),
					array("input"=>trim($_POST['adv_sort']),"require"=>"true","message"=>'广告排序不能为空')
			);
				
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage(Language::get('error').$error,'','error');
			}
			
			$file_name = '';
			if(!empty($_FILES['adv_pic']['name'])){
				$name_type=substr($_FILES['adv_pic']['name'],-4,4);
				$file_name = md5(uniqid(rand(),true)).$name_type;
				move_uploaded_file($_FILES['adv_pic']['tmp_name'],BASE_UPLOAD_PATH.'/adv/'.$file_name);
			}
				
			$params 		= array();
			$params['adv_name'] = trim($_POST['adv_name']);
			$params['adv_pic']	= $file_name;
			$params['adv_state']= intval($_POST['adv_state']);
			$params['adv_sort']	= intval($_POST['adv_sort']);
			$params['wx_id']	= intval($_GET['wx_id']);
			
			$model = Model();
			$res = $model->table('adv')->insert($params);
			
			if($res){
				showMessage('添加广告成功','?act=adv&wx_id='.intval($_GET['wx_id']),'succ');
			}else{
				showMessage('添加广告失败','?act=adv&wx_id='.intval($_GET['wx_id']),'error');
			}
		}
		Tpl::showpage('adv.add');
	}

	/*
	 * 编辑广告
	 */	
	public function editAdvOp(){
		if(isset($_POST) && !empty($_POST)){//修改广告
			//表单验证
			$obj_validate = new Validate();
			$obj_validate->validateparam	=	array(
					array("input"=>trim($_POST['adv_name']),"require"=>"true","message"=>'广告名称不能为空'),
					array("input"=>trim($_POST['adv_sort']),"require"=>"true","message"=>'广告排序不能为空')
			);
			
			$error = $obj_validate->validate();
			if ($error != ''){
				showMessage(Language::get('error').$error,'','error');
			}
			
			$file_name = '';
			$params 		= array();
			if(!empty($_FILES['adv_pic']['name'])){
				$name_type=substr($_FILES['adv_pic']['name'],-4,4);
				$file_name = md5(uniqid(rand(),true)).$name_type;
				move_uploaded_file($_FILES['adv_pic']['tmp_name'],BASE_UPLOAD_PATH.'/adv/'.$file_name);
				$params['adv_pic']	= $file_name;
			}
			
			
			$params['adv_name'] = trim($_POST['adv_name']);
			
			$params['adv_state']= intval($_POST['adv_state']);
			$params['adv_sort']	= intval($_POST['adv_sort']);
			$params['wx_id']	= intval($_GET['wx_id']);
			
			$condition 				= array();
			$condition['adv_id']	= intval($_POST['adv_id']);
			$condition['wx_id']		= intval($_GET['wx_id']);
			
			$model = Model();
			$res = $model->table('adv')->where($condition)->update($params);
			
			if($res){
				showMessage('编辑广告成功','?act=adv&wx_id='.intval($_GET['wx_id']),'succ');
			}else{
				showMessage('编辑广告失败','?act=adv&wx_id='.intval($_GET['wx_id']),'error');
			}			
		}
		
		$model = Model();
		$adv = $model->table('adv')->where(array('wx_id'=>intval($_GET['wx_id']),'adv_id'=>intval($_GET['adv_id'])))->find();

		if(empty($adv)){
			showMessage('该广告不存在','?act=adv&wx_id='.intval($_GET['wx_id']),'error');
		}
		Tpl::output('adv',$adv);
		Tpl::showpage('adv.edit');
	}
	
	/*
	 * 删除商品
	 */	
	public function delAdvOp(){
		$condition	 = array();//删除条件
		$condition['wx_id'] = intval($_GET['wx_id']);
		$condition['adv_id']= array('in',trim($_POST['adv_id']));
		
		$model = Model();		
		$res = $model->table('adv')->where($condition)->delete();
		
		if($res){
			showMessage('删除广告成功','?act=adv&wx_id='.intval($_GET['wx_id']),'succ');
		}else{
			showMessage('删除广告失败','?act=adv&wx_id='.intval($_GET['wx_id']),'error');
		}	
	}
	
	
	
}